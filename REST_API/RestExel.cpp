#include "RestExel.h"

#include <QUrlQuery>
#include <QUrl>
#include <QNetworkReply>
#include <QJsonObject>
#include <QTextBrowser>
#include <QFile>
#include <QFileDialog>
#include <QTextCodec>
#include <QTimer>

#include "JsonSaver.h"

RestExel::RestExel(const globalNet_t & globalIF, QObject *parent) : QObject(parent),
    m_globalIF(globalIF)
{
    connect(&m_manager, &QNetworkAccessManager::finished, this, &RestExel::onManagerFinished);
}

STR RestExel::port      = defaultPort;
STR RestExel::IPadress  = defaultIPadress;
QString statusURL;
QString downloadURL;
QString replyData;
QString rep;
//static QString rep;

/*****************************
* Обработчик ответов от сервера
*/
void RestExel::onManagerFinished(QNetworkReply *reply)
{
    auto consol = static_cast<QTextBrowser *>(m_globalIF["Consol"]);
    if(reply->error())
    {
        qDebug() << "NetworkReply ERROR !";
        qDebug() << reply->errorString();
        qDebug() << reply->readAll();
        //consol->setPlainText(reply->readAll());
        consol->clear();
    } else{
        qDebug()<< "URL" << reply->url();


        if (reply->url() == QUrl(downloadURL) ){
           qDebug()<< "Exel ready";
           auto filename = getFilenameFromReply(reply);
           saveToFile( reply->readAll() , filename);
           downloadURL = "";
        } else if ( reply->url().adjusted(QUrl::RemoveQuery) == statusURL ){
            QString replyData = reply->readAll();
            auto Jdoc = QJsonDocument::fromJson(replyData.toUtf8());
            QJsonObject replyJson = Jdoc.object();
            if ( replyJson.contains("status") ){
                auto progress = replyJson["progress"].toInt();
                qDebug()<< replyJson;
                auto status  = replyJson["status"].toString();
                if (status == "End write Exel"){
                    consol->setPlainText( STR("Сгенерировано:") + STR::number(progress) + STR(" %") );
                    download();
                    statusURL = "";
                }else if (status == "End write with error"){
                    if (replyJson["error"].isArray()){
                        for(auto el : replyJson["error"].toArray() ){
                            consol->insertHtml(el.toString() );
                        }
                    }
                }else {
                    consol->setText( STR("Сгенерировано: ") + STR::number(progress) + STR(" %") );
                    QTimer::singleShot(500, this , &RestExel::getStatusToServer)     ;
                }
            } else {
               qDebug()<< "Error REST status:" <<  replyJson   ;
            }

        } else if ( reply->url() == QUrl( domain() + STR("//exel/api/v1.0/write" ) ) ){
            QString replyData = reply->readAll();
            auto Jdoc = QJsonDocument::fromJson(replyData.toUtf8());
            QJsonObject replyJson = Jdoc.object();
            if ( replyJson.contains("statusURL") ){
                statusURL= replyJson["statusURL"].toString();
                qDebug()<< "statusURL" << statusURL;
                QTimer::singleShot(500, this , &RestExel::getStatusToServer)     ;
            }

        } else {

            rep = reply->readAll();
            consol->setText(rep);
            qDebug() << rep;
        }
    }
}

/*****************************
* Отправка файла проекта к серверу
*/
void RestExel::sendToServer()
{
    QNetworkRequest request(QUrl( domain() + STR("//exel/api/v1.0/write") ) ); //  localhost /write - to receive Exel File
    request.setRawHeader("Content-Type", "application/json");

    QJsonObject proj;
    JsonSaver jsonSaver(m_globalIF);
    jsonSaver.createProjectJson(proj);
    auto doc =  QJsonDocument(proj).toJson();
    m_manager.post(request, doc);

}

/*****************************
* Отправка файла проекта к серверу для валидации
*/
void RestExel::validateToServer()
{
    QNetworkRequest request(QUrl( domain() + STR("//exel/api/v1.0/test")  )); //  localhost /write - to receive Exel File
    request.setRawHeader("Content-Type", "application/json");

    QJsonObject proj;
    JsonSaver jsonSaver(m_globalIF);
    jsonSaver.createProjectJson(proj);
    auto doc =  QJsonDocument(proj).toJson();
    m_manager.post(request, doc);
}

void RestExel::pingToServer()
{
    QUrl url(domain() + STR("//exel/api/v1.0/ping") );
    QUrlQuery query;

    query.addQueryItem("username", "test");
    query.addQueryItem("password", "test");

    url.setQuery(query.query());
    QNetworkRequest request(url); // without ID
    request.setRawHeader("Content-Type", "application/text");
    m_manager.get(request);
    //m_manager.post(request, "go");
}

void RestExel::getStatusToServer()
{
    //QUrl url(STR(domain()) + //exel/api/v1.0/status");
    QUrl url(statusURL);
    QUrlQuery query;

    query.addQueryItem("username", "test");
    query.addQueryItem("password", "test");

    url.setQuery(query.query());
    QNetworkRequest request(url); // without ID
    request.setRawHeader("Content-Type", "application/text");
    m_manager.get(request);
    //m_manager.post(request, "go");
}


void RestExel::download()
{
    auto statusURLlist =  statusURL.split("/");
    downloadURL = QString(domain() + STR("//exel/api/v1.0/download/") ) + statusURLlist.last();
    QUrl url( downloadURL );
    //downloadURL = STR(domain) + /exel/api/v1.0/download/1579005";
    //QUrl url( STR(domain()) + /exel/api/v1.0/download/1579005" );
    QNetworkRequest request(url);
    request.setRawHeader("Content-Type", "application/text");
    m_manager.get(request);
    //m_manager.post(request, "go");

}

bool RestExel::saveToFile(QByteArray data , const QString & fileName)
{
     QString m_fileName;
     m_fileName = QFileDialog::getSaveFileName(nullptr,
                                    tr("Сохранить Exel"), fileName)  ;
    if (m_fileName.isEmpty())
        return false;
    else
    {
        QFile saveFile(m_fileName);

        if (!saveFile.open(QIODevice::WriteOnly)) {
            qWarning("Не получилось сохранить файл");
//            QMessageBox::information(this, tr("Unable to open file"),
//                file.errorString());
            return false;
        }
        saveFile.write( data );
        saveFile.close();
//        delete data;
//        data = 0;
    }
    return true;
}

/*****************************
* Парсинг имени файла из заголовка HTTP
*/
QString  RestExel::getFilenameFromReply(const QNetworkReply *reply){
    auto header = reply->header(QNetworkRequest::ContentDispositionHeader).toString().toStdString();
    std::string utf8;
    const std::string u { R"(UTF-8'')" };
    const auto pos = header.find(u);
    if (pos != std::string::npos )
    {
        utf8 = header.substr(pos + u.size());
    }
    QByteArray encodedString(utf8.c_str(), utf8.length());
    QString string = QUrl::fromPercentEncoding(encodedString);
    return string;
}
