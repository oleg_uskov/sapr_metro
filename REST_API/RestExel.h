#ifndef RESTEXEL_H
#define RESTEXEL_H

#include <QObject>
#include <QNetworkAccessManager>

#include "main.h"
class QNetworkReply;


class RestExel : public QObject
{
    Q_OBJECT
public:
    explicit RestExel(const globalNet_t & globalIF, QObject *parent = nullptr);
    constexpr static auto defaultPort = "8085";
    constexpr static auto defaultIPadress = "192.168.77.22";
    static STR port;
    static STR IPadress;

public slots:
    void sendToServer();
    void pingToServer();
    void validateToServer();
    void getStatusToServer();
    void download();
    void onManagerFinished(QNetworkReply *reply);

signals:

private :
    bool saveToFile(QByteArray data, const QString & fileName);
    STR domain() const {return  STR("http://") + STR(IPadress) + STR(":") + STR(port);}

private :
     QString  getFilenameFromReply(const QNetworkReply *reply);

private :
     QNetworkAccessManager m_manager;
     const globalNet_t & m_globalIF;

};

#endif // RESTEXEL_H
