QT       += core gui svg xml network #qml quick quickwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

#CONFIG += qmltypes
#QML_IMPORT_NAME = my.tablemodel
#QML_IMPORT_MAJOR_VERSION = 1

#QML_IMPORT_PATH += $$PWD
#QML_IMPORT_PATH += $$PWD/Tables/
#QML2_IMPORT_PATH += $$PWD
#QML2_IMPORT_PATH += $$PWD/Tables/
#QML_IMPORT_PATH += /home/uskov_o/Projectsss/QT_PRO/Metro_SAPR/SAPR_Metro/

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += AddressOK \
INCLUDEPATH += RailPrototype\
INCLUDEPATH += RailPrototype/Rail_signal\
INCLUDEPATH += RailPrototype/Blok_area\
INCLUDEPATH += RailPrototype/Rail_section\
INCLUDEPATH += RailPrototype/Common\
INCLUDEPATH += TreeModel\
INCLUDEPATH += Tables\
INCLUDEPATH += REST_API\
INCLUDEPATH += Main

SOURCES += \
    AddressOK/AdressOK_model.cpp \
    AddressOK/PortOK_model.cpp \
    REST_API/RestExel.cpp \
    RailPrototype/Checking_overall_device.cpp \
    RailPrototype/Common/Registrate_base_rail_object.cpp \
    RailPrototype/Metall_construction.cpp \
    RailPrototype/Rail_bell.cpp \
    RailPrototype/Rail_signal/Rail_signal.cpp \
    RailPrototype/Rail_signal/Route_pointer.cpp \
    Tables/QJsonTableModel.cpp \
    Tables/RouteTable.cpp \
    TreeModel/TreeItem.cpp \
    TreeModel/TreeModel.cpp \
    Main/JsonSaver.cpp \
    TreeModel/QJsonModel.cpp \
    RailPrototype/Common/Base_rail_object.cpp \
    RailPrototype/Blok_area/Blok_area.cpp \
    RailPrototype/Common/Connection.cpp \
    RailPrototype/Connection_Line.cpp \
    RailPrototype/Neigh_station.cpp \
    RailPrototype/Blok_area/Rail_blok_edge.cpp \
    RailPrototype/Rail_generatorEN.cpp \
    RailPrototype/Rail_generatorTRC.cpp \
    RailPrototype/Rail_platform.cpp \
    RailPrototype/Rail_prot_area.cpp \
    RailPrototype/Rail_section/Rail_section.cpp \
    RailPrototype/Rail_signal/Rail_lamp.cpp \
    RailPrototype/Rail_station_edge.cpp \
    RailPrototype/Rail_switch.cpp \
    RailPrototype/Station.cpp \
    RailPrototype/Rail_section/Train.cpp \
    RailPrototype/Rail_section/Train_forward.cpp \
    Main/main.cpp \
    Main/mainwindow.cpp \
    Main/workplace.cpp \

HEADERS += \
    AddressOK/AdressOK_model.h \
    AddressOK/PortOK_model.h \
    REST_API/RestExel.h \
    RailPrototype/Checking_overall_device.h \
    RailPrototype/Common/Registrate_base_rail_object.h \
    RailPrototype/Metall_construction.h \
    RailPrototype/Rail_bell.h \
    RailPrototype/Rail_signal/Rail_signal.h \
    RailPrototype/Rail_signal/Route_pointer.h \
    Tables/QJsonTableModel.h \
    Tables/RouteTable.h \
    TreeModel/TreeItem.h \
    TreeModel/TreeModel.h \
    Main/JsonSaver.h \
    TreeModel/QJsonModel.h \
    RailPrototype/Common/Base_rail_object.h \
    RailPrototype/Blok_area/Blok_area.h \
    RailPrototype/Common/Connection.h \
    RailPrototype/Connection_Line.h \
    RailPrototype/Neigh_station.h \
    RailPrototype/Blok_area/Rail_blok_edge.h \
    RailPrototype/Rail_generatorEN.h \
    RailPrototype/Rail_generatorTRC.h \
    RailPrototype/Rail_platform.h \
    RailPrototype/Rail_prot_area.h \
    RailPrototype/Rail_section/Rail_section.h \
    RailPrototype/Rail_signal/Rail_lamp.h \
    RailPrototype/Rail_station_edge.h \
    RailPrototype/Rail_switch.h \
    RailPrototype/Station.h \
    RailPrototype/Rail_section/Train.h \
    RailPrototype/Rail_section/Train_forward.h \
    Main/main.h \
    Main/mainwindow.h \
    Main/workplace.h \

FORMS += \
    Main/Code_SynchoGroup.ui \
    Main/mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin

#target.path = $$PWD
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    icon.qrc \
    jd_object.qrc \
    property_object.qrc \
    qml.qrc

DISTFILES +=


