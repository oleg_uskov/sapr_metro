#include <QGraphicsScene>
#include <QDebug>
#include "Station.h"
#include "Connection.h"

Station::Station(const QMap<QString, QObject *> & GlobalContainerData_map, int ID,  QObject *parent) :
    Base_rail_object( GlobalContainerData_map , ID , parent)
{
    m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/station.svg"));
    this->setSharedRenderer(m_renderer);
    //setAcceptDrops(true);
    m_cnt_item++;
    auto rect = this->boundingRect();
    setText("ст.___________________");
    m_text.setPos(QPointF( 10 , rect.height()/2 - 10) );
    m_text.setParentItem(this);


    InitPropertyView();
    Base_rail_object::ShowPropertyModel();

}


Station::~Station()
{

}

const QString Station::jsonConteinerName = "Stations";
int Station::m_cnt_item = 0;


void Station::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{

    //qDebug() << "Date:"  << elementId();
    //ContextMenu.exec(event->screenPos());
}


void Station::InitPropertyView()
{
    //* Создаем модель из неизменяемых свойств*//
    QJsonTreeItem *root = InitPropertyModel();

    new QJsonTreeItem("Имя станции", text(), root);
    new QJsonTreeItem("Идентификатор", QString::number(ID()) , root);

    addPropertyFromFile(":/tempalete/json/station.json");

}

Station* Station::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    auto item = new Station(GlobalContainerData_map, rail_obj["id"].toInt());
    setBasePropertyFromJson(item, rail_obj);
    return item;
}

