#ifndef Checking_overall_device_H
#define Checking_overall_device_H

#include "Base_rail_object.h"


class Checking_overall_device : public Base_rail_object, public Registrate_base_rail_object<Checking_overall_device>
{
    Q_OBJECT

public:
    using Base_rail_object::Base_rail_object;
    Checking_overall_device();
    explicit Checking_overall_device(const QMap<QString, QObject *> & GlobalContainerData_map, int ID = 0, QObject* parent = 0 );
    ~Checking_overall_device();
    Checking_overall_device(const Checking_overall_device &) = default;

//    enum { Type = Main::Checking_overall_deviceType };
//    int type() const override   { return Type; }

    static const QString jsonConteinerName;
    QString JsonConteinerName() override {return jsonConteinerName ;}
    Checking_overall_device* createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj) override;
    //void setPropertyFromJson( QJsonObject rail_obj );


signals:

//protected:
//    void mousePressEvent( QGraphicsSceneMouseEvent* event ) override;

protected:
    //void ShowPropertyModel(void);
    //void InitPropertyModel(void);
    void InitPropertyView(void);

private:
//    unsigned int m_cornerFlags;
//    unsigned int m_actionFlags;
//    QPointF m_previousPosition;
//    bool m_leftMouseButtonPressed;
//    Dotsignal* cornerGrabber[8];

//    void resizeLeft( const QPointF& pt );
//    void resizeRight( const QPointF& pt );
//    void resizeBottom( const QPointF& pt );
//    void resizeTop( const QPointF& pt );

//    void rotateItem( const QPointF& pt );
//    void setPositionGrabbers();
//    void setVisibilityGrabbers();
//    void hideGrabbers();
    static int m_cnt_item;

signals:

};

Q_DECLARE_METATYPE(Checking_overall_device)

#endif // Checking_overall_device_H
