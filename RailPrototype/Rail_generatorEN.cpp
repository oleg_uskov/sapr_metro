#include "Rail_generatorEN.h"
#include "ui_Code_SynchoGroup.h"

Rail_generatorEN::Rail_generatorEN(const QMap<QString, QObject *> & GlobalContainerData_map, int ID,  QObject *parent) :
    Base_rail_object( GlobalContainerData_map , ID , parent),
    generatorEN_ui(new Ui::Code_SynchoGroup)
{

    m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/generatorEN.svg"));
    this->setSharedRenderer(m_renderer);
    m_cnt_item++;
    auto rect = this->boundingRect();

//    setText(QString::number(m_cnt_item));
//    m_text.setPos(QPointF(5,rect.height() - 20));
//    m_text.setParentItem(this);

//    auto code_combination = new QComboBox();
//    code_combination->addItems({"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15",});

    auto generatorEN_Widget = new QWidget();
    generatorEN_ui->setupUi(generatorEN_Widget);

//    Ui::Code_SynchoGroup *  code_synchoGroup = new Ui::Code_SynchoGroup();

    generatorEN_ui->comboBox_KK->addItems({"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15",});
    generatorEN_ui->comboBox_SG->addItems({"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15",});

    QGraphicsProxyWidget* comboBoxItem = new QGraphicsProxyWidget(this); // the proxy's parent is pMyItem
    comboBoxItem->setWidget(generatorEN_Widget); // adding the QWidget based object to the proxy
    comboBoxItem->setPos(7,4);

    Connection *Dot = new Connection(QPointF(-5, rect.height()/2), ID, "EN_LeftConn", Connection::Triangle);
    Dot->setParentItem(this);

    Dot = new Connection(QPointF(rect.width(), rect.height()/2), ID, "EN_RightConn", Connection::Triangle);
    Dot->setParentItem(this);
}

Rail_generatorEN::~Rail_generatorEN()
{
    delete m_renderer;
}

const QString Rail_generatorEN::jsonConteinerName = "generatorsEN";
int Rail_generatorEN::m_cnt_item = 0;

Rail_generatorEN* Rail_generatorEN::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    auto item = new Rail_generatorEN(GlobalContainerData_map, rail_obj["id"].toInt());
    setBasePropertyFromJson(item, rail_obj);
    return item;
}

//void Rail_switch::setPropertyFromJson( QJsonObject rail_obj ){
//    Base_rail_object::setBasePropertyFromJson(this, rail_obj);
//}
