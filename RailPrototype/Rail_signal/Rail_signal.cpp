#include <QGraphicsScene>
#include <QDebug>
#include <QGraphicsDropShadowEffect>
#include "Rail_signal.h"
#include "Rail_lamp.h"
#include "Route_pointer.h"

Rail_signal::Rail_signal()
{
    if (!m_QStandardItem){
        m_QStandardItem = new QStandardItem(QIcon(":/tempalete/svg/signal_metro.svg"),  "Светофор");
        m_QStandardItem->setData(  metaObject()->className()  );
    }
};

Rail_signal::Rail_signal(const QMap<QString, QObject *> & GlobalContainerData_map, int ID,  QObject *parent) :
    Base_rail_object( GlobalContainerData_map , ID , parent),
    m_route_pointer(nullptr)
{
    m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/signal_pole.svg"));
    this->setSharedRenderer(m_renderer);
    //setAcceptDrops(true);
    m_cnt_item++;
    setText("H" + QString::number(m_cnt_item));
    m_text.setPos(QPointF(-1,35));
    m_text.setParentItem(this);

    Connection *Dot = new Connection(QPointF(0,-10), ID,"signConn");
    Dot->setStyle(Connection::ConnectStyle::Simple);
    Dot->setFlagInheritedStyle(false);
    Dot->setParentItem(this);

    auto lamp = new Rail_lamp(GlobalContainerData_map, ID , this);
    lamp->setPos(Rail_signal::m_StartLampPoint);
    lamp->setMoveble(false);
    lamp->setParentItem(this);
    lamps.append(lamp);
//    this->installSceneEventFilter(lamp);
//    lamp->installSceneEventFilter(this);

    /*создаем структуру контекстного меню */
    connect(m_ContextMenu.addAction("Добавтить лампу"), &QAction::triggered, this, &Rail_signal::addLamp );
    connect(m_ContextMenu.addAction("Добавтить маршрутный указатель"), &QAction::triggered, this, &Rail_signal::addRoute_pointer );
    connect(m_ContextMenu.addAction("Добавтить маршрут"), &QAction::triggered, this, &Rail_signal::addRoute );

    m_BlurEffect = new QGraphicsDropShadowEffect;
    m_BlurEffect->setBlurRadius(10);
    m_BlurEffect->setXOffset(20);
    m_BlurEffect->setYOffset(20);
    setGraphicsEffect(m_BlurEffect);
    m_BlurEffect->setEnabled(false);
    setFlags( ItemIsSelectable | ItemSendsScenePositionChanges | ItemSendsGeometryChanges  );

    InitPropertyView();
    Base_rail_object::ShowPropertyModel();

}


Rail_signal::~Rail_signal()
{
}

const QString Rail_signal::jsonConteinerName = "signals";
int Rail_signal::m_cnt_item = 0;
QPointF Rail_signal::m_StartLampPoint{15 , 14};

//void Rail_signal::mousePressEvent( QGraphicsSceneMouseEvent* event )
//{
//    Base_rail_object::mousePressEvent( event );
//}

void Rail_signal::mouseDoubleClickEvent( QGraphicsSceneMouseEvent* event )
{
    SetupItemEditor();
    Base_rail_object::mouseDoubleClickEvent( event );
}

/*
 * метод активации режима редактирования ламп светофора
*/
void Rail_signal::SetupItemEditor()
{
    m_BlurEffect->setEnabled(true);
    setScale(5);
    for (auto lamp: lamps) {
        lamp->setFlag(ItemIsSelectable, true);
    }
    if (m_route_pointer)
        m_route_pointer->setFlag(ItemIsSelectable, true);

    this->setMoveble(false);
    emit itemEditorActivated(this);
    m_itemEditorState = true;
}

/*
 * метод Деактивации режима редактирования ламп светофора
*/
void Rail_signal::sceneActionChangedSlot(Main::Actions act)
{
    if (act != Main::itemEditorAction && m_itemEditorState) {
        m_BlurEffect->setEnabled(false);
        setScale(1);
        for (auto lamp: lamps) {
            lamp->setFlag(ItemIsSelectable, false);
        }
        if (m_route_pointer)
            m_route_pointer->setFlag(ItemIsSelectable, false);
        this->setMoveble(true);
    }
}

//QVariant Rail_signal::itemChange(GraphicsItemChange change, const QVariant &value)
//{
//    return Base_rail_object::itemChange(change, value);
//}

//void Rail_signal::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
//{
//}

void Rail_signal::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    if (!m_BlurEffect->isEnabled())
        m_ContextMenu.exec(event->screenPos());
}

QRectF Rail_signal::boundingRect() const
{
    QRectF rect = Base_rail_object::boundingRect();
    auto childwidth = rect.width();
    if (m_route_pointer)
        childwidth += m_route_pointer->boundingRect().width();

    for (auto lamp: lamps) {
        childwidth += lamp->boundingRect().width();
    }
    //auto childRect = childrenBoundingRect();
    //rect.setWidth(childRect.width() + 20);
    rect.setWidth(childwidth);
    return rect;
}

void Rail_signal::InitPropertyView()
{
    //* Создаем модель из неизменяемых свойств*//
    QJsonTreeItem *root = InitPropertyModel();

    new QJsonTreeItem("Имя светофора", text(), root);
    new QJsonTreeItem("Идентификатор", QString::number(ID()) , root);


    //* Создаем модель из изменяемых свойств*//
    //addPropertyFromFile("../SAPR_Metro/tempalete/json/signal.json");
    addPropertyFromFile(":/tempalete/json/signal.json");
    //*********************************************************//

//    m_routesNode = new QJsonTreeItem("Маршруты", "" , root);
//    addPropertyFromFile(":/tempalete/json/route.json");
}

Rail_signal* Rail_signal::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    auto item = new Rail_signal(GlobalContainerData_map, rail_obj["id"].toInt());
    item->setPropertyFromJson(rail_obj);
    return item;
}

void Rail_signal::setPropertyFromJson( QJsonObject rail_obj ){
    if (rail_obj.contains("Lamps") && rail_obj["Lamps"].isArray()){

        auto flamp = lamps.first();
        flamp->setParentItem(nullptr);
        lamps.removeOne(flamp); //удаляем первую зеленую лампу, которая создается по умолчанию
        //delete flamp;
        auto jsonArr = rail_obj["Lamps"].toArray();
        for(auto  trainVar : jsonArr){
            QJsonObject trainJson = trainVar.toObject();
            auto lamp =  Rail_lamp::createFromJson_static( globalIF(),trainJson );
            lamp->setMoveble(false);
//            lamp->setY(14); /////???????????????
//            lamp->setX(lamp->x() + 1); /////???????????????
            lamps.append(lamp);
            lamp->setParentItem(this);
        }
    }

    if (rail_obj.contains("Route_pointer") && rail_obj["Route_pointer"].isObject()){
        auto jsonObj = rail_obj["Route_pointer"].toObject();
        m_route_pointer =  Route_pointer::createFromJson_static( globalIF(), jsonObj );
        m_route_pointer->setMoveble(false);
//        m_route_pointer->setY(14); /////???????????????
//        m_route_pointer->setX(m_route_pointer->x() + 1); /////???????????????
        m_route_pointer->setParentItem(this);
    }
    Base_rail_object::setBasePropertyFromJson(this, rail_obj);
}

QJsonObject Rail_signal::serialize()
{
    QJsonObject rail_obj = serializeBaseObj();
    // добавим часть данных
    QJsonArray lampsArray;
    for (auto lamp: lamps) {
        lampsArray.append(lamp->serialize());
    }

    if (!lampsArray.empty())
        rail_obj["Lamps"] = lampsArray;

    if (m_route_pointer)
        rail_obj["Route_pointer"] = m_route_pointer->serialize();

    return rail_obj;
}


void Rail_signal::addLamp()
{
    auto lamp = new Rail_lamp( globalIF() );
    if (!lamps.empty()){
        auto lastLamp = lamps.last();
        auto shiftX = lastLamp->boundingRect().width() - 1;
        lamp->setPos(lastLamp->pos() + QPointF( shiftX,0) );
    } else{
        if (m_route_pointer){
            auto shiftX = m_route_pointer->boundingRect().width() - 1;
            lamp->setPos( m_route_pointer->pos() + QPointF( shiftX,0)  );
        }else{
            lamp->setPos( Rail_signal::m_StartLampPoint );
        }

    }
    lamp->setMoveble(false);
    lamps.append(lamp);
    lamp->setParentItem(this);
}


void Rail_signal::addRoute_pointer()
{
    if (!m_route_pointer){
        m_route_pointer = new Route_pointer( globalIF() );
        m_route_pointer->setPos( Rail_signal::m_StartLampPoint );
        m_route_pointer->setMoveble(false);
        m_route_pointer->setParentItem(this);
        if (!lamps.empty()){
            //auto firstLamp = lamps.first();
            auto shiftX = m_route_pointer->boundingRect().width() - 1;
            for (int i = 0; i < lamps.count(); i++ ){
                auto lamp = lamps.at(i);
                lamp->setPos(m_route_pointer->pos() + QPointF( shiftX*(i+1) , 0) );
            }
        }
    }
}

void Rail_signal::deletedItemSlot(QGraphicsItem* item)
{
    if (auto delLamp = dynamic_cast<Rail_lamp *>( item )){

        lamps.removeOne(delLamp);
//        qDebug() << childItems().count();
//        this->childItems().removeOne(delLamp);
//        qDebug() << childItems().count();
//        delete delLamp;
//        qDebug() << childItems().count();

        if (!lamps.empty()){
            auto firstLamp = lamps.first();
            auto shiftX = firstLamp->boundingRect().width() - 1;
            if (m_route_pointer){
                firstLamp->setPos( Rail_signal::m_StartLampPoint + QPointF( shiftX , 0) );
            } else {
                firstLamp->setPos( Rail_signal::m_StartLampPoint );
            }
            for (int i = 1; i < lamps.count(); i++ ){
                auto lamp = lamps.at(i);
                lamp->setPos(firstLamp->pos() + QPointF( shiftX*i , 0) );
            }
        }
    } else if (auto delRP = dynamic_cast<Route_pointer *>( item )){
        if (delRP == m_route_pointer){
            m_route_pointer->setParent(nullptr);
            //delete m_route_pointer;
            m_route_pointer = nullptr;
            // сдвиг всеx ламп
            if (!lamps.empty()){
                auto firstLamp = lamps.first();
                firstLamp->setPos( Rail_signal::m_StartLampPoint );
                auto shiftX = firstLamp->boundingRect().width() - 1;
                for (int i = 1; i < lamps.count(); i++ ){
                    auto lamp = lamps.at(i);
                    lamp->setPos(firstLamp->pos() + QPointF( shiftX*i , 0) );
                }
            }
        }
    }


}

void Rail_signal::addRoute()
{

}
