#include <QGraphicsScene>
#include <QDebug>
#include "Route_pointer.h"

Route_pointer::Route_pointer()
{
//    if (!m_QStandardItem){
//        m_QStandardItem = new QStandardItem(QIcon(":/tempalete/svg/route_pointer.svg"),  "Маршрутный указатель");
//        m_QStandardItem->setData(  metaObject()->className()  );
//    }
};

Route_pointer::Route_pointer(const QMap<QString, QObject *> & GlobalContainerData_map, int ID,  QObject *parent) :
    Base_rail_object( GlobalContainerData_map , ID , parent)
{
    m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/route_pointer.svg"));
    this->setSharedRenderer(m_renderer);
    //setAcceptDrops(true);
    m_cnt_item++;
    setText("");

    setFlag(ItemIsSelectable, false);

    InitPropertyView();
    Base_rail_object::ShowPropertyModel();
}

Route_pointer::~Route_pointer()
{
}

const QString Route_pointer::jsonConteinerName = ""; // не нужно паковать автоматически
int Route_pointer::m_cnt_item = 0;


void Route_pointer::mouseDoubleClickEvent( QGraphicsSceneMouseEvent* event )
{
    if  ( event->modifiers() & Qt::SHIFT ){
        //goToNextStyle();
    }

    Base_rail_object::mouseDoubleClickEvent( event );
}

void Route_pointer::InitPropertyView()
{
    //* Создаем модель из неизменяемых свойств*//
    QJsonTreeItem *root = InitPropertyModel();

    new QJsonTreeItem("Имя маршрутного указателя", text(), root);
    //new QJsonTreeItem("Идентификатор", QString::number(ID()) , root);

    //* Создаем модель из изменяемых свойств*//
    //addPropertyFromFile("../SAPR_Metro/tempalete/json/route_pointer.json");
    addPropertyFromFile(":/tempalete/json/route_pointer.json");
    //*********************************************************//

}

void Route_pointer::wheelEvent(QGraphicsSceneWheelEvent *event)
{
//    if( this->isSelected() )
//    {
//        if( event->delta() > 0 )
//            goToNextStyle(1);
//        else
//            goToNextStyle(-1);

//        event->accept();
//    }
    //Base_rail_object::wheelEvent( event );

}

Route_pointer* Route_pointer::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    return Route_pointer::createFromJson_static( GlobalContainerData_map, rail_obj);
}

Route_pointer* Route_pointer::createFromJson_static(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    auto item = new Route_pointer(GlobalContainerData_map, rail_obj["id"].toInt(), 0);
    item->setPropertyFromJson(rail_obj);
    return item;
}

void Route_pointer::setPropertyFromJson( QJsonObject rail_obj ){
    Base_rail_object::setBasePropertyFromJson(this, rail_obj);
}

QJsonObject Route_pointer::serialize()
{
    QJsonObject lampJson = serializeBaseObj();
    return lampJson;
}
