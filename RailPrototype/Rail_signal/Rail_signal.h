#ifndef RAIL_SIGNAL_H
#define RAIL_SIGNAL_H


#include "Base_rail_object.h"
class Rail_lamp;
class Route_pointer;
class QGraphicsDropShadowEffect;

class Rail_signal : public Base_rail_object , public Registrate_base_rail_object<Rail_signal>
{
    Q_OBJECT

public:
    using Base_rail_object::Base_rail_object;
    Rail_signal();
    explicit Rail_signal(const QMap<QString, QObject *> & GlobalContainerData_map, int ID = 0, QObject* parent = 0 );
    ~Rail_signal();
    Rail_signal(const Rail_signal &) = default;

    enum { Type = Main::Rail_signalType };
    int type() const override   { return Type; }

    static QPointF m_StartLampPoint;
    static const QString jsonConteinerName;
    QString JsonConteinerName() override {return jsonConteinerName ;}
    Rail_signal* createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj) override;
    void setPropertyFromJson( QJsonObject rail_obj ) override;
    QJsonObject serialize() override;

signals:

protected:
    //void mousePressEvent( QGraphicsSceneMouseEvent* event ) override;
    void mouseDoubleClickEvent( QGraphicsSceneMouseEvent* event ) override;
    //QVariant itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value) override;
    QRectF boundingRect(void) const override;

protected:
    void InitPropertyView(void);
    void SetupItemEditor(void);

public slots:
        void deletedItemSlot(QGraphicsItem* item) override;
        void sceneActionChangedSlot(Main::Actions act) override;

private slots:
    void addRoute(void);
    void addLamp(void);
    void addRoute_pointer(void);

protected:
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override;



private:
    static int m_cnt_item;
    QJsonTreeItem * m_routesNode;
    QList< Rail_lamp *> lamps;
    Route_pointer * m_route_pointer;
    QGraphicsDropShadowEffect* m_BlurEffect;
    bool m_itemEditorState;



signals:

};

Q_DECLARE_METATYPE(Rail_signal)

#endif // RAIL_SIGNAL_H
