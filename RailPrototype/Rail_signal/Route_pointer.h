#ifndef Route_pointer_H
#define Route_pointer_H


#include "Base_rail_object.h"

class Route_pointer : public Base_rail_object , public Registrate_base_rail_object<Route_pointer>
{
    Q_OBJECT

public:
    using Base_rail_object::Base_rail_object;
    Route_pointer();
    explicit Route_pointer(const QMap<QString, QObject *> & GlobalContainerData_map, int ID = 0, QObject* parent = 0 );
    ~Route_pointer();
    Route_pointer(const Route_pointer &) = default;

//    enum { Type = Main::Route_pointerType };
//    int type() const override   { return Type; }

    static const QString jsonConteinerName;
    static Route_pointer* createFromJson_static(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj);
    Route_pointer* createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj) override;
    void setPropertyFromJson( QJsonObject rail_obj ) override;
    QJsonObject serialize() override;

signals:


protected:
    void InitPropertyView(void);


private slots:


protected:
    //void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override;
	void mouseDoubleClickEvent( QGraphicsSceneMouseEvent* event ) override;
    void wheelEvent(QGraphicsSceneWheelEvent *event) override;

private:
    static int m_cnt_item;

signals:

};

Q_DECLARE_METATYPE(Route_pointer)

#endif // Route_pointer_H
