#include <QGraphicsScene>
#include <QDebug>
#include "Rail_lamp.h"

Rail_lamp::Rail_lamp()
{
//    if (!m_QStandardItem){
//        m_QStandardItem = new QStandardItem(QIcon(":/tempalete/svg/lamp_green.svg"),  "Лампа");
//        m_QStandardItem->setData(  metaObject()->className()  );
//    }
};

Rail_lamp::Rail_lamp(const QMap<QString, QObject *> & GlobalContainerData_map, int ID,  QObject *parent) :
    Base_rail_object( GlobalContainerData_map , ID , parent)
{
    m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/lamp_green.svg"));
    this->setSharedRenderer(m_renderer);
    //setAcceptDrops(true);
    m_cnt_item++;
    setText("");
//    m_text.setPos(QPointF(5,35));
//    m_text.setParentItem(this);

    setFlag(ItemIsSelectable, false);

    InitPropertyView();
    Base_rail_object::ShowPropertyModel();
}

Rail_lamp::~Rail_lamp()
{
}

const QString Rail_lamp::jsonConteinerName = ""; // не нужно паковать автоматически
int Rail_lamp::m_cnt_item = 0;


void Rail_lamp::mouseDoubleClickEvent( QGraphicsSceneMouseEvent* event )
{
    if  ( event->modifiers() & Qt::SHIFT ){
        goToNextStyle();
    }

    Base_rail_object::mouseDoubleClickEvent( event );
}

void Rail_lamp::InitPropertyView()
{
    //* Создаем модель из неизменяемых свойств*//
    QJsonTreeItem *root = InitPropertyModel();

    new QJsonTreeItem("Имя лампы", text(), root);
    //new QJsonTreeItem("Идентификатор", QString::number(ID()) , root);

    //* Создаем модель из изменяемых свойств*//
    //addPropertyFromFile("../SAPR_Metro/tempalete/json/lamp.json");
    addPropertyFromFile(":/tempalete/json/lamp.json");
    //*********************************************************//

}

void Rail_lamp::wheelEvent(QGraphicsSceneWheelEvent *event)
{
    if( this->isSelected() )
    {
        if( event->delta() > 0 )
            goToNextStyle(1);
        else
            goToNextStyle(-1);

        event->accept();
    }
    //Base_rail_object::wheelEvent( event );

}

//Rail_lamp* Rail_lamp::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
//    auto item = new Rail_lamp(GlobalContainerData_map, rail_obj["id"].toInt());
//    setBasePropertyFromJson(item, rail_obj);
//    return item;
//}
Rail_lamp* Rail_lamp::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    return Rail_lamp::createFromJson_static( GlobalContainerData_map, rail_obj);
}

Rail_lamp* Rail_lamp::createFromJson_static(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    auto item = new Rail_lamp(GlobalContainerData_map, rail_obj["id"].toInt(), 0);
    item->setPropertyFromJson(rail_obj);
    return item;
}

void Rail_lamp::setPropertyFromJson( QJsonObject rail_obj ){

    Base_rail_object::setBasePropertyFromJson(this, rail_obj);
    setStyle( (LampStyle)rail_obj["lampType"].toInt() );

}

QJsonObject Rail_lamp::serialize()
{
    QJsonObject lampJson = serializeBaseObj();
    lampJson["lampType"]  = (int)m_lampStyle;
    return lampJson;
}

void Rail_lamp::goToNextStyle(int step )
{
    if (m_lampStyle == NUMBER_LampStyles){
        m_lampStyle = GREEN;
    } else if (m_lampStyle == 0){
            m_lampStyle = (LampStyle)((int)NUMBER_LampStyles - 1);
    } else {
        auto next = m_lampStyle + step;
        m_lampStyle = (LampStyle)next;
    }
    changeView();
}

void Rail_lamp::changeView()
{
    if(!m_renderer)
        delete m_renderer;

    if (m_lampStyle == GREEN)
        m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/lamp_green.svg"));
    else if (m_lampStyle == RED)
        m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/lamp_red.svg"));
    else if(m_lampStyle == YELLOW)
        m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/lamp_yellow.svg"));
    else if(m_lampStyle == WHITE)
        m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/lamp_white.svg"));
    else if(m_lampStyle == BLINK_WHITE)
        m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/lamp_blink_white.svg"));
    else if(m_lampStyle == POLE)
        m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/lamp_pole.svg"));
    else if(m_lampStyle == ABSENT)
        m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/lamp_absent.svg"));
    else if(m_lampStyle == BLUE)
        m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/lamp_blue.svg"));
    else if(m_lampStyle == BLINK_YELLOW)
        m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/lamp_blink_yellow.svg"));

    this->setSharedRenderer(m_renderer);
}

void Rail_lamp::setStyle(const LampStyle style) {
    m_lampStyle = style;
    changeView();
}
