#ifndef Rail_lamp_H
#define Rail_lamp_H


#include "Base_rail_object.h"

class Rail_lamp : public Base_rail_object , public Registrate_base_rail_object<Rail_lamp>
{
    Q_OBJECT

    enum LampStyle
    {
		GREEN = 1,
		RED,
		YELLOW,
        WHITE,
        BLINK_WHITE,
        POLE,
        ABSENT,
        BLUE,
        BLINK_YELLOW,

        NUMBER_LampStyles
    };

public:
    using Base_rail_object::Base_rail_object;
    Rail_lamp();
    explicit Rail_lamp(const QMap<QString, QObject *> & GlobalContainerData_map, int ID = 0, QObject* parent = 0 );
    ~Rail_lamp();
    Rail_lamp(const Rail_lamp &) = default;

//    enum { Type = Main::Rail_lampType };
//    int type() const override   { return Type; }

    static const QString jsonConteinerName;
    static Rail_lamp* createFromJson_static(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj);
    Rail_lamp* createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj) override;
    void setPropertyFromJson( QJsonObject rail_obj ) override;
    QJsonObject serialize() override;

    LampStyle Style() const{return m_lampStyle;}
	void setStyle(const LampStyle style);

    //QPointF centeredScenePosition() const { return m_CenteredPosition; }
    //void setCenteredPosition(QPointF pos);
    void changeView();
    void goToNextStyle(int step = 1);

signals:


protected:
    void InitPropertyView(void);


private slots:


protected:
    //void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override;
	void mouseDoubleClickEvent( QGraphicsSceneMouseEvent* event ) override;
    void wheelEvent(QGraphicsSceneWheelEvent *event) override;

private:
    static int m_cnt_item;
    LampStyle      m_lampStyle = GREEN;

signals:

};

Q_DECLARE_METATYPE(Rail_lamp)

#endif // Rail_lamp_H
