#ifndef Rail_generatorTRC_H
#define Rail_generatorTRC_H


#include "Base_rail_object.h"

class Rail_generatorTRC : public Base_rail_object, public Registrate_base_rail_object<Rail_generatorTRC>
{
    Q_OBJECT


public:
    //using Base_rail_object::Base_rail_object;
    Rail_generatorTRC() ;
    explicit Rail_generatorTRC(const QMap<QString, QObject *> & GlobalContainerData_map, int ID = 0, QObject* parent = 0 );
    ~Rail_generatorTRC();
    Rail_generatorTRC(const Rail_generatorTRC &) = default;

//    enum { Type = Main::Rail_generatorTRCType };
//    int type() const override   { return Type; }

    static const QString jsonConteinerName;
    QString JsonConteinerName() override {return jsonConteinerName ;}
    Rail_generatorTRC* createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj) override;
    //void setPropertyFromJson( QJsonObject rail_obj );
    Connection * getConn() const { return m_generatorTRCConn; }

signals:



protected:

    void InitPropertyView(void);


private slots:

protected:
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override;


private:


    static int m_cnt_item;

signals:

private:
    Connection * m_generatorTRCConn;

};

Q_DECLARE_METATYPE(Rail_generatorTRC)

#endif // Rail_generatorTRC_H
