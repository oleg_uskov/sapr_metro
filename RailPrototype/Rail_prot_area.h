#ifndef RAIL_PROT_AREA_H
#define RAIL_PROT_AREA_H

#include "Base_rail_object.h"

class Rail_prot_area : public Base_rail_object
{
    Q_OBJECT


public:
    using Base_rail_object::Base_rail_object;
    Rail_prot_area(){};
    explicit Rail_prot_area(const QMap<QString, QObject *> & GlobalContainerData_map, int ID = 0, QObject* parent = 0 );
    ~Rail_prot_area();
    Rail_prot_area(const Rail_prot_area &) = default;

    enum { Type = Main::Rail_prot_areaType };
    int type() const override   { return Type; }

    static const QString jsonConteinerName;
    QString JsonConteinerName() override {return jsonConteinerName ;}
    Rail_prot_area* createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj) override;
    //void setPropertyFromJson( QJsonObject rail_obj );

signals:

private slots:
    //void addProtect_area(void);

protected:
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override;


private:
//    unsigned int m_cornerFlags;
//    unsigned int m_actionFlags;
//    QPointF m_previousPosition;
//    bool m_leftMouseButtonPressed;
//    Dotsignal* cornerGrabber[8];

//    void resizeLeft( const QPointF& pt );
//    void resizeRight( const QPointF& pt );
//    void resizeBottom( const QPointF& pt );
//    void resizeTop( const QPointF& pt );

//    void rotateItem( const QPointF& pt );
//    void setPositionGrabbers();
//    void setVisibilityGrabbers();
//    void hideGrabbers();

    static int m_cnt_item;

signals:

};

Q_DECLARE_METATYPE(Rail_prot_area)

#endif // RAIL_SIGNAL_H
