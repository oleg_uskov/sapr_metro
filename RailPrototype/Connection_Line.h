#ifndef Connection_Line_H
#define Connection_Line_H

#include "Base_rail_object.h"
//#include <QTreeWidget>

class Connection_Line : public Base_rail_object
{
    Q_OBJECT


public:
    //using Base_rail_object::Base_rail_object;
    Connection_Line() {};
    explicit Connection_Line(const QMap<QString, QObject *> & GlobalContainerData_map, int ID, QObject* parent = 0 );
    ~Connection_Line();
    Connection_Line(const Connection_Line &) = default;

    enum { Type = Main::Connection_LineType };
    int type() const override   { return Type; }

    static const QString jsonConteinerName;
    QString JsonConteinerName() override {return jsonConteinerName ;}
    Connection_Line* createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj) override;
    void setPropertyFromJson( QJsonObject rail_obj ) override;
    QJsonObject serialize() override;


    void setEndPoint(QPointF endPoint);
    QPointF startPoint( void ) const {return mapToScene(m_startPoint);}

signals:

protected:
    void mousePressEvent( QGraphicsSceneMouseEvent* event ) override;
    //QVariant itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value) override;

protected:
    void InitPropertyView(void);
    QRectF boundingRect(void) const override;
    QPainterPath shape(void) const override;
    //void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

public slots:
//    void sceneActionChangedSlot(int action);
//    void deletedItemSlot(QGraphicsItem* item);
    void dataChangedStot(const QModelIndex & topLeft, const QModelIndex & bottomRight,  const QVector<int> &roles = QVector<int>()) override;

private slots:

private:

    Connection *           m_startConnection;
    Connection *           m_endConnection;
    QLineF                 m_line ; //= QLineF(0, 0 , 0 , 0);
    QGraphicsLineItem *     m_lineItem ;
    QPointF                m_startPoint;
    //QGraphicsLineItem *   m_backgroundLineItem ;

};

Q_DECLARE_METATYPE(Connection_Line)

#endif // Connection_Line_H
