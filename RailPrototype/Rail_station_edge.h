#ifndef Rail_station_edge_H
#define Rail_station_edge_H


#include "Base_rail_object.h"

class Rail_station_edge : public Base_rail_object, public Registrate_base_rail_object<Rail_station_edge>
{
    Q_OBJECT


public:
    using Base_rail_object::Base_rail_object;
    Rail_station_edge();
    explicit Rail_station_edge(const QMap<QString, QObject *> & GlobalContainerData_map, int ID = 0, QObject* parent = 0 );
    ~Rail_station_edge();
    Rail_station_edge(const Rail_station_edge &) = default;

    enum { Type = Main::Rail_station_edgeType };
    int type() const override   { return Type; }

    static const QString jsonConteinerName;
    QString JsonConteinerName() override {return jsonConteinerName ;}
    Rail_station_edge* createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj) override;
    //void setPropertyFromJson( QJsonObject rail_obj );



signals:

private slots:
    //void addProtect_area(void);

protected:
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override;
    void InitPropertyView(void);


private:
    static int m_cnt_item;

signals:

};

Q_DECLARE_METATYPE(Rail_station_edge)

#endif // RAIL_SIGNAL_H
