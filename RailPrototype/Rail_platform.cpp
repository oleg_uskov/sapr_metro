#include <QGraphicsScene>
#include <QDebug>
#include "Rail_platform.h"
#include "Connection.h"

Rail_platform::Rail_platform()
{
    if (!m_QStandardItem){
        m_QStandardItem = new QStandardItem(QIcon(":/tempalete/svg/platform.svg"),  "Платформа");
        m_QStandardItem->setData(  metaObject()->className()  );
    }
};

Rail_platform::Rail_platform(const QMap<QString, QObject *> & GlobalContainerData_map, int ID,  QObject *parent) :
    Base_rail_object( GlobalContainerData_map , ID , parent)
{
    m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/platform.svg"));
    this->setSharedRenderer(m_renderer);
    //setAcceptDrops(true);
    m_cnt_item++;
    setText("имя платформы");
    m_text.setPos(QPointF(100,20));
    m_text.setParentItem(this);
    Connection *Dot;
    auto rect = this->boundingRect();
    Dot = new Connection(QPointF(0, 0), ID, "platformLTConn");
    Dot->setStyle(Connection::Simple);
    Dot->setParentItem(this);

    Dot = new Connection(QPointF(0, rect.height()), ID, "platformLBConn");
    Dot->setStyle(Connection::Simple);
    Dot->setParentItem(this);

    Dot = new Connection(QPointF(rect.width(), 0), ID, "platformRTConn");
    Dot->setStyle(Connection::Simple);
    Dot->setParentItem(this);

    Dot = new Connection(QPointF(rect.width(), rect.height()), ID, "platformRBConn");
    Dot->setStyle(Connection::Simple);
    Dot->setParentItem(this);

    InitPropertyView();
    Base_rail_object::ShowPropertyModel();

}


Rail_platform::~Rail_platform()
{

}

const QString Rail_platform::jsonConteinerName = "Rail_platforms";
int Rail_platform::m_cnt_item = 0;


void Rail_platform::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{

    //qDebug() << "Date:"  << elementId();
    //ContextMenu.exec(event->screenPos());
}

void Rail_platform::addProtect_area()
{
    //    for (auto & child: this->childItems()) {
    //        if(Connection* child_conn = dynamic_cast<Connection*>( child )){
    //            QPointF start_point = child_conn->scenePos();
    //            QPainterPath path;
    //            path.moveTo(start_point);
    //            path.lineTo( event->scenePos() );
    //            QGraphicsPathItem * protectAreaPath =  new QGraphicsPathItem;
    //            protectAreaPath->setPath( path );
    //            if(auto MyScene = this->scene())
    //                    MyScene-> addItem( protectAreaPath );
    //            break;
    //         }
    //    }

//    Protect_area * protect_area = new Protect_area(this);
//    protect_area->setParentItem(this);
}



void Rail_platform::InitPropertyView()
{
    //* Создаем модель из неизменяемых свойств*//
    QJsonTreeItem *root = InitPropertyModel();

    new QJsonTreeItem("Имя платформы", text(), root);
    new QJsonTreeItem("Идентификатор", QString::number(ID()) , root);

    addPropertyFromFile(":/tempalete/json/platform.json");

}

Rail_platform* Rail_platform::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    auto item = new Rail_platform(GlobalContainerData_map, rail_obj["id"].toInt());
    setBasePropertyFromJson(item, rail_obj);
    return item;
}

//void Rail_switch::setPropertyFromJson( QJsonObject rail_obj ){
//    Base_rail_object::setBasePropertyFromJson(this, rail_obj);
//}

