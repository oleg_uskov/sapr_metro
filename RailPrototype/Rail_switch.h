#ifndef SWITCH_H
#define SWITCH_H

#include "Base_rail_object.h"


class Rail_switch : public Base_rail_object, public Registrate_base_rail_object<Rail_switch>
{
    Q_OBJECT

public:
    using Base_rail_object::Base_rail_object;
    Rail_switch();
    explicit Rail_switch(const QMap<QString, QObject *> & GlobalContainerData_map, int ID = 0, QObject* parent = 0 );
    ~Rail_switch();
    Rail_switch(const Rail_switch &) = default;

    enum { Type = Main::Rail_switchType };
    int type() const override   { return Type; }

    static const QString jsonConteinerName;
    QString JsonConteinerName() override {return jsonConteinerName ;}
    Rail_switch* createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj) override;
    //void setPropertyFromJson( QJsonObject rail_obj );


signals:

//protected:
//    void mousePressEvent( QGraphicsSceneMouseEvent* event ) override;

protected:
    //void ShowPropertyModel(void);
    //void InitPropertyModel(void);
    void InitPropertyView(void);

private:
//    unsigned int m_cornerFlags;
//    unsigned int m_actionFlags;
//    QPointF m_previousPosition;
//    bool m_leftMouseButtonPressed;
//    Dotsignal* cornerGrabber[8];

//    void resizeLeft( const QPointF& pt );
//    void resizeRight( const QPointF& pt );
//    void resizeBottom( const QPointF& pt );
//    void resizeTop( const QPointF& pt );

//    void rotateItem( const QPointF& pt );
//    void setPositionGrabbers();
//    void setVisibilityGrabbers();
//    void hideGrabbers();
    static int m_cnt_item;

signals:

};

Q_DECLARE_METATYPE(Rail_switch)

#endif // SWITCH_H
