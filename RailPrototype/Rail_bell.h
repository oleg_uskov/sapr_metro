#ifndef Rail_bell_H
#define Rail_bell_H

#include "Base_rail_object.h"


class Rail_bell : public Base_rail_object, public Registrate_base_rail_object<Rail_bell>
{
    Q_OBJECT

public:
    using Base_rail_object::Base_rail_object;
    Rail_bell();
    explicit Rail_bell(const QMap<QString, QObject *> & GlobalContainerData_map, int ID = 0, QObject* parent = 0 );
    ~Rail_bell();
    Rail_bell(const Rail_bell &) = default;

//    enum { Type = Main::Rail_bellType };
//    int type() const override   { return Type; }

    static const QString jsonConteinerName;
    QString JsonConteinerName() override {return jsonConteinerName ;}
    Rail_bell* createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj) override;
    //void setPropertyFromJson( QJsonObject rail_obj );


signals:



protected:
    void InitPropertyView(void);

private:
    static int m_cnt_item;

signals:

};

Q_DECLARE_METATYPE(Rail_bell)

#endif // Rail_bell_H
