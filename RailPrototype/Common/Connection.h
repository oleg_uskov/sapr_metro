#ifndef CONNECTION_H
#define CONNECTION_H

#include <QObject>
#include <QString>
#include <QGraphicsSvgItem>
#include <QSvgRenderer>
#include <QGraphicsSceneDragDropEvent>
#include <QMimeData>
#include <QAction>
#include <QMenu>

#include "main.h"
class WorkPlace;


class Connection : public QGraphicsSvgItem
{
    Q_OBJECT
    Q_PROPERTY( QList <Connection *> Neighbour_connections READ Neighbour_connections)

public:
    enum ConnectTypes
    {
        NoConnect,
        Connect,
        //IsolConnect,
    };

    enum ConnectStyle
    {
        //connection style
		WithoutIsolation = 2,
		Isolation,
		OverSizedIsolation,
        Simple,

        //No Connection style
        Cross = 10,
        Triangle,
        Dock,
        DotLine
    };

public:
    using QGraphicsSvgItem::QGraphicsSvgItem;
    explicit Connection(QPointF centrePos, int parentID, QString nameConn = "", ConnectStyle style = Cross, QGraphicsItem *parent = nullptr);
    enum { Type = Main::ConnectionType };
    int type() const override   { return Type; }

    int m_parent_ID;
    QList <Connection *> m_Neighbour_connections; // будет такой класс Connection, с который есть связь
    QList <Connection *> Neighbour_connections() const{ return m_Neighbour_connections;};
    ConnectTypes m_CurrentConnectState;
    QString m_nameConn;

    ConnectStyle Style(ConnectTypes ConnectState = Connect) const{return ConnectState == Connect ? m_ConnectStyle : m_NoConnectStyle;}
	void setStyle(const ConnectStyle connectStyle, const ConnectStyle NoConnectStyle = Cross)  ;
    ConnectTypes State() const{return m_CurrentConnectState;}
	//void setState(const ConnectTypes connectType) const{m_CurrentConnectState =connectType;}

    void connectWith(Connection * neighbour_connection);
    void disconnectWith(Connection * neighbour_connection);
    void disconnectAll();
    QPointF centeredScenePosition() const { return m_CenteredPosition; }
    void setCenteredPosition(QPointF pos);
    void goToNextStyle();
    bool isInheritedStyle() const{return m_ena_inheritStyle;}
    void setFlagInheritedStyle(const bool en) { m_ena_inheritStyle = en; }
    void inheritStyle(Connection* otherConn) ;
    void addPowerEnd(void) ;

signals:
    void previousPositionChanged();
    void signalMove( QGraphicsItem* item, qreal dx, qreal dy );
    void ConnectionClicked(Connection* conn );

protected:
    void mouseMoveEvent( QGraphicsSceneMouseEvent* event ) override;
    void mousePressEvent( QGraphicsSceneMouseEvent* event ) override;
//    void mouseReleaseEvent( QGraphicsSceneMouseEvent* event ) override;
    void hoverLeaveEvent( QGraphicsSceneHoverEvent* event ) override;
    void hoverMoveEvent( QGraphicsSceneHoverEvent* event ) override;
    void hoverEnterEvent( QGraphicsSceneHoverEvent* event ) override;
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override;
	void mouseDoubleClickEvent( QGraphicsSceneMouseEvent* event ) override;

    bool dragOver = false;


public slots:
    void installJointSlot();

private slots:
//    void slotMove( QGraphicsItem* signalOwner, qreal dx, qreal dy );


private:

    void changeConnectorView(ConnectTypes con);
    void moveToCenteredPoint(void);

    QMenu             m_ContextMenu;

    QPointF           m_CenteredPosition;
    //bool              m_leftMouseButtonPressed;
    ConnectStyle      m_NoConnectStyle = Cross;
    ConnectStyle      m_ConnectStyle = WithoutIsolation;	
	QSvgRenderer *    m_renderer;
	bool              m_ena_inheritStyle = true;

};

#endif // CONNECTION_H
