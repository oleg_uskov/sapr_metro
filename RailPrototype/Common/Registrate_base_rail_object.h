#ifndef Registrate_base_rail_object_H
#define Registrate_base_rail_object_H

#include <QObject>

QList< QString > &get_base_rail_object();
bool add_object( QString ) ;

template<class T>
class Registrate_base_rail_object {
public:
    Registrate_base_rail_object() {
        reg;  //force specialization
    }

    static bool reg;
    static bool init()
    {
       //T* base = new T();
       //auto base_rail = static_cast<Base_rail_object*>(base);
       auto id = qMetaTypeId<T>();
       qRegisterMetaType<T>( QMetaType::typeName(id) );
       add_object( QMetaType::typeName(id) );
       return true;
    }
};

template<class T> bool Registrate_base_rail_object<T>::reg = Registrate_base_rail_object<T>::init();


#endif // Registrate_base_rail_object_H
