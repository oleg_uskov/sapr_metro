#include <QCheckBox>

#include "Connection.h"
#include "Rail_generatorTRC.h"
#include "workplace.h"

Connection::Connection(QPointF centrePos, int parentID, QString nameConn, ConnectStyle style, QGraphicsItem *parent) :
    QGraphicsSvgItem(parent),
    m_CenteredPosition(centrePos),
    m_NoConnectStyle(style)

{
    changeConnectorView(NoConnect);
    //this->setPos(pos);
    //this->setSharedRenderer(m_renderer);
    moveToCenteredPoint();
    //setFlags( ItemIsSelectable);

    m_parent_ID = parentID;
    m_nameConn = nameConn;
	
	/*создаем структуру контекстного меню */
//    QAction *addJoint_Action = m_ContextMenu.addAction("Добавить стык");
//    connect(addJoint_Action, &QAction::triggered, this, &Connection::installJointSlot );

    connect(m_ContextMenu.addAction("Добавить питающий конец"),      &QAction::triggered, this, &Connection::addPowerEnd ) ;
	
}


void Connection::mousePressEvent( QGraphicsSceneMouseEvent* event )
{

    if  ( event->button() & Qt::LeftButton )
         if  (!( event->modifiers() & Qt::SHIFT ) )//
        {
            emit ConnectionClicked(this);
        }
    //QGraphicsSvgItem::mousePressEvent( event );
}


void Connection::hoverLeaveEvent( QGraphicsSceneHoverEvent* event )
{
    QGraphicsSvgItem::hoverLeaveEvent( event );
}

void Connection::hoverMoveEvent( QGraphicsSceneHoverEvent* event )
{
    QGraphicsSvgItem::hoverMoveEvent( event );
}

void Connection::hoverEnterEvent( QGraphicsSceneHoverEvent* event )
{
    QGraphicsSvgItem::hoverEnterEvent( event );
}

void Connection::mouseMoveEvent( QGraphicsSceneMouseEvent* event )
{
    QGraphicsSvgItem::mouseMoveEvent( event );
}

void Connection::changeConnectorView(ConnectTypes con)
{
    if(!m_renderer)
        delete m_renderer;

    if (con == Connect){
		if (m_ConnectStyle == WithoutIsolation)
			m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/connect_circle.svg"));
		else if (m_ConnectStyle == Isolation)
			m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/isol_connect.svg"));
		else if(m_ConnectStyle == OverSizedIsolation)
			m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/oversized_isol_connect.svg"));
        else if(m_ConnectStyle == Simple)
			m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/connect_line.svg"));
	}
    else { //NoConnect
        if (m_NoConnectStyle == Cross)
            m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/no_connect_cross.svg"));
        else if (m_NoConnectStyle == Triangle)
             m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/no_connect_triangle.svg"));
        else if (m_NoConnectStyle == Dock)
             m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/dock.svg"));
        else if (m_NoConnectStyle == DotLine)
             m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/dot_line.svg"));
    }

    this->setSharedRenderer(m_renderer);
    moveToCenteredPoint();
}


void Connection::connectWith(Connection * neighbour_connection)
{
    if (!m_Neighbour_connections.contains(neighbour_connection))
        m_Neighbour_connections.append(neighbour_connection);

    changeConnectorView(Connect);
    m_CurrentConnectState = Connect;
}

void Connection::disconnectWith(Connection * neighbour_connection)
{
    m_Neighbour_connections.removeAll(neighbour_connection);
    if (m_Neighbour_connections.count() == 0){
        changeConnectorView(NoConnect);
        m_CurrentConnectState = NoConnect;
    }
}

void Connection::disconnectAll()
{
    m_Neighbour_connections.clear();
    changeConnectorView(NoConnect);
    m_CurrentConnectState = NoConnect;
}

void Connection::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    //qDebug() << "Date:"  << elementId();
    m_ContextMenu.exec(event->screenPos());
    //Connection::contextMenuEvent( event );
    event->accept();
}

void Connection::mouseDoubleClickEvent( QGraphicsSceneMouseEvent* event )
{
    if  ( event->modifiers() & Qt::SHIFT ){
        goToNextStyle();
        changeConnectorView(m_CurrentConnectState);
    }


    QGraphicsSvgItem::mouseDoubleClickEvent( event );
}

void Connection::installJointSlot()
{
	setStyle(ConnectStyle::Isolation);
	changeConnectorView(m_CurrentConnectState);
}

/*****************************
* Добавляем генератор ТРЦ на сцену
*/
void Connection::addPowerEnd() {
    auto base = static_cast<Base_rail_object *>(parentItem());
    auto  workplace  = base->workplace();
    auto generator = new Rail_generatorTRC(base->globalIF(), workplace->getUniqueID());

    auto genConn = generator->getConn();
    auto deltaPos = genConn->pos() - generator->pos();

    auto centerConn = boundingRect().center();
    workplace->add_new_item_to_scene(generator, mapToScene(centerConn) - deltaPos );
}

void Connection::moveToCenteredPoint()
{
    auto rect = this->boundingRect();
    this->setPos(m_CenteredPosition.x()-rect.width()/2   ,
                 m_CenteredPosition.y()-rect.width()/2   );
    this->setTransformOriginPoint(rect.width()/2,  rect.height()/2);
}

void Connection::setCenteredPosition(QPointF pos)
{
   m_CenteredPosition = pos;
   moveToCenteredPoint();
}

void Connection::goToNextStyle( )
{
    if (m_CurrentConnectState == Connect){
        if (Style() == ConnectStyle::WithoutIsolation)
            setStyle(ConnectStyle::Isolation);
        else if (Style() == ConnectStyle::Isolation)
            setStyle(ConnectStyle::OverSizedIsolation);
        else if (Style() == ConnectStyle::OverSizedIsolation)
            setStyle(ConnectStyle::Simple);
        else
            setStyle(ConnectStyle::WithoutIsolation);
    }else if (m_CurrentConnectState == NoConnect){
        if (Style(NoConnect) == ConnectStyle::Cross){
            //m_NoConnectStyle = ConnectStyle::Triangle;
        //else if (Style() == ConnectStyle::Triangle){
            if(m_nameConn == "left" or m_nameConn == "startConn"){
                m_NoConnectStyle =  ConnectStyle::Dock;
                this->setRotation(0);
            }
            else if (m_nameConn == "right" or m_nameConn == "endConn"){
                m_NoConnectStyle = ConnectStyle::Dock;
                this->setRotation(180);
            }
            else{
                m_NoConnectStyle = ConnectStyle::Cross;
            }
        } else if (Style(NoConnect) == ConnectStyle::Dock){
            if(m_nameConn == "left" or m_nameConn == "startConn" or
                    m_nameConn == "right" or m_nameConn == "endConn"){
                m_NoConnectStyle = ConnectStyle::DotLine;
            }
            else{
                m_NoConnectStyle = ConnectStyle::Cross;
            }
         } else if (Style(NoConnect) == ConnectStyle::DotLine){
            m_NoConnectStyle = ConnectStyle::Cross;
            this->setRotation(0);
        }
    }


    for (auto & n_conn: m_Neighbour_connections){
        n_conn->setStyle(this->Style());
    }
}

void Connection::setStyle(const ConnectStyle connectStyle, const ConnectStyle NoConnectStyle) {
    m_ConnectStyle = connectStyle;
    m_NoConnectStyle = NoConnectStyle;
    changeConnectorView(m_CurrentConnectState);
}

void Connection::inheritStyle(Connection* otherConn) {
    if (otherConn->isInheritedStyle()){
        if ( this->isInheritedStyle() )
                setStyle(otherConn->Style());// перемешающие объекты БЕРУТ СТИЛЬ СТОЯЧИХ
    }
}
