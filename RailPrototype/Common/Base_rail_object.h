#ifndef BASE_RAIL_OBJECT_H
#define BASE_RAIL_OBJECT_H

//using namespace std;

#include <QObject>
#include <QGraphicsPathItem>
#include <QGraphicsSvgItem>
#include <QSvgRenderer>
#include <QGraphicsItemGroup>
#include <QGraphicsSceneDragDropEvent>
#include <QMimeData>
#include <QDrag>
#include <QMenu>
#include <QAction>
#include <QStandardItemModel>
#include <QTreeWidgetItem>
#include <QFile>
#include <QDebug>
#include <QMetaType>

#include "main.h"
#include "Registrate_base_rail_object.h"
//#include "TreeModel.h"
//#include "TreeItem.h"
#include "QJsonModel.h"
#include "Connection.h"
//#include "workplace.h"

class WorkPlace;

class QGraphicsSceneMouseEvent;
class QGraphicsSvgItem;
class QSvgRenderer;
class QWheelEvent;
class QPaintEvent;
class QStandardItem;

class Base_rail_object : public QGraphicsSvgItem
{
    Q_OBJECT
    Q_PROPERTY( QPointF previousPosition READ previousPosition WRITE setPreviousPosition NOTIFY
                    previousPositionChanged )
    Q_PROPERTY( QString Text READ text WRITE setText NOTIFY  textChanged )
    Q_PROPERTY( int     ID   READ ID   WRITE setID)


public:
    //using QGraphicsSvgItem::QGraphicsSvgItem;
    //explicit Base_rail_object() ;
    explicit Base_rail_object(const QMap<QString, QObject *> & GlobalContainerData_map  ={ {"AdressOK_model", nullptr} , {"A", nullptr} } , //
                              int ID = 0, QObject* parent = 0 );
    ~Base_rail_object();
    Base_rail_object(const Base_rail_object & );

    enum { Type = Main::Base_rail_objectType };
    int type() const override   { return Type; }

    static const QString                        jsonConteinerName;
    //static QStandardItem*                       initStandartItem();
    static Base_rail_object*                    createFromMime_static(const globalNet_t net, const QMimeData * mime);
    virtual QString                             JsonConteinerName() {return jsonConteinerName ;}
    static Base_rail_object*                    createFromJson_static(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj);
    virtual Base_rail_object*                   createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj) {return new Base_rail_object ;}
    static void                                 setBasePropertyFromJson(Base_rail_object* objItem, QJsonObject rail_obj);
    static void                                 setConnectionPropertyFromJson(QGraphicsItem & objItem, QJsonArray connectionsArray);
    QJsonObject                                 serializeBaseObj() const;
    void                                        serializeConnection(QGraphicsItem & objItem, QJsonArray & connectionsArray) const;
    virtual QJsonObject                         serialize();
    virtual void                                setPropertyFromJson( QJsonObject rail_obj );

    QSvgRenderer *                              m_renderer;
    //QList<  Connection *>                       Connections_list;
    QPointF                                     previousPosition() const;
    void                                        setPreviousPosition( const QPointF previousPosition );
    QString                                     text() const;
    QTransform                                  textTransform() const;
    void                                        setText( const QString);
    void                                        setPath( const QPainterPath& path );
    int                                         ID() const;
    void                                        setID( const int);
    void                                        cycleRotate();
    void                                        cycleMirror();
    void                                        setMirror(bool XAxis, bool YAxis);
    bool                                        mirrorX() const { return m_mirror_state[Qt::XAxis];  }
    bool                                        mirrorY() const { return m_mirror_state[Qt::YAxis];  }
    //QIcon icon() const { return m_icon;  }
    QStandardItem  *                           standardItem() const { return m_QStandardItem;  }
    QMap<QString, QObject *> &                 globalIF() { return m_GlobalContainerData_map;  }
    WorkPlace *                                workplace() const ;

    QJsonModel*                                propertyModel() const { return  m_PropertyModel;};
    void                                       setpropertyModel(QJsonModel * model) {m_PropertyModel = model;}
    void                                       ShowPropertyModel(void);
    //virtual void clone(int);
    void                                       disconnectAllItem(void);
    void                                       preparationToMove(QPointF startPoint);
    void                                       setMoveble(bool move) {m_moveble = move;}

signals:
    void previousPositionChanged();
    void textChanged();
    void clicked( Base_rail_object* rect );
    void signalMove( Base_rail_object* item, qreal dx, qreal dy );
    void childConnectionclicked(Connection* conn);
    void itemEditorActivated(Base_rail_object* item);


protected:
    void mouseMoveEvent( QGraphicsSceneMouseEvent* event ) override;
    void mousePressEvent( QGraphicsSceneMouseEvent* event ) override;
    void mouseReleaseEvent( QGraphicsSceneMouseEvent* event ) override;
    void hoverLeaveEvent( QGraphicsSceneHoverEvent* event ) override;
    void hoverMoveEvent( QGraphicsSceneHoverEvent* event ) override;
    void hoverEnterEvent( QGraphicsSceneHoverEvent* event ) override;
    void dragEnterEvent(QGraphicsSceneDragDropEvent *event) override;
    void dragLeaveEvent(QGraphicsSceneDragDropEvent *event) override;
    void dropEvent(QGraphicsSceneDragDropEvent *event) override;
    void mouseDoubleClickEvent( QGraphicsSceneMouseEvent* event ) override;
    //QVariant itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value) override;

protected:

    QJsonTreeItem * InitPropertyModel(void);
    void addPropertyFromFile(const QString &name);

public slots:
    void ChangedVisibleSlot(bool visible_state);
    void clickChildConnectionSlot(Connection* conn);
    virtual void deletedItemSlot(QGraphicsItem* item) { Q_UNUSED(item)};
    virtual void sceneActionChangedSlot(Main::Actions act) { Q_UNUSED(act)};
    virtual void dataChangedStot(const QModelIndex & topLeft, const QModelIndex & bottomRight,  const QVector<int> &roles = QVector<int>());

private slots:
    void slotMove( Base_rail_object* signalOwner, qreal dx, qreal dy );

private:
    //QGraphicsSvgItem *          m_svgItem;

    int                           m_ID;
    QPointF                       m_previousPosition;
    bool                          m_ena_move;
    bool                          m_moveble{true};
    int                           m_delta_moving = 2;
    QPainterPath                  Base_rail_objectPath;


protected:
    QGraphicsTextItem             m_text;
    QMenu                         m_ContextMenu;
    //QStandardItemModel Property_model;
    //TreeModel Property_model;
    //QStandardItemModel m_PropertyModel;
    QJsonModel                *   m_PropertyModel = nullptr;
//    QList<QTreeWidgetItem *>    PropertyItem_list;
//    QHash<QString, QVariant>    m_PropertyContainer;
    QMap<QString, QObject *>      m_GlobalContainerData_map;
    //QTransform                    transform;
    QMap<Qt::Axis, bool>           m_mirror_state = {  {Qt::XAxis, false} , {Qt::YAxis, false}  };
    //QIcon                         m_icon;
    QStandardItem  *               m_QStandardItem = nullptr;

};

Q_DECLARE_METATYPE(Base_rail_object)


#endif // BASE_RAIL_OBJECT_H
