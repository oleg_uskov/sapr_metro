
//#include "Base_rail_object.h"
#include "Registrate_base_rail_object.h"


/*****************************
* Регистрация типов
*/
QList< QString > &get_base_rail_object()
{
    static QList< QString > DerivedBaseRailList;
    return DerivedBaseRailList;
}

bool add_object( QString base)
{
    get_base_rail_object().push_back(base);
    return true;
}
