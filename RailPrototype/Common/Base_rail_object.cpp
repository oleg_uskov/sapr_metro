#include <QGraphicsSceneMouseEvent>
#include <QPainterPath>
#include <QGraphicsScene>
#include <QGraphicsPathItem>
#include <QGraphicsSvgItem>
#include <QDebug>
#include <QVariant>

#include "main.h"
#include "workplace.h"
#include "Base_rail_object.h"


//Base_rail_object::Base_rail_object() {

//};



Base_rail_object::Base_rail_object(const QMap<QString, QObject *> & GlobalContainerData_map, int ID, QObject *parent):
    m_ID (ID),
    m_text(""),
    m_GlobalContainerData_map(GlobalContainerData_map)
{
    Q_UNUSED(parent);
    setPos( QPointF(0,0));
    setAcceptHoverEvents( true );
    setFlags( ItemIsSelectable | ItemSendsScenePositionChanges | ItemSendsGeometryChanges  ); // | ItemIsMovable | ItemSendsScenePositionChanges| ItemSendsGeometryChanges)
//    setFlag(ItemSendsScenePositionChanges, false);
//    setFlag(ItemSendsGeometryChanges, false);
//    m_text.setFlag( ItemIgnoresTransformations, true );
//    m_text.setFlag( ItemSendsGeometryChanges, false );

    //connect(m_PropertyModel, &QJsonModel::dataChanged , this , &Base_rail_object::dataChangedStot);
}

Base_rail_object::~Base_rail_object()
{

}

Base_rail_object::Base_rail_object(const Base_rail_object & ref)
{
    //Base_rail_object::createFromJson( m_GlobalContainerData_map, ref.serializeBaseObj() );
    Base_rail_object::setBasePropertyFromJson(this ,  ref.serializeBaseObj());
}


const QString Base_rail_object::jsonConteinerName = "";

//QStandardItem * Base_rail_object::m_QStandardItem = nullptr;//Base_rail_object::initStandartItem();

//QStandardItem * Base_rail_object::initStandartItem(){
//    auto item = new QStandardItem(QIcon(":/tempalete/svg/switch.svg"),  "БАЗА");
//    item->setData(  Base_rail_object::staticMetaObject.className()  );
//    return item;
//}

Base_rail_object* Base_rail_object::createFromMime_static(const globalNet_t net, const QMimeData * mime)
{
    const QStringList formats = mime->formats();
    for (const QString &format : formats) {

        if (  format == QLatin1String("application/x-qabstractitemmodeldatalist")  ) {
            /*recteate object from MIME*/
            QByteArray encoded = mime->data(format);
            QDataStream stream(&encoded, QIODevice::ReadOnly);
            int row, col;
            //QMap<int,  QVariant> roleDataMap;
            QStandardItem item;
            while (!stream.atEnd())
            {
                 stream >> row >> col >> item;
            }

            auto typeName = item.data().toByteArray();
            int typeID= QMetaType::type(typeName);

            if (typeID != QMetaType::UnknownType) {
                WorkPlace * workPlace = static_cast<WorkPlace *>(net["workplaceScene"]);

                QJsonObject rail_obj {  {"id", workPlace->getUniqueID()},   };

                void * myClassPtr = QMetaType::create(typeID);
                auto odj = static_cast<Base_rail_object *>(myClassPtr);

                auto svgObj = odj->createFromJson(net, rail_obj);
                return svgObj;
            }
        } else if ( format == QLatin1String("application/x-qstandarditemmodeldatalist") ) {
            return nullptr;
        } else {
            return nullptr;
        }

    }
    return nullptr;
}

QPointF Base_rail_object::previousPosition() const
{
    return m_previousPosition;
}

void Base_rail_object::setPreviousPosition( const QPointF previousPosition )
{
    if ( m_previousPosition == previousPosition )
    {
        return;
    }

    m_previousPosition = previousPosition;
    emit previousPositionChanged();
}

QString Base_rail_object::text() const
{
    return m_text.toPlainText();
}

QTransform Base_rail_object::textTransform() const
{
    return m_text.transform();
}

void Base_rail_object::setText( const QString newText)
{
    m_text.setPlainText(newText) ;
    emit textChanged();
}

int Base_rail_object::ID() const
{
    return m_ID;
}

void Base_rail_object::setID( const int ID)
{
    m_ID = ID;
    auto  root =  m_PropertyModel->invisibleRootItem();
    for (auto & child: root->children()) {
        if ( child->key() == "Идентификатор") {
            child->setValue( QString::number(m_ID) );
        }
    }
}


void Base_rail_object::setPath( const QPainterPath& path )
{
    Base_rail_objectPath = path;
}


void Base_rail_object::mouseMoveEvent( QGraphicsSceneMouseEvent* event )
{
    if ( m_ena_move and m_moveble)
    {
        auto dx = event->scenePos().x() - m_previousPosition.x();
        auto dy = event->scenePos().y() - m_previousPosition.y();
        dx = dx - ((int)dx % m_delta_moving);
        dy = dy - ((int)dy % m_delta_moving);
        moveBy( dx, dy );
        //setPreviousPosition( event->scenePos() );
        setPreviousPosition(QPointF(dx,dy) + m_previousPosition);
        emit signalMove( this, dx, dy );
    }
    QGraphicsSvgItem::mouseMoveEvent( event );
}

void Base_rail_object::mousePressEvent( QGraphicsSceneMouseEvent* event )
{
    if ( event->button() & Qt::LeftButton )
    {
        preparationToMove( event->scenePos() );
        ShowPropertyModel();
        emit clicked( this );
    }

    QGraphicsSvgItem::mousePressEvent( event );
}

void Base_rail_object::mouseDoubleClickEvent( QGraphicsSceneMouseEvent* event )
{
    QGraphicsSvgItem::mouseDoubleClickEvent( event );
}


void Base_rail_object::mouseReleaseEvent( QGraphicsSceneMouseEvent* event )
{
    if ( event->button() & Qt::LeftButton )
    {
        m_ena_move = false;
    }

    QGraphicsSvgItem::mouseReleaseEvent( event );
}


void Base_rail_object::hoverLeaveEvent( QGraphicsSceneHoverEvent* event )
{
    QGraphicsSvgItem::hoverLeaveEvent( event );
}

void Base_rail_object::hoverMoveEvent( QGraphicsSceneHoverEvent* event )
{
    QGraphicsSvgItem::hoverMoveEvent( event );
}

void Base_rail_object::hoverEnterEvent( QGraphicsSceneHoverEvent* event )
{
    QGraphicsSvgItem::hoverEnterEvent( event );
}

void Base_rail_object::slotMove( Base_rail_object* signalOwner, qreal dx, qreal dy )
{
    Q_UNUSED(signalOwner);
    Q_UNUSED(dx);
    Q_UNUSED(dy);
    //QPainterPath Base_rail_objectPath = path();

//    for ( int i = 0; i < Base_rail_objectPath.elementCount(); i++ )
//    {
//        if ( listDotes.at( i ) == signalOwner )
//        {
//            QPointF pathPoint = Base_rail_objectPath.elementAt( i );
//            Base_rail_objectPath.setElementPositionAt( i, pathPoint.x() + dx, pathPoint.y() + dy );
//        }
//    }
//    setPath( Base_rail_objectPath );
}

//! [1]
void Base_rail_object::dragEnterEvent(QGraphicsSceneDragDropEvent *event)

{
    if (event->mimeData()->hasFormat("text/plain"))
            event->acceptProposedAction();
    else
        QGraphicsItem::dragEnterEvent(event);
}
//! [1]

//! [2]
void Base_rail_object::dragLeaveEvent(QGraphicsSceneDragDropEvent *event)
{
 //   update();
    QGraphicsItem::dragLeaveEvent(event);
}

void Base_rail_object::dropEvent(QGraphicsSceneDragDropEvent *event)
{
//    update();
    QGraphicsItem::dropEvent(event);
}

/*****************************
* Инициализация модели свойств
*/
void Base_rail_object::preparationToMove(QPointF startPoint)
{
    if (m_moveble){
        m_ena_move = true;
        setPreviousPosition( startPoint );
    }
}

/*****************************
* Инициализация модели свойств
*/
QJsonTreeItem * Base_rail_object::InitPropertyModel(void){
    m_PropertyModel = new QJsonModel;
    connect(m_PropertyModel, &QJsonModel::dataChanged , this , &Base_rail_object::dataChangedStot);
    QJsonTreeItem * root = m_PropertyModel->invisibleRootItem();
    return root;
}

/*****************************
* Отображения модели свойств
*/
void Base_rail_object::ShowPropertyModel()
{
    auto property_tree = static_cast<QTreeView *>(m_GlobalContainerData_map["propertyRail_tree"]);
    property_tree->setModel(m_PropertyModel);
    property_tree->expandAll();

}

void Base_rail_object::ChangedVisibleSlot(bool visible_state)
{
    this->setVisible(visible_state);
}

void Base_rail_object::cycleRotate(void)
{
    auto rect = this->boundingRect();
    this->setTransformOriginPoint(rect.width()/2,  rect.height()/2);
    this->setRotation(this->rotation() + 45);
}

/*!
    формируется последовательно отражение по оси Х, потом У
*/
void Base_rail_object::cycleMirror(void)
{
    this->resetTransform();
    m_text.resetTransform();
    auto rect = this->boundingRect();
    this->setTransform(QTransform::fromTranslate( rect.width()/2, rect.height()/2) , true);
    m_text.setTransform(QTransform::fromTranslate( m_text.boundingRect().width()/2,
                                                   m_text.boundingRect().height()/2 ) , true );

    if(!m_mirror_state[Qt::XAxis] && !m_mirror_state[Qt::YAxis]){
        this->setTransform(QTransform::fromScale(-1, 1) , true );
        m_text.setTransform(QTransform::fromScale(-1, 1) , true );
        m_mirror_state[Qt::XAxis] = true;
    }
    else if(m_mirror_state[Qt::XAxis] && !m_mirror_state[Qt::YAxis]){
        this->setTransform(QTransform::fromScale(-1, -1) , true );
        m_text.setTransform(QTransform::fromScale(-1, -1) , true );
        m_mirror_state[Qt::YAxis] = true;
    }
    else if(m_mirror_state[Qt::XAxis] && m_mirror_state[Qt::YAxis]){
        this->setTransform(QTransform::fromScale(1, -1) , true );
        m_text.setTransform(QTransform::fromScale(1, -1) , true );
        m_mirror_state[Qt::XAxis] = false;
    }
    else{
        this->setTransform(QTransform::fromScale(1, 1) , true );
        m_text.setTransform(QTransform::fromScale(1, 1) , true );
        m_mirror_state[Qt::XAxis] = false;
        m_mirror_state[Qt::YAxis] = false;
    }

    this->setTransform(QTransform::fromTranslate( -rect.width()/2, -rect.height()/2 )  , true );
    m_text.setTransform(QTransform::fromTranslate( -m_text.boundingRect().width()/2,
                                                   -m_text.boundingRect().height()/2 ) , true );

}

//QVariant Base_rail_object::itemChange(GraphicsItemChange change, const QVariant &value)
//{
//    return QGraphicsSvgItem::itemChange(change, value);
//}

void Base_rail_object::setMirror(bool XAxis, bool YAxis)
{
    this->resetTransform();
    m_text.resetTransform();
    auto rect = this->boundingRect();
    this->setTransform(QTransform::fromTranslate( rect.width()/2, rect.height()/2) , true);
    m_text.setTransform(QTransform::fromTranslate( m_text.boundingRect().width()/2,
                                                   m_text.boundingRect().height()/2 ) , true );

    m_mirror_state[Qt::XAxis] = XAxis;
    m_mirror_state[Qt::YAxis] = YAxis;
    this->setTransform(QTransform::fromScale( XAxis ? -1: 1,
                                             YAxis ? -1: 1)  , true );
    m_text.setTransform(QTransform::fromScale( XAxis ? -1: 1,
                                             YAxis ? -1: 1)  , true);

    this->setTransform(QTransform::fromTranslate( -rect.width()/2, -rect.height()/2 )  , true );
    m_text.setTransform(QTransform::fromTranslate( -m_text.boundingRect().width()/2,
                                                   -m_text.boundingRect().height()/2 ) , true );

}


void Base_rail_object::clickChildConnectionSlot(Connection* conn)
{
    emit childConnectionclicked(conn);
}

void Base_rail_object::disconnectAllItem()
{
    for (auto & child: childItems()) {
        if(Connection* conn = dynamic_cast<Connection*>( child )){
            for(auto & neig_conn: conn->Neighbour_connections()){
                neig_conn->disconnectWith(conn);
            }

        }
    }
}

void Base_rail_object::addPropertyFromFile(const QString &name) {
    //* Создаем модель из изменяемых свойств*//
    QString saveData;
    //auto openFile = QFile( name );
    QFile openFile(name);

    if(!openFile.open(QIODevice::ReadOnly)) {
        qDebug()<<"file not opened \n";
    }
    else
    {
        //qDebug()<<"file opened \n";
        saveData = openFile.readAll();
    }

    openFile.close();

    auto Jdoc = QJsonDocument::fromJson(saveData.toUtf8());
    QJsonObject JObject = Jdoc.object();
    QJsonObject obj = JObject["Properties"].toObject();
    m_PropertyModel->addJson(QJsonDocument(obj));
    //*********************************************************//
}

//Base_rail_object* Base_rail_object::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
//    auto base = new Base_rail_object(GlobalContainerData_map, 0);
//    setBasePropertyFromJson(base, rail_obj);
//    return base;
//}

void Base_rail_object::setBasePropertyFromJson(Base_rail_object* objItem, QJsonObject rail_obj){


    objItem->setX(rail_obj["posX"].toDouble());
    objItem->setY(rail_obj["posY"].toDouble());
    if (rail_obj.contains("text") ) {  objItem->setText(rail_obj["text"].toString()); }


    setConnectionPropertyFromJson(*objItem, rail_obj["Connections"].toArray());

    QJsonObject obj = rail_obj["Properties"].toObject();
    objItem->propertyModel()->updateFromJson(QJsonDocument(obj));

    objItem->ShowPropertyModel();
    objItem->setSelected(true);

    objItem->setMirror(rail_obj["mirrorX"].toBool() ,
                       rail_obj["mirrorY"].toBool());

    objItem->setRotation(rail_obj["angle"].toInt());
}

void Base_rail_object::setConnectionPropertyFromJson(QGraphicsItem & objItem, QJsonArray connectionsArray)
{
    //QJsonArray connectionsArray = rail_obj["Connections"].toArray();
    for (auto & child: objItem.childItems()) {
        if(Connection* conn = dynamic_cast<Connection*>( child )){
            for (auto connection: connectionsArray) {
                QJsonObject connect_obj = connection.toObject();
                if (connect_obj["name"].toString() == conn->m_nameConn){
                    Connection::ConnectStyle Connectionstyle = static_cast<Connection::ConnectStyle>( connect_obj["ConnectStyle"].toInt() );
                    Connection::ConnectStyle NoConnectionstyle = static_cast<Connection::ConnectStyle>( connect_obj["NoConnectStyle"].toInt() );
                    conn->setStyle( Connectionstyle,  NoConnectionstyle);
                }
            }
        }
//        else {
//            setConnectionPropertyFromJson(*child , connectionsArray);
//        }
    }
}

QJsonObject Base_rail_object::serializeBaseObj() const
{
    QJsonObject rail_obj;
    rail_obj["type"] = metaObject()->className();
    rail_obj["id"]   = m_ID;
    rail_obj["posX"] = x();
    rail_obj["posY"] = y();
    rail_obj["angle"] = rotation();
    rail_obj["mirrorX"] = mirrorX();
    rail_obj["mirrorY"] = mirrorY();
    rail_obj["text"] = text();

    QJsonArray connectionsArray;
//    QJsonArray trainsArray;
//    QJsonObject endPointObj;
    for (auto & child: this->childItems()) {
        if(Connection* conn = dynamic_cast<Connection*>( child )){
            QJsonObject connect_obj;
            connect_obj["name"]         = conn->m_nameConn;
            connect_obj["ConnectTypes"] = conn->State();
            connect_obj["ConnectStyle"] = conn->Style(Connection::Connect);
            connect_obj["NoConnectStyle"] = conn->Style(Connection::NoConnect);
            QJsonArray NeighbourConnectionsArray;
            for (auto & neib: conn->Neighbour_connections()) {
                QJsonObject connect_neibJson;
                connect_neibJson["parent_id"]    = neib->m_parent_ID;
                connect_neibJson["name"]         = neib->m_nameConn;
                NeighbourConnectionsArray.append(connect_neibJson);
            }
            connect_obj["WhoIsConnected"] = NeighbourConnectionsArray;

            connectionsArray.append(connect_obj);
        }
    }

//    auto copy_this = *this;
//    serializeConnection(copy_this, connectionsArray);
    rail_obj["Connections"] = connectionsArray;

    auto PropertyOK_model = this->propertyModel();
    if (PropertyOK_model)
        rail_obj["Properties"] = PropertyOK_model->json().object();

    return rail_obj;
}


void Base_rail_object::setPropertyFromJson( QJsonObject rail_obj ){
    Base_rail_object::setBasePropertyFromJson(this, rail_obj);
}

QJsonObject Base_rail_object::serialize()
{
    QJsonObject rail_obj = Base_rail_object::serializeBaseObj();
    return rail_obj;

}


/*****************************
* Изменяем текст объекта на сцене
*/
void Base_rail_object::dataChangedStot(const QModelIndex & topLeft, const QModelIndex & bottomRight,  const QVector<int> &roles){

    auto display_data = m_PropertyModel->data(topLeft, Qt::DisplayRole);
    auto edit_data = m_PropertyModel->data(topLeft, Qt::EditRole);

    if (!display_data.isValid()) // не изменяем пустые ячейки
        return;
    auto textIndex = m_PropertyModel->index(0, 1);
    if (topLeft == textIndex)
        setText(edit_data.toString());
}

WorkPlace * Base_rail_object::workplace() const {
    return static_cast<WorkPlace *>(m_GlobalContainerData_map["workplaceScene"]);
}
