#ifndef Station_H
#define Station_H


#include "Base_rail_object.h"

class Station : public Base_rail_object
{
    Q_OBJECT


public:
    //using Base_rail_object::Base_rail_object;
    Station() {};
    explicit Station(const QMap<QString, QObject *> & GlobalContainerData_map, int ID = 0, QObject* parent = 0 );
    ~Station();
    Station(const Station &) = default;

//    enum { Type = Main::StationType };
//    int type() const override   { return Type; }

    static const QString jsonConteinerName;
    QString JsonConteinerName() override {return jsonConteinerName ;}
    Station* createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj) override;
    //void setPropertyFromJson( QJsonObject rail_obj );

signals:



protected:

    void InitPropertyView(void);


private slots:

protected:
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override;


private:


    static int m_cnt_item;

signals:

};

Q_DECLARE_METATYPE(Station)

#endif // Station_H
