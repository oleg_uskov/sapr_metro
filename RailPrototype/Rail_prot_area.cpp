#include <QGraphicsScene>
#include <QDebug>
#include "Rail_prot_area.h"
//#include "Protect_area.h"

Rail_prot_area::Rail_prot_area(const QMap<QString, QObject *> & GlobalContainerData_map, int ID,  QObject *parent) :
    Base_rail_object( GlobalContainerData_map , ID , parent)
{
    m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/protect_area.svg"));
    this->setSharedRenderer(m_renderer);
    m_cnt_item++;
    setText(QString::number(m_cnt_item) + "ЗУ");
    m_text.setPos(QPointF(30,5));
    m_text.setParentItem(this);

    Connection *Dot = new Connection(QPointF(5,70), ID,"ProtAreaConn");
    Dot->setParentItem(this);
    setFlags( ItemIsSelectable | ItemSendsGeometryChanges );
}


Rail_prot_area::~Rail_prot_area()
{

}

const QString Rail_prot_area::jsonConteinerName = "protAreas";
int Rail_prot_area::m_cnt_item = 0;


void Rail_prot_area::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{

    //qDebug() << "Date:"  << elementId();
    m_ContextMenu.exec(event->screenPos());


}

Rail_prot_area* Rail_prot_area::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    auto item = new Rail_prot_area(GlobalContainerData_map, rail_obj["id"].toInt());
    setBasePropertyFromJson(item, rail_obj);
    return item;
}

//void Rail_switch::setPropertyFromJson( QJsonObject rail_obj ){
//    Base_rail_object::setBasePropertyFromJson(this, rail_obj);
//}


