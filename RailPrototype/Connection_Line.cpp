#include "Connection_Line.h"
//#include "workplace.h"
//#include "Connection.h"
#include <QPen>

Connection_Line::Connection_Line(const QMap<QString, QObject *> & GlobalContainerData_map, int ID, QObject *parent) :
    Base_rail_object( GlobalContainerData_map , ID , parent),
    m_startPoint(0,0)

{
    m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/whitesheet.svg"));
    this->setSharedRenderer(m_renderer);
    setFlags( ItemIsSelectable   ); //| ItemSendsGeometryChanges

    m_line.setP1( m_startPoint );
    m_line.setP2( QPointF(10,0) );

//    m_backgroundLineItem = new QGraphicsLineItem(m_line);
//    m_backgroundLineItem->setPen( QPen(Qt::white, 50) );
//    m_backgroundLineItem->setParentItem(this);

    m_lineItem = new QGraphicsLineItem(m_line);
    m_lineItem->setPen( QPen(Qt::black, 1.5) );
    //m_lineItem->setLine(m_line);
    //m_lineItem->setFlags(ItemIsSelectable | ItemSendsGeometryChanges);
    //this->setSelected(true);
    m_lineItem->setParentItem(this);

    m_startConnection = new Connection(QPointF(0,0), ID, "startConn");
    m_startConnection->setStyle(Connection::ConnectStyle::Simple);
    m_startConnection->setParentItem(this);
    connect( m_startConnection , &Connection::ConnectionClicked, this, &Base_rail_object::clickChildConnectionSlot );

    m_endConnection = new Connection(QPointF(10,0) , ID, "endConn");
    m_endConnection->setStyle(Connection::ConnectStyle::Simple);
    m_endConnection->setParentItem(this);
    connect( m_endConnection , &Connection::ConnectionClicked, this, &Base_rail_object::clickChildConnectionSlot );

    InitPropertyView();
    Base_rail_object::ShowPropertyModel();
}

Connection_Line::~Connection_Line()
{
    //delete     m_renderer;
    delete     m_lineItem;
}

const QString Connection_Line::jsonConteinerName = "connectionLines";
//int Connection_Line::m_cnt_item = 100;


void Connection_Line::mousePressEvent( QGraphicsSceneMouseEvent* event )
{
    Base_rail_object::mousePressEvent( event );
}

void Connection_Line::setEndPoint(QPointF endPoint)
{
    //* Создаем модель из неизменяемых свойств*//
    m_endConnection->setCenteredPosition(mapFromScene(endPoint));
    m_line.setP2(mapFromScene(endPoint));
    //m_backgroundLineItem->setLine(m_line);
    m_lineItem->setLine(m_line);

//    m_startConnection->setRotation(m_line.angle() - 180);
//    m_endConnection->setRotation(m_line.angle() - 180);

//    qDebug() << m_line.angle();

    ////////////////////////////////////////////////////////////////
}

QRectF Connection_Line::boundingRect() const
{
    //return m_lineItem->boundingRect();
    return shape().controlPointRect();
}

QPainterPath Connection_Line::shape() const
{
    QPointF tl, tr, bl, br ;

    if ( (m_line.angle() > 45 and m_line.angle() < 135) || (m_line.angle() > 225 and m_line.angle() < 315) )
    {
        tl = m_line.p1() + QPointF(-10, 0);
        tr = m_line.p2() + QPointF(-10, 0);
        bl = m_line.p1() + QPointF( 10, 0);
        br = m_line.p2() + QPointF( 10, 0);
    }
    else {
        tl = m_line.p1() + QPointF(0, -10);
        tr = m_line.p2() + QPointF(0, -10);
        bl = m_line.p1() + QPointF(0, 10);
        br = m_line.p2() + QPointF(0, 10);
    }


    QPainterPath path(tl);
    path.lineTo(tr);
    path.lineTo(br);
    path.lineTo(bl);
    return path;
    //return m_lineItem->shape();
}

//void Connection_Line::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
//{
//    m_lineItem->paint(painter, option, widget);
//}

void Connection_Line::InitPropertyView()
{
    //* Создаем модель из неизменяемых свойств*//
    QJsonTreeItem *root = InitPropertyModel();


    new QJsonTreeItem("Имя секции", "соединительная линия", root);
    new QJsonTreeItem("Идентификатор", QString::number(ID()) , root);
    new QJsonTreeItem("Прозрачность", QString::number(1) , root);

    connect(m_PropertyModel, &QJsonModel::dataChanged , this , &Connection_Line::dataChangedStot);
    ////////////////////////////////////////////////////////////////
}


Connection_Line* Connection_Line::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    auto item = new Connection_Line(GlobalContainerData_map, rail_obj["id"].toInt());
    item->setPropertyFromJson(rail_obj);
    return item;
}

void Connection_Line::setPropertyFromJson( QJsonObject rail_obj ){

    if (rail_obj.contains("EndPoint") && rail_obj["EndPoint"].isObject()){
       QJsonObject EndPointObj = rail_obj["EndPoint"].toObject();
       this->setEndPoint( QPointF( EndPointObj["endPointX"].toDouble() ,
                                       EndPointObj["endPointY"].toDouble()) );
       m_lineItem->setOpacity(rail_obj["Opacity"].toDouble());
    }

    Base_rail_object::setBasePropertyFromJson(this, rail_obj);
}

QJsonObject Connection_Line::serialize()
{
    QJsonObject rail_obj = Base_rail_object::serializeBaseObj();

    QJsonObject endPointObj;
    endPointObj["endPointX"]        = m_line.p2().x();
    endPointObj["endPointY"]        = m_line.p2().y();
    rail_obj["EndPoint"] =          endPointObj;
    rail_obj["Opacity"] =           m_lineItem->opacity();

    return rail_obj;

}


/*****************************
* Изменяем прозрачность объекта на сцене
*/
void Connection_Line::dataChangedStot(const QModelIndex & topLeft, const QModelIndex & bottomRight,  const QVector<int> &roles){

    //auto display_data = m_PropertyModel->data(topLeft, Qt::DisplayRole);
    auto edit_data = m_PropertyModel->data(topLeft, Qt::EditRole);
    auto key = m_PropertyModel->data(topLeft.siblingAtColumn(0), Qt::DisplayRole);
//    if (!display_data.isValid()) // не изменяем пустые ячейки
//        return;

    auto opacity = edit_data.toFloat();
    if (key.toString() == "Прозрачность")
        m_lineItem->setOpacity(opacity);

    Base_rail_object::dataChangedStot(topLeft, bottomRight, roles);
}



