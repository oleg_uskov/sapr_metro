#include <QGraphicsScene>
#include <QDebug>
#include <QCheckBox>
#include "Rail_generatorTRC.h"
#include "Connection.h"

Rail_generatorTRC::Rail_generatorTRC()
{
    if (!m_QStandardItem){
        m_QStandardItem = new QStandardItem(QIcon(":/tempalete/svg/generatorTRC.svg"),  "Питающий конец");
        m_QStandardItem->setData(  metaObject()->className()  );
    }
};

Rail_generatorTRC::Rail_generatorTRC(const QMap<QString, QObject *> & GlobalContainerData_map, int ID,  QObject *parent) :
    Base_rail_object( GlobalContainerData_map , ID , parent)
{
    m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/generatorTRC.svg"));
    this->setSharedRenderer(m_renderer);
    //setAcceptDrops(true);
    m_cnt_item++;
    auto rect = this->boundingRect();
    setText("780/12");
    m_text.setPos(QPointF( 0 , rect.height() - 20) );
    m_text.setParentItem(this);

    m_generatorTRCConn = new Connection(QPointF(rect.width()/2, 0), ID, "generatorTRCConn");
    m_generatorTRCConn->setStyle(Connection::Simple);
    m_generatorTRCConn->setFlagInheritedStyle(false);
    m_generatorTRCConn->setParentItem(this);

    auto cb = static_cast<QCheckBox *>( GlobalContainerData_map["show_generator_chbox"] );
    if (cb){
        connect(cb, &QCheckBox::stateChanged, this, &Rail_generatorTRC::ChangedVisibleSlot) ;
    }

    InitPropertyView();
    Base_rail_object::ShowPropertyModel();

}


Rail_generatorTRC::~Rail_generatorTRC()
{

}

const QString Rail_generatorTRC::jsonConteinerName = "Rail_generatorTRCs";
int Rail_generatorTRC::m_cnt_item = 0;


void Rail_generatorTRC::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{

    //qDebug() << "Date:"  << elementId();
    //ContextMenu.exec(event->screenPos());
}


void Rail_generatorTRC::InitPropertyView()
{
    //* Создаем модель из неизменяемых свойств*//
    QJsonTreeItem *root = InitPropertyModel();

    new QJsonTreeItem("Частота несущая/модуляции", text(), root);
    new QJsonTreeItem("Идентификатор", QString::number(ID()) , root);

    addPropertyFromFile(":/tempalete/json/generatorTRC.json");
    //addPropertyFromFile("../SAPR_Metro/tempalete/json/generatorTRC.json");
}

Rail_generatorTRC* Rail_generatorTRC::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    auto item = new Rail_generatorTRC(GlobalContainerData_map, rail_obj["id"].toInt());
    setBasePropertyFromJson(item, rail_obj);
    return item;
}

