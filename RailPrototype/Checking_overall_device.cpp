
#include "Checking_overall_device.h"


Checking_overall_device::Checking_overall_device()
{
    if (!m_QStandardItem){
        m_QStandardItem = new QStandardItem(QIcon(":/tempalete/svg/checking_overall_device.svg"),  "КГУ");
        m_QStandardItem->setData(  metaObject()->className()  );
    }
};

Checking_overall_device::Checking_overall_device(const QMap<QString, QObject *> & GlobalContainerData_map, int ID,  QObject *parent) :
    Base_rail_object( GlobalContainerData_map , ID , parent)
{
    m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/checking_overall_device.svg"));
    //m_renderer = new QSvgRenderer(QLatin1String("../SAPR_Metro/tempalete/svg/сhecking_overall_device.svg"));
    this->setSharedRenderer(m_renderer);
    //setAcceptDrops(true);
    m_cnt_item++;

//    setText("стр" + QString::number(m_cnt_item));
//    m_text.setPos(QPointF(0,0));
//    m_text.setParentItem(this);

    //auto rect = this->boundingRect();
    auto Dot = new Connection(QPointF(-45, 25), ID, "leftOveralldeviceConn");
    Dot->setStyle(Connection::ConnectStyle::Simple);
    Dot->setFlagInheritedStyle(false);
    Dot->setParentItem(this);
    Dot = new Connection(QPointF(65 , 25), ID, "RightOveralldeviceConn");
    Dot->setStyle(Connection::ConnectStyle::Simple);
    Dot->setFlagInheritedStyle(false);
    Dot->setParentItem(this);

    InitPropertyView();
    Base_rail_object::ShowPropertyModel();
}

Checking_overall_device::~Checking_overall_device()
{

}

const QString Checking_overall_device::jsonConteinerName = "Checking_overall_devices";
int Checking_overall_device::m_cnt_item = 0;

void Checking_overall_device::InitPropertyView()
{
    //* Создаем модель из неизменяемых свойств*//
    QJsonTreeItem *root = InitPropertyModel();

    new QJsonTreeItem("Имя КГУ", text(), root);
    new QJsonTreeItem("Идентификатор", QString::number(ID()) , root);
    //*********************************************************//

    //* Создаем модель из изменяемых свойств*//
    //addPropertyFromFile("../SAPR_Metro/tempalete/json/checking_overall_device.json");
    addPropertyFromFile(":/tempalete/json/checking_overall_device.json");
    //*********************************************************//

}

Checking_overall_device* Checking_overall_device::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    auto item = new Checking_overall_device(GlobalContainerData_map, rail_obj["id"].toInt());
    setBasePropertyFromJson(item, rail_obj);
    return item;
}

//void Checking_overall_device::setPropertyFromJson( QJsonObject rail_obj ){
//    Base_rail_object::setBasePropertyFromJson(this, rail_obj);
//}

