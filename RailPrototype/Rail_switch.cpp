
#include "Rail_switch.h"


Rail_switch::Rail_switch()
{
    if (!m_QStandardItem){
        m_QStandardItem = new QStandardItem(QIcon(":/tempalete/svg/switch.svg"),  "Стрелка");
        m_QStandardItem->setData(  metaObject()->className()  );
    }
};

Rail_switch::Rail_switch(const QMap<QString, QObject *> & GlobalContainerData_map, int ID,  QObject *parent) :
    Base_rail_object( GlobalContainerData_map , ID , parent)
{
    m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/switch.svg"));
    //m_renderer = new QSvgRenderer(QLatin1String("../SAPR_Metro/tempalete/svg/switch.svg"));
    this->setSharedRenderer(m_renderer);
    //setAcceptDrops(true);
    m_cnt_item++;

    setText("стр" + QString::number(m_cnt_item));
    m_text.setPos(QPointF(0,0));
    m_text.setParentItem(this);

    auto rect = this->boundingRect();
    Connection *Dot = new Connection(QPointF(-2,26), ID, "commonEnd");
    Dot->setParentItem(this);
    Dot->setStyle(Connection::ConnectStyle::Simple);
    connect( Dot , &Connection::ConnectionClicked, this, &Base_rail_object::clickChildConnectionSlot);

    Dot = new Connection(QPointF(rect.width()+2,26), ID, "plusEnd");
    Dot->setStyle(Connection::ConnectStyle::Simple);
    Dot->setParentItem(this);
    connect( Dot , &Connection::ConnectionClicked, this, &Base_rail_object::clickChildConnectionSlot);

    Dot = new Connection(QPointF(rect.width()+1,1), ID, "minusEnd");
    Dot->setStyle(Connection::ConnectStyle::Simple);
    Dot->setParentItem(this);
    //Dot->setRotation(-45);
    connect( Dot , &Connection::ConnectionClicked, this, &Base_rail_object::clickChildConnectionSlot);

    InitPropertyView();
    Base_rail_object::ShowPropertyModel();
}

Rail_switch::~Rail_switch()
{

}

const QString Rail_switch::jsonConteinerName = "switches";
int Rail_switch::m_cnt_item = 0;

void Rail_switch::InitPropertyView()
{
    //* Создаем модель из неизменяемых свойств*//
    QJsonTreeItem *root = InitPropertyModel();

    new QJsonTreeItem("Имя стрелки", text(), root);
    new QJsonTreeItem("Идентификатор", QString::number(ID()) , root);
    //*********************************************************//

    //* Создаем модель из изменяемых свойств*//
    //addPropertyFromFile("../SAPR_Metro/tempalete/json/switch.json");
    addPropertyFromFile(":/tempalete/json/switch.json");
    //*********************************************************//

}

Rail_switch* Rail_switch::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    auto item = new Rail_switch(GlobalContainerData_map, rail_obj["id"].toInt());
    setBasePropertyFromJson(item, rail_obj);
    return item;
}

//void Rail_switch::setPropertyFromJson( QJsonObject rail_obj ){
//    Base_rail_object::setBasePropertyFromJson(this, rail_obj);
//}

