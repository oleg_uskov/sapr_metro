
#include "Metall_construction.h"


Metall_construction::Metall_construction()
{
    if (!m_QStandardItem){
        m_QStandardItem = new QStandardItem(QIcon(":/tempalete/svg/metall_construction.svg"),  "Металлоконструкция");
        m_QStandardItem->setData(  metaObject()->className()  );
    }
};

Metall_construction::Metall_construction(const QMap<QString, QObject *> & GlobalContainerData_map, int ID,  QObject *parent) :
    Base_rail_object( GlobalContainerData_map , ID , parent)
{
    m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/metall_construction.svg"));
    //m_renderer = new QSvgRenderer(QLatin1String("../SAPR_Metro/tempalete/svg/metall_construction.svg"));
    this->setSharedRenderer(m_renderer);
    //setAcceptDrops(true);
    m_cnt_item++;

//    setText("стр" + QString::number(m_cnt_item));
//    m_text.setPos(QPointF(0,0));
//    m_text.setParentItem(this);

    //auto rect = this->boundingRect();
    auto Dot = new Connection(QPointF(-45,-25), ID, "leftMetallConn");
    Dot->setStyle(Connection::ConnectStyle::Simple);
    Dot->setFlagInheritedStyle(false);
    Dot->setParentItem(this);
    Dot = new Connection(QPointF(65 , -25), ID, "RightMetallConn");
    Dot->setStyle(Connection::ConnectStyle::Simple);
    Dot->setFlagInheritedStyle(false);
    Dot->setParentItem(this);

    InitPropertyView();
    Base_rail_object::ShowPropertyModel();
}

Metall_construction::~Metall_construction()
{

}

const QString Metall_construction::jsonConteinerName = "metall_constructions";
int Metall_construction::m_cnt_item = 0;

void Metall_construction::InitPropertyView()
{
    //* Создаем модель из неизменяемых свойств*//
    QJsonTreeItem *root = InitPropertyModel();

    new QJsonTreeItem("Имя металлоконструкции", text(), root);
    new QJsonTreeItem("Идентификатор", QString::number(ID()) , root);
    //*********************************************************//

    //* Создаем модель из изменяемых свойств*//
    //addPropertyFromFile("../SAPR_Metro/tempalete/json/metall_construction.json");
    addPropertyFromFile(":/tempalete/json/metall_construction.json");
    //*********************************************************//

}

Metall_construction* Metall_construction::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    auto item = new Metall_construction(GlobalContainerData_map, rail_obj["id"].toInt());
    setBasePropertyFromJson(item, rail_obj);
    return item;
}

//void Metall_construction::setPropertyFromJson( QJsonObject rail_obj ){
//    Base_rail_object::setBasePropertyFromJson(this, rail_obj);
//}

