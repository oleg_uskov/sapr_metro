#ifndef Neigh_station_H
#define Neigh_station_H


#include "Base_rail_object.h"

class Neigh_station : public Base_rail_object, public Registrate_base_rail_object<Neigh_station>
{
    Q_OBJECT


public:
    //using Base_rail_object::Base_rail_object;
    Neigh_station();
    explicit Neigh_station(const QMap<QString, QObject *> & GlobalContainerData_map, int ID = 0, QObject* parent = 0 );
    ~Neigh_station();
    Neigh_station(const Neigh_station &) = default;

//    enum { Type = Main::Neigh_stationType };
//    int type() const override   { return Type; }

    static const QString jsonConteinerName;
    QString JsonConteinerName() override {return jsonConteinerName ;}
    Neigh_station* createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj) override;
    //void setPropertyFromJson( QJsonObject rail_obj );

signals:



protected:

    void InitPropertyView(void);


private slots:

protected:
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override;


private:


    static int m_cnt_item;

signals:

};

Q_DECLARE_METATYPE(Neigh_station)

#endif // Neigh_station_H
