#include "Rail_station_edge.h"

Rail_station_edge::Rail_station_edge()
{
    if (!m_QStandardItem){
        m_QStandardItem = new QStandardItem(QIcon(":/tempalete/svg/edge_of_station.svg"),  "Граница управления");
        m_QStandardItem->setData(  metaObject()->className()  );
    }
};


Rail_station_edge::Rail_station_edge(const QMap<QString, QObject *> & GlobalContainerData_map, int ID,  QObject *parent) :
    Base_rail_object( GlobalContainerData_map , ID , parent)
{
    m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/edge_of_station.svg"));
    this->setSharedRenderer(m_renderer);
//    m_cnt_item++;
//    setText(QString::number(m_cnt_item) + "ЗУ");
//    m_text.setPos(QPointF(30,5));
//    m_text.setParentItem(this);

    auto rect = this->boundingRect();
    Connection *Dot = new Connection(QPointF(rect.width()/2-2, 100), ID, "StationEdgeConn");
    Dot->setStyle(Connection::ConnectStyle::Simple);
    Dot->setFlagInheritedStyle(false);
    Dot->setParentItem(this);
    setFlags( ItemIsSelectable | ItemSendsGeometryChanges );

    InitPropertyView();
    Base_rail_object::ShowPropertyModel();
}


Rail_station_edge::~Rail_station_edge()
{

}


const QString Rail_station_edge::jsonConteinerName = "stationEdges";
int Rail_station_edge::m_cnt_item = 0;

void Rail_station_edge::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
}


void Rail_station_edge::InitPropertyView()
{
    //* Создаем модель из неизменяемых свойств*//
    QJsonTreeItem *root = InitPropertyModel();

    new QJsonTreeItem("Имя границы управления", text(), root); // всегда должен быть первым с списке
    new QJsonTreeItem("Идентификатор", QString::number(ID()) , root);
}

Rail_station_edge* Rail_station_edge::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    auto item = new Rail_station_edge(GlobalContainerData_map, rail_obj["id"].toInt());
    setBasePropertyFromJson(item, rail_obj);
    return item;
}

//void Rail_switch::setPropertyFromJson( QJsonObject rail_obj ){
//    Base_rail_object::setBasePropertyFromJson(this, rail_obj);
//}
