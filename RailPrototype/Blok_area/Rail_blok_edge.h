#ifndef RAIL_BLOK_EDGE_H
#define RAIL_BLOK_EDGE_H


//#include "Base_rail_object.h"
#include <QGraphicsSvgItem>

class Rail_blok_edge : public QGraphicsSvgItem //Base_rail_object
{
    Q_OBJECT


public:

    explicit Rail_blok_edge() {};
    explicit Rail_blok_edge(int parentID );
    ~Rail_blok_edge();
//    Rail_blok_edge(const Rail_blok_edge &) = default;

//    enum { Type = Main::Rail_blok_edgeType };
//    int type() const override   { return Type; }

//    static const QString jsonConteinerName;
//    QString JsonConteinerName() override {return jsonConteinerName ;}
//    Rail_blok_edge* createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj) override;
//    void setPropertyFromJson( QJsonObject rail_obj );
    int parentID( ) const {return  m_parent_ID ;}
    void setMirror(bool XAxis, bool YAxis);


signals:
    void clicked( QGraphicsSvgItem* rect );
    void signalMove( QGraphicsSvgItem* item, qreal dx, qreal dy );
    //void childConnectionclicked(Connection* conn);

private slots:
    //void addProtect_area(void);

protected:
    void mouseMoveEvent( QGraphicsSceneMouseEvent* event ) override;
    void mousePressEvent( QGraphicsSceneMouseEvent* event ) override;
    void mouseReleaseEvent( QGraphicsSceneMouseEvent* event ) override;



private:
    int                           m_parent_ID;
    QPointF                       m_previousPosition;
    bool                          m_leftMouseButtonPressed;
    int                           m_delta_moving = 2;
    QMap<Qt::Axis, bool>          m_mirror_state = {  {Qt::XAxis, false} , {Qt::YAxis, false}  };


};

//Q_DECLARE_METATYPE(Rail_blok_edge)

#endif // RAIL_BLOK_EDGE_H
