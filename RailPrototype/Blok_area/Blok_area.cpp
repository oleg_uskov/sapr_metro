#include <QGraphicsScene>
#include <QDebug>
#include <QRect>
#include <QCheckBox>
#include "Blok_area.h"
#include "Connection.h"
#include "Rail_blok_edge.h"
#include "workplace.h"

Blok_area::Blok_area()
{
    if (!m_QStandardItem){
        m_QStandardItem = new QStandardItem(QIcon(":/tempalete/svg/edge_blok_area.svg"),  "Блок-участок");
        m_QStandardItem->setData(  metaObject()->className()  );
    }
};

Blok_area::Blok_area(const QMap<QString, QObject *> & GlobalContainerData_map, int ID,  QObject *parent) :
    Base_rail_object( GlobalContainerData_map , ID , parent)
{
    m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/whitesheet.svg"));
    this->setSharedRenderer(m_renderer);

    m_line.setP1(QPointF(0,0));
    m_line.setP2(QPointF(200,0));

    m_lineItem = new QGraphicsLineItem(m_line);
    m_lineItem->setPen( QPen(Qt::gray, 1, Qt::DashLine) );


    m_left_blokEdge = new Rail_blok_edge(ID);
    m_right_blokEdge = new Rail_blok_edge(ID);

    auto rect = m_left_blokEdge->boundingRect();
    m_left_blokEdge->setParentItem(this);
    m_left_blokEdge->setX(m_line.p1().x() - rect.width()/2);

    m_right_blokEdge->setParentItem(this);
    m_right_blokEdge->setX(m_line.p2().x() - rect.width()/2);
    m_right_blokEdge->setMirror(true,false);

    connect( m_left_blokEdge  , &Rail_blok_edge::signalMove, this, &Blok_area::blokEdgeMoveSlot );
    connect( m_right_blokEdge , &Rail_blok_edge::signalMove, this, &Blok_area::blokEdgeMoveSlot );

    m_left_dot = new Connection(QPointF(m_line.p1().x()-9,
                                             rect.height()), ID, "LeftEdgeBlokConn");
    m_left_dot->setStyle(Connection::ConnectStyle::Simple);
    m_left_dot->setFlagInheritedStyle(false);
    m_left_dot->setParentItem(this);

    m_right_dot = new Connection(QPointF(m_line.p2().x()+9,
                                             rect.height()), ID, "RightEdgeBlokConn");
    m_right_dot->setStyle(Connection::ConnectStyle::Simple);
    m_right_dot->setFlagInheritedStyle(false);
    m_right_dot->setParentItem(this);

    m_cnt_item++;

    setText(QString::number(m_cnt_item) + "БУ");
    m_text.setPos( m_line.center() );
    m_text.setParentItem(this);
    m_lineItem->setParentItem(this);

    auto cb = static_cast<QCheckBox *>( GlobalContainerData_map["show_BU_chbox"] );
    if (cb){
        connect(cb, &QCheckBox::stateChanged, this, &Blok_area::ChangedVisibleSlot) ;
    }

    InitPropertyView();
    Base_rail_object::ShowPropertyModel();
}


Blok_area::~Blok_area()
{

}

const QString Blok_area::jsonConteinerName = "Blok_areas";
int Blok_area::m_cnt_item = 0;


QRectF Blok_area::boundingRect() const
{
    return shape().controlPointRect();
}

QPainterPath Blok_area::shape() const
{
    auto tl = m_line.p1(); // m_left_blokEdge->mapToParent(  m_left_blokEdge->boundingRect().topLeft()   )
    auto bl = m_text.mapToParent(  m_text.boundingRect().bottomLeft()   );
    auto br = m_text.mapToParent(  m_text.boundingRect().bottomRight()  );
    auto tr = m_line.p2(); // right_blokEdge->mapToParent( m_right_blokEdge->boundingRect().topRight()  )

    QPainterPath path(tl);
    path.lineTo(tr);
    path.lineTo(br);
    path.lineTo(bl);
    //path.addRect(  QRectF(tl , br) ); //
    return path;
}


void Blok_area::blokEdgeMoveSlot( QGraphicsSvgItem* signalOwner, qreal dx, qreal dy )
{
    if (signalOwner == m_left_blokEdge){
//        m_line.setP1(m_line.p1() +  QPointF(dx,dy));
//        m_lineItem->setLine(m_line);
//        m_text.setPos( m_line.center());
//        m_left_dot->setCenteredPosition(m_left_dot->centeredScenePosition() +  QPointF(dx,dy));
        m_left_blokEdge->moveBy( -dx, -dy);
        this->moveBy( dx, dy);
    } else if (signalOwner == m_right_blokEdge){
         m_line.setP2(m_line.p2() +  QPointF(dx,dy));
         m_lineItem->setLine(m_line);
         m_text.setPos( m_line.center());
         m_right_dot->setCenteredPosition(m_right_dot->centeredScenePosition() +  QPointF(dx,dy));
    }

    auto  workplace  = this->workplace();
//    if (isSelected()){
//        if (signalOwner == m_left_blokEdge){
//            m_right_blokEdge->moveBy( dx, dy );
//            workplace->find_intersect_connection(m_right_blokEdge);
//        }
//        else if (signalOwner == m_right_blokEdge){
//            m_left_blokEdge->moveBy( dx, dy );
//            workplace->find_intersect_connection(m_left_blokEdge);
//        }
//        emit signalMove( this, dx, dy );
//    }
    workplace->find_intersect_connection(this);


}

void Blok_area::InitPropertyView()
{
    //* Создаем модель из неизменяемых свойств*//
    QJsonTreeItem *root = InitPropertyModel();

    new QJsonTreeItem("Имя БУ", text(), root);
    new QJsonTreeItem("Идентификатор", QString::number(ID()) , root);

    addPropertyFromFile(":/tempalete/json/blok_area.json");

}

Blok_area* Blok_area::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    auto item = new Blok_area(GlobalContainerData_map, rail_obj["id"].toInt());
    //setBasePropertyFromJson(item, rail_obj);
    item->setPropertyFromJson(rail_obj);
    return item;
}

void Blok_area::setPropertyFromJson( QJsonObject rail_obj ){

    if (rail_obj.contains("EndPoint") && rail_obj["EndPoint"].isObject()){
       QJsonObject EndPointObj = rail_obj["EndPoint"].toObject();
       m_line.setP2( QPointF( EndPointObj["endPointX"].toDouble() ,
                                       EndPointObj["endPointY"].toDouble()) );
       auto rect = m_left_blokEdge->boundingRect();
       m_right_blokEdge->setPos(m_line.p2().x() - rect.width()/2 , m_line.p2().y());
       m_lineItem->setLine(m_line);
       m_text.setPos( m_line.center() );
       m_right_dot->setCenteredPosition( QPointF(m_line.p2().x()+9, m_line.p2().y() + rect.height()) );


    }

    Base_rail_object::setBasePropertyFromJson(this, rail_obj);
}

QJsonObject Blok_area::serialize()
{
    QJsonObject rail_obj = Base_rail_object::serializeBaseObj();

    QJsonObject endPointObj;
    endPointObj["endPointX"]        = m_line.p2().x();
    endPointObj["endPointY"]        = m_line.p2().y();
    rail_obj["EndPoint"] = endPointObj;

    return rail_obj;

}

