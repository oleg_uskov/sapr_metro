#include <QGraphicsScene>
#include <QDebug>
#include <QSvgRenderer>
#include "Rail_blok_edge.h"
#include <Connection.h>


Rail_blok_edge::Rail_blok_edge(int parentID ) :
    QGraphicsSvgItem(nullptr), //Base_rail_object( GlobalContainerData_map , ID , parent)
    m_parent_ID( parentID )
{
    this->setSharedRenderer(
                new QSvgRenderer(QLatin1String(":/tempalete/svg/edge_blok_area.svg")) );

    //setText(QString::number(m_cnt_item) + "|" + QString::number(m_cnt_item + 1) + "БУ");

//    auto rect = this->boundingRect();
//    m_text.setPos(QPointF(3,rect.height() - 82));
//    m_text.setParentItem(this);

//    Connection *Dot = new Connection(QPointF(rect.width()/2 - 2,
//                                             rect.height()), parentID, "EdgeBlokConn");
//    Dot->setParentItem(this);

    setFlags( ItemIsSelectable | ItemSendsGeometryChanges );
    setFlag( ItemIgnoresTransformations, true );

//    InitPropertyView();
//    Base_rail_object::ShowPropertyModel();
}


Rail_blok_edge::~Rail_blok_edge()
{

}

void Rail_blok_edge::mouseMoveEvent( QGraphicsSceneMouseEvent* event )
{
    if ( m_leftMouseButtonPressed )
    {
        auto dx = event->scenePos().x() - m_previousPosition.x();
        auto dy = event->scenePos().y() - m_previousPosition.y();
        dx = dx - ((int)dx % m_delta_moving);
        dy = dy - ((int)dy % m_delta_moving);
        moveBy( dx, dy );
        m_previousPosition += (QPointF(dx,dy));
        emit signalMove( this, dx, dy );
    }
    QGraphicsSvgItem::mouseMoveEvent( event );
}

void Rail_blok_edge::mousePressEvent( QGraphicsSceneMouseEvent* event )
{
    if ( event->button() & Qt::LeftButton )
    {
        m_leftMouseButtonPressed = true;
        m_previousPosition =  event->scenePos() ;
        emit clicked( this );
    }

    QGraphicsSvgItem::mousePressEvent( event );
}

void Rail_blok_edge::mouseReleaseEvent( QGraphicsSceneMouseEvent* event )
{
    if ( event->button() & Qt::LeftButton )
    {
        m_leftMouseButtonPressed = false;
    }

    QGraphicsSvgItem::mouseReleaseEvent( event );
}

void Rail_blok_edge::setMirror(bool XAxis, bool YAxis)
{
    this->resetTransform();
    //m_text.resetTransform();
    auto rect = this->boundingRect();
    this->setTransform(QTransform::fromTranslate( rect.width()/2, rect.height()/2) , true);
//    m_text.setTransform(QTransform::fromTranslate( m_text.boundingRect().width()/2,
//                                                   m_text.boundingRect().height()/2 ) , true );

    m_mirror_state[Qt::XAxis] = XAxis;
    m_mirror_state[Qt::YAxis] = YAxis;
    this->setTransform(QTransform::fromScale( XAxis ? -1: 1,
                                             YAxis ? -1: 1)  , true );
//    m_text.setTransform(QTransform::fromScale( XAxis ? -1: 1,
//                                             YAxis ? -1: 1)  , true);

    this->setTransform(QTransform::fromTranslate( -rect.width()/2, -rect.height()/2 )  , true );
//    m_text.setTransform(QTransform::fromTranslate( -m_text.boundingRect().width()/2,
//                                                   -m_text.boundingRect().height()/2 ) , true );

}


//const QString Rail_blok_edge::jsonConteinerName = "blokEdges";
//int Rail_blok_edge::m_cnt_item = 0;



//void Rail_blok_edge::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
//{

//    //qDebug() << "Date:"  << elementId();
//    ContextMenu.exec(event->screenPos());
//}

//void Rail_blok_edge::InitPropertyView()
//{
//    //* Создаем модель из неизменяемых свойств*//
//    QJsonTreeItem *root = InitPropertyModel();

//    new QJsonTreeItem("Имя границы БУ", text(), root);
//    new QJsonTreeItem("Идентификатор", QString::number(ID()) , root);
//    ////////////////////////////////////////////////////////////////

//    //* Создаем модель из изменяемых свойств*//
//    QString saveData;
//    QFile openFile( QLatin1String(":/tempalete/json/edge_blok.json"));

//    if(!openFile.open(QIODevice::ReadOnly)) {
//        qDebug()<<"file not opened \n";
//    }
//    else
//    {
//        //qDebug()<<"file opened \n";
//        saveData = openFile.readAll();
//    }

//    openFile.close();

//    auto Jdoc = QJsonDocument::fromJson(saveData.toUtf8());
//    QJsonObject JObject = Jdoc.object();
//    QJsonObject obj = JObject["Properties"].toObject();
//    m_PropertyModel->addJson(QJsonDocument(obj));
//}

//Rail_blok_edge* Rail_blok_edge::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
//    auto item = new Rail_blok_edge(GlobalContainerData_map, rail_obj["id"].toInt());
//    setBasePropertyFromJson(item, rail_obj);
//    return item;
//}




