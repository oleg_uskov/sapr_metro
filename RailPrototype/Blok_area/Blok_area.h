#ifndef Blok_area_H
#define Blok_area_H


#include "Base_rail_object.h"
class Rail_blok_edge;

class Blok_area : public Base_rail_object , public Registrate_base_rail_object<Blok_area>
{
    Q_OBJECT

public:
    //using Base_rail_object::Base_rail_object;
    Blok_area();
    explicit Blok_area(const QMap<QString, QObject *> & GlobalContainerData_map, int ID = 0, QObject* parent = 0 );
    ~Blok_area();
    Blok_area(const Blok_area &) = default;

//    enum { Type = Main::Blok_areaType };
//    int type() const override   { return Type; }

    static const QString jsonConteinerName;
    QString JsonConteinerName() override {return jsonConteinerName ;}
    Blok_area* createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj) override;
    void setPropertyFromJson( QJsonObject rail_obj ) override;
    QJsonObject serialize() override;

signals:



protected:
    QRectF boundingRect(void) const override;
    QPainterPath shape(void) const override;
    //void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    void InitPropertyView(void);


private slots:
    void blokEdgeMoveSlot( QGraphicsSvgItem* signalOwner, qreal dx, qreal dy );

protected:
    //void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override;



private:
    static int m_cnt_item;

    Rail_blok_edge *             m_left_blokEdge  ;
    Rail_blok_edge *             m_right_blokEdge ;
    Connection *                 m_left_dot;
    Connection *                 m_right_dot;
    QLineF                       m_line ;
    QGraphicsLineItem *          m_lineItem ;
};

Q_DECLARE_METATYPE(Blok_area)

#endif // Blok_area_H
