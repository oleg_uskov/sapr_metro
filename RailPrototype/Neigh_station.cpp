#include <QGraphicsScene>
#include <QDebug>
#include "Neigh_station.h"
#include "Connection.h"

Neigh_station::Neigh_station()
{
    if (!m_QStandardItem){
        m_QStandardItem = new QStandardItem(QIcon(":/tempalete/svg/neigh_station.svg"),  "Соседняя станция");
        m_QStandardItem->setData(  metaObject()->className()  );
    }
};

Neigh_station::Neigh_station(const QMap<QString, QObject *> & GlobalContainerData_map, int ID,  QObject *parent) :
    Base_rail_object( GlobalContainerData_map , ID , parent)
{
    m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/neigh_station.svg"));
    this->setSharedRenderer(m_renderer);
    //setAcceptDrops(true);
    m_cnt_item++;
    auto rect = this->boundingRect();
    setText("соседняя станция");
    m_text.setPos(QPointF(10, rect.height()/2 - 12));
    m_text.setParentItem(this);


    InitPropertyView();
    Base_rail_object::ShowPropertyModel();

}


Neigh_station::~Neigh_station()
{

}

const QString Neigh_station::jsonConteinerName = "Neigh_stations";
int Neigh_station::m_cnt_item = 0;


void Neigh_station::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{

    //qDebug() << "Date:"  << elementId();
    //ContextMenu.exec(event->screenPos());
}


void Neigh_station::InitPropertyView()
{
    //* Создаем модель из неизменяемых свойств*//
    QJsonTreeItem *root = InitPropertyModel();

    new QJsonTreeItem("Соседняя станция", text(), root);
    new QJsonTreeItem("Идентификатор", QString::number(ID()) , root);

    addPropertyFromFile(":/tempalete/json/neigh_station.json");

}

Neigh_station* Neigh_station::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    auto item = new Neigh_station(GlobalContainerData_map, rail_obj["id"].toInt());
    setBasePropertyFromJson(item, rail_obj);
    return item;
}

