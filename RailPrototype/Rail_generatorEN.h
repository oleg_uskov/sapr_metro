#ifndef RAIL_GENERATOREN_H
#define RAIL_GENERATOREN_H

#include <QObject>
#include <QWidget>
#include <QComboBox>
#include <QGraphicsProxyWidget>
#include "Base_rail_object.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Code_SynchoGroup; }
QT_END_NAMESPACE


class Rail_generatorEN : public Base_rail_object
{
    Q_OBJECT
public:
    Rail_generatorEN() {};
    explicit Rail_generatorEN(const QMap<QString, QObject *> & GlobalContainerData_map, int ID = 0, QObject* parent = 0 );
    ~Rail_generatorEN();
    Rail_generatorEN(const Rail_generatorEN &) = default;

    enum { Type = Main::Rail_generatorENType };
    int type() const override   { return Type; }

    static const QString jsonConteinerName;
    QString JsonConteinerName() override {return jsonConteinerName ;}
    Rail_generatorEN* createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj) override;
    //void setPropertyFromJson( QJsonObject rail_obj );

signals:


protected:


public slots:

private slots:

private:
    Ui::Code_SynchoGroup * generatorEN_ui;

    static int m_cnt_item;

};

Q_DECLARE_METATYPE(Rail_generatorEN)

#endif // RAIL_GENERATOREN_H
