
#include "Rail_bell.h"


Rail_bell::Rail_bell()
{
    if (!m_QStandardItem){
        m_QStandardItem = new QStandardItem(QIcon(":/tempalete/svg/bell.svg"),  "Звонок");
        m_QStandardItem->setData(  metaObject()->className()  );
    }
};

Rail_bell::Rail_bell(const QMap<QString, QObject *> & GlobalContainerData_map, int ID,  QObject *parent) :
    Base_rail_object( GlobalContainerData_map , ID , parent)
{
    m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/bell.svg"));
    this->setSharedRenderer(m_renderer);
    //setAcceptDrops(true);
    m_cnt_item++;

    setText("Зв." + QString::number(m_cnt_item));
    m_text.setPos(QPointF(0,0));
    m_text.setParentItem(this);

    InitPropertyView();
    Base_rail_object::ShowPropertyModel();
}

Rail_bell::~Rail_bell()
{

}

const QString Rail_bell::jsonConteinerName = "bells";
int Rail_bell::m_cnt_item = 0;

void Rail_bell::InitPropertyView()
{
    //* Создаем модель из неизменяемых свойств*//
    QJsonTreeItem *root = InitPropertyModel();

    new QJsonTreeItem("Имя звонка", text(), root);
    new QJsonTreeItem("Идентификатор", QString::number(ID()) , root);
    //*********************************************************//

    //* Создаем модель из изменяемых свойств*//
    //addPropertyFromFile("../SAPR_Metro/tempalete/json/bell.json");
    addPropertyFromFile(":/tempalete/json/bell.json");
    //*********************************************************//

}

Rail_bell* Rail_bell::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    auto item = new Rail_bell(GlobalContainerData_map, rail_obj["id"].toInt());
    setBasePropertyFromJson(item, rail_obj);
    return item;
}

//void Rail_bell::setPropertyFromJson( QJsonObject rail_obj ){
//    Base_rail_object::setBasePropertyFromJson(this, rail_obj);
//}

