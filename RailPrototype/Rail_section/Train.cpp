#include <QCheckBox>

#include "Train.h"
#include "Train_forward.h"
#include "workplace.h"
#include "Rail_section.h"


Train::Train(const QMap<QString, QObject *> & GlobalContainerData_map, int parentID, TrainStyle style, QGraphicsItem * parent) :
    QGraphicsSvgItem(parent),
    m_GlobalContainerData_map(GlobalContainerData_map),
    m_leftMouseButtonPressed(false)
{
    setFlags( ItemIsSelectable | ItemSendsGeometryChanges | ItemSendsScenePositionChanges);
    auto trainRender = new QSvgRenderer(QLatin1String(":/tempalete/svg/train.svg"));
    //auto train = new QGraphicsSvgItem();
    this->setSharedRenderer(trainRender);
    auto rect = this->boundingRect();
    this->setPos(QPointF(rect.width()/2 +10, rect.height()/2 + 20));
    m_parent_ID = parentID;
    //setAcceptHoverEvents(true);

}

void Train::mouseDoubleClickEvent( QGraphicsSceneMouseEvent* event )
{
    if ( event->button() & Qt::LeftButton ) {
        auto train_forward = createTrainForward(event->scenePos());
        WorkPlace * workPlace = static_cast<WorkPlace *>(m_GlobalContainerData_map["workplaceScene"]);
        workPlace->add_new_item_to_scene(train_forward);
    }

    QGraphicsSvgItem::mouseDoubleClickEvent( event );
}

void Train::mousePressEvent( QGraphicsSceneMouseEvent* event )
{
    if ( event->button() & Qt::LeftButton ){
        m_leftMouseButtonPressed = true;
        setToolTip(Info());
    }

    QGraphicsSvgItem::mousePressEvent( event );
}


void Train::mouseMoveEvent( QGraphicsSceneMouseEvent* event )
{
    QGraphicsSvgItem::mouseMoveEvent( event );

    if ( m_leftMouseButtonPressed )
    {
        m_leftMouseButtonPressed = false;
    }

}

void Train::mouseReleaseEvent( QGraphicsSceneMouseEvent* event )
{
    if ( event->button() & Qt::LeftButton )
    {
        m_leftMouseButtonPressed = false;
    }
    QGraphicsSvgItem::mouseReleaseEvent( event );
}

void Train::hoverEnterEvent( QGraphicsSceneHoverEvent* event ) {
    QGraphicsSvgItem::hoverEnterEvent( event );
}

QVariant Train::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if (change == ItemSelectedHasChanged && scene()) {
        if (!value.toBool()){ //снято выделение
            //this->setVisible(false);
        }
    } else if (change == ItemVisibleChange && scene()) {
        emit TrainChangedVisibleSignal(value.toBool());
    }

    return QGraphicsSvgItem::itemChange(change, value);
}

void Train::deletedTrain_forwardSlot(QGraphicsItem* item){
    if (Train_forward * train = dynamic_cast<Train_forward *>( item ))
        connectedTrains_forward.removeOne(train);
}

QString & Train::Info(){
        info =  "Speed numbers = "  + QString::number( connectedTrains_forward.count() ) ;
        return info;
}

void Train::childChangedSelectionSlot(Train_forward * signalOwner){
    if (this->isSelected() or parentItem()->isSelected() ) {
        return;
    }
    bool oneOfChildSelected = false;
    for (auto & train_forward : connectedTrains_forward){
        oneOfChildSelected = oneOfChildSelected | train_forward->isSelected();
    }
    this->setVisible(oneOfChildSelected);
}


Train_forward * Train::createTrainForward(QPointF pos) {
    WorkPlace * workPlace = static_cast<WorkPlace *>(m_GlobalContainerData_map["workplaceScene"]);
    auto train_forward = new Train_forward(m_GlobalContainerData_map, workPlace->getUniqueID(), m_parent_ID, m_lastSpeedValue);

    connectedTrains_forward.append(train_forward);
    this->m_lastSpeedValue =  m_lastSpeedValue == 40 ? m_lastSpeedValue + 20 : m_lastSpeedValue + 10;
    auto par = this->parentItem();
    if (auto base =  dynamic_cast<Base_rail_object *>(par) ) {
        connect( this , &Train::TrainChangedVisibleSignal, train_forward, &Train_forward::ChangedVisibleSlot );
        connect( train_forward, &Train_forward::changedSelectionSignal, this , &Train::childChangedSelectionSlot  );
        auto cb = static_cast<QCheckBox *>(m_GlobalContainerData_map["show_speed_chbox"]);
        if (cb){
            connect(cb, &QCheckBox::stateChanged, train_forward, &Train_forward::ChangedVisibleSlot) ;
        }
        train_forward->setTransFromParent(base->transform(), base->textTransform());
        train_forward->setRotation( base->rotation() );
        train_forward->setPos(pos);
    }
    return train_forward;
}

Train_forward *  Train::createTrainForward( QJsonObject rail_obj ) {

    auto train_forward = Train_forward::createFromJson_static(m_GlobalContainerData_map, rail_obj);
    connectedTrains_forward.append(train_forward);
    connect( this , &Train::TrainChangedVisibleSignal, train_forward, &Train_forward::ChangedVisibleSlot );
    connect( train_forward, &Train_forward::changedSelectionSignal, this , &Train::childChangedSelectionSlot  );
    auto cb = static_cast<QCheckBox *>(m_GlobalContainerData_map["show_speed_chbox"]);
    if (cb){
        connect(cb, &QCheckBox::stateChanged, train_forward, &Train_forward::ChangedVisibleSlot) ;
    }

    auto par = this->parentItem();
    if (auto base =  dynamic_cast<Rail_section *>(par) ) {
        train_forward->setTransFromParent(base->transform(), base->textTransform());
        train_forward->setRotation( base->rotation() );
        train_forward->setParent_ID(base->ID());
    }
    return train_forward;
}

