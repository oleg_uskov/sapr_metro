#ifndef TRAIN_H
#define TRAIN_H

#include <QObject>
#include <QString>
#include <QGraphicsSvgItem>
#include <QSvgRenderer>
#include <QGraphicsSceneDragDropEvent>
#include <QMimeData>
#include "Train_forward.h"
//#include "main.h"

class Train : public QGraphicsSvgItem
{
    Q_OBJECT
    //Q_PROPERTY( QList <Train *> Neighbour_Trains READ Neighbour_Trains)

public:

    enum TrainStyle
    {
        Cross,
        Triangle,
    };

public:
    using QGraphicsSvgItem::QGraphicsSvgItem;
    explicit Train(const QMap<QString, QObject *> & GlobalContainerData_map, int parentID, TrainStyle style = Cross, QGraphicsItem *parent = nullptr);
    enum { Type = Main::TrainType };
    int type() const override   { return Type; }

    QSvgRenderer *m_renderer;

//    QList <Train *> m_Neighbour_Trains; // будет такой класс Train, с который есть связь
//    QList <Train *> Neighbour_Trains() const{ return m_Neighbour_Trains;};
//    QString m_nameConn;

    int parentID() const {return m_parent_ID;}

    //TrainStyle Style() const{return m_CurrentTrainStyle;}
    bool isLeftMouseButtonPressed() const{return m_leftMouseButtonPressed;}
    int lastSpeedValue() const{return m_lastSpeedValue;}
    Train_forward * createTrainForward(QPointF pos);
    Train_forward * createTrainForward(QJsonObject rail_obj ) ;

    QList<Train_forward *> connectedTrains_forward;

signals:
    void TrainChangedVisibleSignal(bool visible_state);

protected:
    void mouseDoubleClickEvent( QGraphicsSceneMouseEvent* event ) override;
    void mouseMoveEvent( QGraphicsSceneMouseEvent* event ) override;
    void mousePressEvent( QGraphicsSceneMouseEvent* event ) override;
    void mouseReleaseEvent( QGraphicsSceneMouseEvent* event ) override;
    void hoverEnterEvent( QGraphicsSceneHoverEvent* event ) override;
    QVariant itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value) override;

    QString & Info(void);

    //bool dragOver = false;

public slots:
    void deletedTrain_forwardSlot(QGraphicsItem* item);
    void childChangedSelectionSlot(Train_forward * signalOwner);

private slots:
//    void slotMove( QGraphicsItem* signalOwner, qreal dx, qreal dy );

private:

    int m_parent_ID;
    int m_lastSpeedValue = 40;
    QPointF m_previousPosition;
    bool m_leftMouseButtonPressed;
    TrainStyle m_CurrentTrainStyle = Cross;
    QString info;
    const QMap<QString, QObject *>    m_GlobalContainerData_map;

};

#endif // TRAIN_H
