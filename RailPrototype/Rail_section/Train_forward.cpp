#include <QDebug>
#include <QComboBox>
#include <QCompleter>
#include <QSpinBox>
#include <QGraphicsProxyWidget>
#include <QTimer>

#include "Train_forward.h"

Train_forward::Train_forward(const QMap<QString, QObject *> & GlobalContainerData_map, int ID, int parentID, int speedValue,  QObject *parent) :
    Base_rail_object( GlobalContainerData_map , ID , parent),
    m_parent_ID(parentID)
{
    m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/train_forward.svg"));
    this->setSharedRenderer(m_renderer);
    Connection *Dot = new Connection(QPointF(0,-50), ID, "Train_forwardConn");
    Dot->setFlagInheritedStyle(false);
    Dot->setStyle(Connection::Simple);
    Dot->setParentItem(this);


    InitSpeedBox(speedValue);
    InitPropertyView();
    Base_rail_object::ShowPropertyModel();
}

Train_forward::~Train_forward()
{
    delete m_renderer;
    delete m_SpeedBoxItem;
    delete m_PropertyModel;
}

//const QString Train_forward::jsonConteinerName = "switches";
void Train_forward::InitSpeedBox(int speedValue)
{
    auto spinBox = new QSpinBox;
    spinBox->setRange(40,80);
    spinBox->setSingleStep(10);
    spinBox->setValue(speedValue);
    spinBox->setFixedSize(38,20);
    m_SpeedBoxItem = new QGraphicsProxyWidget(this); // the proxy's parent is pMyItem
    m_SpeedBoxItem->setWidget(spinBox); // adding the QWidget based object to the proxy
    //m_SpeedBoxItem->setVisible(false);
    //m_SpeedBoxItem->setFlag( ItemIgnoresTransformations, true );
    auto rect = this->boundingRect();
    this->setTransformOriginPoint(rect.width()/2,  rect.height()/2);

}

int Train_forward::speed()
{
    auto spinBox = static_cast<QSpinBox *>(m_SpeedBoxItem->widget() );
    return spinBox->value();
}

void Train_forward::setSpeed(int speedValue)
{
    auto spinBox = static_cast<QSpinBox *>(m_SpeedBoxItem->widget() );
    spinBox->setValue(speedValue);
}

void Train_forward::InitPropertyView()
{
    //* Создаем модель из неизменяемых свойств*//
    QJsonTreeItem *root = InitPropertyModel();

    new QJsonTreeItem("Скорость", text(), root);
    new QJsonTreeItem("Имя секции", text(), root);
    new QJsonTreeItem("ID", QString::number(ID()), root);
    new QJsonTreeItem("ParentID", QString::number(m_parent_ID), root);
}

QVariant Train_forward::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if(m_SpeedBoxItem) {
        if (change == ItemSelectedHasChanged && scene()) {
            if (!value.toBool()){ //снято выделение
                QTimer::singleShot(500, this , &Train_forward::unselected_delayed)     ;
                //qDebug() << speed();
            }
        }


    }

    return QGraphicsSvgItem::itemChange(change, value);
}


void Train_forward::setTransFromParent( const QTransform & parentTrans, const QTransform & textTrans )
{
    this->resetTransform();
    m_SpeedBoxItem->resetTransform();

    this->setTransform( parentTrans);
    m_SpeedBoxItem->setTransform( textTrans);

}


Train_forward* Train_forward::createFromJson_static(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    auto item = new Train_forward(GlobalContainerData_map, rail_obj["id"].toInt(), 0);
    item->setPropertyFromJson(rail_obj);
    return item;
}

Train_forward* Train_forward::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    return Train_forward::createFromJson_static( GlobalContainerData_map, rail_obj);
}

void Train_forward::setPropertyFromJson( QJsonObject rail_obj ){

    //auto train_forward = new Train_forward(m_GlobalContainerData_map, trainJson["id"].toInt(), this->ID());
    setX(rail_obj["posX"].toDouble());
    setY(rail_obj["posY"].toDouble());
    setSpeed(rail_obj["speed"].toInt());
}

QJsonObject Train_forward::serialize()
{
    QJsonObject train_forwardJson;
    train_forwardJson["id"]        = ID();
    train_forwardJson["posX"]      = x();
    train_forwardJson["posY"]      = y();
    train_forwardJson["speed"]     = speed();

    return train_forwardJson;

}


