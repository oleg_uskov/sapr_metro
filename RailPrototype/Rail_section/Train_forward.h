#ifndef Train_forward_H
#define Train_forward_H

#include "Base_rail_object.h"
#include <QTreeWidget>


class Train_forward : public Base_rail_object
{
    Q_OBJECT
    Q_PROPERTY( int speed READ speed )


public:
    //using Base_rail_object::Base_rail_object;
    Train_forward() {};
    explicit Train_forward(const QMap<QString, QObject *> & GlobalContainerData_map, int ID, int parentID = 0, int speedValue = 40, QObject* parent = 0 );
    ~Train_forward();
    Train_forward(const Train_forward &) = default;

    enum { Type = Main::Train_forwardType };
    int type() const override   { return Type; }

    //static const QString jsonConteinerName;
    static Train_forward* createFromJson_static(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj);
    Train_forward* createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj) override;
    void setPropertyFromJson( QJsonObject rail_obj ) override;
    QJsonObject serialize() override;

    int speed(void);
    void setSpeed(int speedValue);
    int parent_ID(void) const {return m_parent_ID;};
    void setParent_ID(int id) {m_parent_ID = id;}
    //bool willBeSelected = false;
    void setTransFromParent( const QTransform & parentTrans, const QTransform & textTrans) ;

signals:
    void changedSelectionSignal(Train_forward * signalOwner);

protected:
    QVariant itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value) override;


protected:
    void InitSpeedBox(int speedValue);
    void InitPropertyView(void);
    void unselected_delayed() { emit changedSelectionSignal(this); }

public slots:

private slots:

private:

    static int m_cnt_item;
    //QGraphicsItemGroup * m_sectGroup;
    QGraphicsProxyWidget * m_SpeedBoxItem;
    int m_parent_ID;
};

Q_DECLARE_METATYPE(Train_forward)

#endif // Train_forward_H
