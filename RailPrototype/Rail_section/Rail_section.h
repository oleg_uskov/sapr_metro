#ifndef SECTION_H
#define SECTION_H

#include "Base_rail_object.h"
#include "Train.h"
#include <QTreeWidget>

class Rail_section : public Base_rail_object , public Registrate_base_rail_object<Rail_section>
{
    Q_OBJECT


public:
    //using Base_rail_object::Base_rail_object;
    Rail_section();
    explicit Rail_section(const QMap<QString, QObject *> & GlobalContainerData_map, int ID = 0, QObject* parent = 0 );
    ~Rail_section();
    Rail_section(const Rail_section &) = default;

    static uint lastSectionName;
    static int deltaSectionName;

    enum { Type = Main::Rail_sectionType };
    int type() const override { return Type; }

    static const QString jsonConteinerName;
    QString JsonConteinerName() override {return jsonConteinerName ;}
    Rail_section* createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj) override;
    void setPropertyFromJson( QJsonObject rail_obj ) override;
    QJsonObject serialize() override;

    QGraphicsItemGroup * sectGroup() const { return m_sectGroup;  }
    Train * train() const { return m_train;  }

    Rail_section* clone(int ID);

signals:

protected:
    void mousePressEvent( QGraphicsSceneMouseEvent* event ) override;
    QVariant itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value) override;

protected:
    //void ShowPropertyModel(void);
    //void InitPropertyModel(void);
    void InitPropertyView(void);
    //void InitSpeedBox(void);
    QString createNewName() const;

public slots:
    //void sceneActionChangedSlot(int action);
    void deletedItemSlot(QGraphicsItem* item) override;

private slots:
    void textChangedStot();

private:

    static uint m_cnt_item;
    QGraphicsItemGroup * m_sectGroup;
    Train * m_train;
    Connection * m_leftConnection;
    Connection * m_rightConnection;

};

Q_DECLARE_METATYPE(Rail_section)

#endif // SECTION_H
