//#include <QGraphicsSceneMouseEvent>
//#include <QPainterPath>
//#include <QGraphicsScene>
//#include <QGraphicsPathItem>
//#include <QGraphicsSvgItem>
#include <QDebug>
//#include <QVariant>
#include <QComboBox>
#include <QCompleter>
#include <QListWidget>
#include <QTableWidget>
#include <QGroupBox>
#include <QGraphicsProxyWidget>
#include <QRadioButton>
#include <QVBoxLayout>

#include "Rail_section.h"
#include "workplace.h"


Rail_section::Rail_section()
{
    if (!m_QStandardItem){
        m_QStandardItem = new QStandardItem(QIcon(":/tempalete/svg/section.svg"),  "Секция");
        m_QStandardItem->setData(  metaObject()->className()  );
    }
};

Rail_section::Rail_section(const QMap<QString, QObject *> & GlobalContainerData_map, int ID,  QObject *parent) :
    Base_rail_object( GlobalContainerData_map , ID , parent)
{
//    Q_INVOKABLE

    m_renderer = new QSvgRenderer(QLatin1String(":/tempalete/svg/section.svg"));
    this->setSharedRenderer(m_renderer);
    m_cnt_item += 2;
    setText(  createNewName()  );
    m_text.setPos(QPointF(40,2));
    m_text.setParentItem(this);
    auto rect = this->boundingRect();
    m_leftConnection = new Connection(QPointF(-1,rect.height()/2 + 0.4), ID, "left");
    m_leftConnection->setParentItem(this);
    connect( m_leftConnection , &Connection::ConnectionClicked, this, &Base_rail_object::clickChildConnectionSlot);
    m_rightConnection = new Connection(QPointF(rect.width() + 2 , rect.height()/2 + 0.4), ID, "right");
    m_rightConnection->setParentItem(this);
    connect( m_rightConnection , &Connection::ConnectionClicked, this, &Base_rail_object::clickChildConnectionSlot);
    //InitSpeedBox();
    m_train = new Train(m_GlobalContainerData_map, ID);
    m_train->setParentItem(this);
    m_train->setVisible(false);

    connect(this, &Rail_section::textChanged , this , &Rail_section::textChangedStot);

    InitPropertyView();
    //setFlags( ItemIsSelectable | ItemSendsScenePositionChanges | ItemSendsGeometryChanges );
    Base_rail_object::ShowPropertyModel();
}

Rail_section::~Rail_section()
{
    delete m_renderer;
}


const QString Rail_section::jsonConteinerName = "sections";
uint Rail_section::m_cnt_item = 100;
uint Rail_section::lastSectionName = Rail_section::m_cnt_item;
int Rail_section::deltaSectionName = 2;

QString Rail_section::createNewName() const
{
    Rail_section::lastSectionName += Rail_section::deltaSectionName;
    return QString::number(   Rail_section::lastSectionName  );
}


void Rail_section::mousePressEvent( QGraphicsSceneMouseEvent* event )
{
    m_train->setVisible(true);
    Base_rail_object::mousePressEvent( event );
}

//void Rail_section::sceneActionChangedSlot(const int action)
//{
//    if (action != WorkPlace::Train_forwardType) //
//        m_train->setVisible(false);
//}

void Rail_section::deletedItemSlot(QGraphicsItem* item)
{
    this->train()->deletedTrain_forwardSlot(item);
}

QVariant Rail_section::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if (change == ItemSelectedHasChanged && scene()) {
        if (!value.toBool()){ //снято выделение
            if (!m_train->isLeftMouseButtonPressed())
                m_train->setVisible(false);
        }
    }
    return Base_rail_object::itemChange(change, value);
}

void Rail_section::InitPropertyView()
{
    //* Создаем модель из неизменяемых свойств*//
    QJsonTreeItem *root = InitPropertyModel();

    new QJsonTreeItem("Имя секции", text(), root); // всегда должен быть первым с списке
    //connect(m_PropertyModel, &QJsonModel::dataChanged , this , &Rail_section::dataChangedStot);

    new QJsonTreeItem("Идентификатор", QString::number(ID()) , root);
    //new QJsonTreeItem("Кол-во скоростей", QString::number(m_train->connectedTrains_forward.count()) , root);
    ////////////////////////////////////////////////////////////////

    //* Создаем модель из изменяемых свойств*//
    addPropertyFromFile(":/tempalete/json/section.json");
}

Rail_section* Rail_section::clone(int ID)
{
    auto item = new Rail_section(m_GlobalContainerData_map, ID );

    return item;
    //Base_rail_object::clone(ID);
}

Rail_section* Rail_section::createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj){
    auto item = new Rail_section(GlobalContainerData_map, rail_obj["id"].toInt());
    //setBasePropertyFromJson(item, rail_obj);
    item->setPropertyFromJson(rail_obj);
    return item;
}

void Rail_section::setPropertyFromJson( QJsonObject rail_obj ){
    Base_rail_object::setBasePropertyFromJson(this, rail_obj);

    if (rail_obj.contains("Speeds") && rail_obj["Speeds"].isArray()){
        auto trainsForward = rail_obj["Speeds"].toArray();
        for(auto  trainVar : trainsForward){
            QJsonObject trainJson = trainVar.toObject();
            auto train_forward =  m_train->createTrainForward(trainJson);
            m_train->setVisible(true);
            train_forward->setVisible(true);
            WorkPlace * workPlace = static_cast<WorkPlace *>(m_GlobalContainerData_map["workplaceScene"]);
            workPlace->add_new_item_to_scene(train_forward);
        }
    }
}

QJsonObject Rail_section::serialize()
{
    QJsonObject rail_obj = serializeBaseObj();
    // добавим часть данных
    QJsonArray trainsArray;
    for (auto train_forward: m_train->connectedTrains_forward) {
//        QJsonObject train_forwardJson;
//        train_forwardJson["id"]        = train_forward->ID();
//        train_forwardJson["posX"]      = train_forward->x();
//        train_forwardJson["posY"]      = train_forward->y();
//        train_forwardJson["speed"]     = train_forward->speed();
        trainsArray.append(train_forward->serialize());
    }

    if (!trainsArray.empty())
        rail_obj["Speeds"] = trainsArray;

    return rail_obj;
}

void Rail_section::textChangedStot()
{
    bool ok;
    text().toUInt(&ok);
    if (ok){
        if (text().toUInt() <= 2 ){
            Rail_section::deltaSectionName = 2;
        }
        else if (text().toUInt() > Rail_section::lastSectionName){
            Rail_section::deltaSectionName = 2;
        }
        else if (text().toUInt() < Rail_section::lastSectionName) {
            Rail_section::deltaSectionName = -2;
        }
        Rail_section::lastSectionName = text().toUInt();
    }
    if (Rail_section::lastSectionName <= 2){
        Rail_section::deltaSectionName = 2;
    }
}

