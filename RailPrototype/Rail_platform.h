#ifndef Rail_platform_H
#define Rail_platform_H


#include "Base_rail_object.h"

class Rail_platform : public Base_rail_object, public Registrate_base_rail_object<Rail_platform>
{
    Q_OBJECT


public:
    using Base_rail_object::Base_rail_object;
    Rail_platform() ;
    explicit Rail_platform(const QMap<QString, QObject *> & GlobalContainerData_map, int ID = 0, QObject* parent = 0 );
    ~Rail_platform();
    Rail_platform(const Rail_platform &) = default;

    enum { Type = Main::Rail_platformType };
    int type() const override   { return Type; }

    static const QString jsonConteinerName;
    QString JsonConteinerName() override {return jsonConteinerName ;}
    Rail_platform* createFromJson(const QMap<QString, QObject *> & GlobalContainerData_map, QJsonObject rail_obj) override;
    //void setPropertyFromJson( QJsonObject rail_obj );

signals:



protected:

    void InitPropertyView(void);


private slots:
    void addProtect_area(void);

protected:
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override;


private:


    static int m_cnt_item;

signals:

};

Q_DECLARE_METATYPE(Rail_platform)

#endif // Rail_platform_H
