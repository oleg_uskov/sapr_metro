﻿//using namespace std;

#include <QApplication>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsSceneDragDropEvent>
#include <QKeyEvent>
#include <QDebug>
#include <QPainter>
#include <QPen>
#include <QClipboard>
#include <QGraphicsRotation>
#include <QGraphicsItem>
#include <QGraphicsView>
#include <QMetaType>
#include <QStringListModel>

#include "Base_rail_object.h"
//#include "Registrate_base_rail_object.h"
#include "Rail_switch.h"
#include "Rail_section.h"
#include "Rail_signal.h"
//#include "Rail_blok_edge.h"
#include "Rail_prot_area.h"
#include "Rail_generatorEN.h"
#include "Train_forward.h"
#include "Train.h"
#include "Rail_station_edge.h"
#include "Connection.h"
#include "Connection_Line.h"
#include "Connection.h"
#include "Connection_Line.h"
#include "Neigh_station.h"
#include "Station.h"
#include "Rail_platform.h"
#include "Rail_generatorTRC.h"
#include "Blok_area.h"
#include "Rail_lamp.h"

#include <QGraphicsRectItem>
#include "mainwindow.h"
#include "workplace.h"



#define  m_GlobalIF m_GlobalContainerData_map;


WorkPlace::WorkPlace(const QMap<QString, QObject *> & GlobalContainerData_map, QObject* parent ) :
    QGraphicsScene( parent ),
    m_GlobalContainerData_map(GlobalContainerData_map),
    m_MaxID(1),
    currentItem( nullptr ),
    m_currentAction( Main::DefaultAction ),
    //m_JsonSaver(nullptr),
    m_previousAction( Main::DefaultAction ),
    m_previousPosition( 0 , 100 ),
    m_leftMouseButtonPressed( false  )
{
//    qRegisterMetaType<Rail_section>("Rail_section");
//    qRegisterMetaType<Rail_switch>("Rail_switch");
//    qRegisterMetaType<Rail_signal>("Rail_signal");
    qRegisterMetaType<Rail_prot_area>("Rail_prot_area");
    qRegisterMetaType<Rail_generatorEN>("Rail_generatorEN");
    qRegisterMetaType<Train_forward>("Train_forward");
    qRegisterMetaType<Rail_station_edge>("Rail_station_edge");
    qRegisterMetaType<Connection_Line>("Connection_Line");
    qRegisterMetaType<Neigh_station>("Neigh_station");
    qRegisterMetaType<Station>("Station");
    qRegisterMetaType<Rail_platform>("Rail_platform");
    qRegisterMetaType<Rail_generatorTRC>("Rail_generatorTRC");
//    qRegisterMetaType<Blok_area>("Blok_area");
    initTemplate();

    /*создаем структуру контекстного меню */
    auto platform_Action = m_ContextMenu.addAction("Добавить платформу");
    connect(platform_Action, &QAction::triggered, [ = ]() {  this->add_new_item_to_scene( new Rail_platform( m_GlobalContainerData_map , getUniqueID() )); } );
    auto NeighStation_Action = m_ContextMenu.addAction("Добавить соседнюю станцию");
    connect(NeighStation_Action, &QAction::triggered, [ = ]() {  this->add_new_item_to_scene( new Neigh_station( m_GlobalContainerData_map, getUniqueID()  )); } );
    auto Station_Action = m_ContextMenu.addAction("Добавить имя станции");
    connect(Station_Action, &QAction::triggered, [ = ]() {  this->add_new_item_to_scene( new Station( m_GlobalContainerData_map , getUniqueID()  )); } );

//    auto polygon = new QPolygonF();
//    *polygon << QPointF(10.4, 20.5) << QPointF(20.2, 30.2) << QPointF(100, 50) << QPointF(0, 5) ;
//    auto pol = new QGraphicsPolygonItem (*polygon);
//    this->addItem(pol);
//    pol->setFlags( QGraphicsItem::ItemIsSelectable );
    auto rect = QRectF (0, 0, 5000,  1000);
    this->addRect(  rect  );


}

WorkPlace::~WorkPlace()
{
    delete currentItem;
}

Actions WorkPlace::currentAction() const
{
    return m_currentAction;
}

void WorkPlace::setCurrentAction( Actions action)
{
    m_previousAction = m_currentAction;
    m_currentAction = action;
    emit currentActionChanged( m_currentAction );
}

void WorkPlace::setCurrentItem(Base_rail_object * item )
{
    //setCurrentAction( DefaultAction );
    currentItem = item;
}

QColor WorkPlace::color_1() const
{
    return m_colorW;
}

QPointF WorkPlace::previousPosition() const
{
    return m_previousPosition;
}

void WorkPlace::setPreviousPosition( const QPointF previousPosition )
{
    if ( m_previousPosition == previousPosition )
    {
        return;
    }

    m_previousPosition = previousPosition;
    emit previousPositionChanged();
}

/*метод для рисования секции и формирование области выделения объектов*/
void WorkPlace::mousePressEvent( QGraphicsSceneMouseEvent* event )
{
    if ( event->button() & Qt::LeftButton )
    {
        m_leftMouseButtonPressed = true;
        setPreviousPosition( event->scenePos() );
        //deselectItems();

        if ( this->items(event->scenePos()).count() == 0 ) // клик по пустому месту ( QApplication::keyboardModifiers() & Qt::ShiftModifier )
        {
            setCurrentAction( SelectionAction );
        }
    }

    switch ( m_currentAction )
    {
        case SelectionAction:
            {
                    if ( m_leftMouseButtonPressed && !( event->button() & Qt::RightButton )
                     && !( event->button() & Qt::MiddleButton ) )
                    {
                        deselectItems();
                        currentItem = this->addRect(QRectF(m_previousPosition,m_previousPosition),
                                                    QPen(Qt::DashLine));
                    }
                    break;
            }
        case itemEditorAction:
            {
                auto point = currentItem->mapFromScene( event->scenePos() );
                //qDebug() << point;
                if ( !currentItem->contains(point) ){
                    setCurrentAction( DefaultAction );
                }
                QGraphicsScene::mousePressEvent( event );
                break;
            }
        default:
            {
                QGraphicsScene::mousePressEvent( event );
                break;
            }
             //QGraphicsScene::mousePressEvent( event );
        }
}

//рисуем новый объект (нужно только для секции)
void WorkPlace::mouseMoveEvent( QGraphicsSceneMouseEvent* event )
{
    switch ( m_currentAction )
    {

        case Connection_LineAction:
            {
                if (auto line = dynamic_cast<Connection_Line*>( currentItem )){
                    if (!line->isVisible()){
                        auto dx = event->scenePos().x() - m_previousPosition.x();
                        auto dy = event->scenePos().y() - m_previousPosition.y();
                        if (qAbs( dx ) > 5 || qAbs( dy ) > 5){ // фиксируем минимальный сдвиг
                            line->setVisible(true);
                        }
                    }else {
                        if ( event->modifiers() & Qt::SHIFT ){ // рисуем прямую линию
                            line->setEndPoint( QPointF( event->scenePos().x() , line->startPoint().y() ));
                        } else {
                            line->setEndPoint( event->scenePos() );
                        }
                        find_intersect_connection(line);
                    }
                } else {
                    qDebug()<<" Connection_Line ERROR \n";
                }

                break;
            }
        case SelectionAction:
            {
                if ( m_leftMouseButtonPressed )
                {
                    auto dx = event->scenePos().x() - m_previousPosition.x();
                    auto dy = event->scenePos().y() - m_previousPosition.y();
                    QGraphicsRectItem* rectangle = qgraphicsitem_cast<QGraphicsRectItem*>( currentItem );
                    rectangle->setRect( ( dx > 0 ) ? m_previousPosition.x() : event->scenePos().x(),
                                        ( dy > 0 ) ? m_previousPosition.y() : event->scenePos().y(),
                                        qAbs( dx ), qAbs( dy ) );
                }
                break;
            }

        default:
            {
                QGraphicsScene::mouseMoveEvent( event );
                break;
            }
    }
}

void WorkPlace::mouseReleaseEvent( QGraphicsSceneMouseEvent* event )
{
    if ( event->button() & Qt::LeftButton )
    {
        m_leftMouseButtonPressed = false;
    }

    switch ( m_currentAction )
    {
        case Connection_LineAction:
        {
            if (auto line = dynamic_cast<Connection_Line*>( currentItem )){
                if (!line->isVisible()){ //если так и не сдвинули с места объект, то удалянм его
                    line->disconnectAllItem();
                    removeItem(line);
                }
            } else {
                qDebug()<<" Connection_Line ERROR \n";
            }
            setCurrentAction( DefaultAction );
            break;
      }

        case SelectionAction:
        {
            if ( !m_leftMouseButtonPressed &&
                 !( event->button() & Qt::RightButton ) &&
                 !( event->button() & Qt::MiddleButton ) )
            {
                QGraphicsRectItem* rectangle = qgraphicsitem_cast<QGraphicsRectItem*>( currentItem );
                this->setSelectionArea(rectangle->shape());
                this->removeItem( currentItem );
                setCurrentAction( DefaultAction );
                currentItem = nullptr;
            }

            break;
        }

        default:
        {
            QGraphicsScene::mouseReleaseEvent( event );
            break;
        }
    }
}

void WorkPlace::wheelEvent(QGraphicsSceneWheelEvent *event)
{
    if( event->modifiers() & Qt::ControlModifier )
    {
        //update();
        double scaleFactor = 1.1;
        static double currentScale = 1.0;  // stores the current scale value.
        static const double scaleMin = 0.5; // defines the min scale limit.
        static const double scaleMax = 2;
        //QRect widgetRect = ui->graphicsView->geometry();
        //qDebug() << event->scenePos();
        auto pos = event->scenePos();
        auto view = views().at(0);
        view->setTransformationAnchor(QGraphicsView::AnchorViewCenter);
        if( event->delta() > 0 )
        {
            if (currentScale < scaleMax) {
                view->scale(scaleFactor,scaleFactor);
                currentScale *= scaleFactor;
            }
        }
        else
        {
            if (currentScale > scaleMin) {
                view->scale(1/scaleFactor,1/scaleFactor);
                currentScale /= scaleFactor;
            }
        }

        view->centerOn(pos);
        //            for(auto item : items(pos, Qt::IntersectsItemBoundingRect)){
        //                if ( auto rail_object = dynamic_cast<Base_rail_object *>( item ) ) {
        //                    view->centerOn(rail_object);
        //                    break;
        //                }
        //            }
        event->accept();
    }else{
        QGraphicsScene::wheelEvent( event );
    }

}

/*
 * Событие попадания пореносимого объекта в зону WorkPlace
*/
void WorkPlace::dragEnterEvent(QGraphicsSceneDragDropEvent *event) {
    auto mime = event->mimeData();
    auto base = Base_rail_object::createFromMime_static(m_GlobalContainerData_map, mime);
    //qDebug() << event->proposedAction();
    if (base){
        deselectItems();
        auto railObj = add_new_item_to_scene(base, event->scenePos() + QPointF(50,0) );
        setPreviousPosition(event->scenePos());
        currentItem = railObj;
        event->acceptProposedAction();
    }
}

void WorkPlace::dragMoveEvent(QGraphicsSceneDragDropEvent *event) {
    if (auto railObj = dynamic_cast<Base_rail_object*>( currentItem )){
        auto dx = event->scenePos().x() - m_previousPosition.x();
        auto dy = event->scenePos().y() - m_previousPosition.y();
        railObj->moveBy(dx,dy);
        slotMove(railObj, dx, dy);
        setPreviousPosition(QPointF(dx,dy) + m_previousPosition);
    }
}

void WorkPlace::dropEvent(QGraphicsSceneDragDropEvent *event) {
    //qDebug() << event->mimeData();
//    event->accept();
    event->acceptProposedAction();
}


void WorkPlace::keyPressEvent( QKeyEvent* event )
{
    switch ( event->key() )
    {
        case Qt::Key_Delete:
            {
                foreach ( QGraphicsItem* item, selectedItems() )
                {
                    if (auto rail_object = dynamic_cast<Base_rail_object *>( item )) {
                        rail_object->disconnectAllItem();
                    }
                    if (auto rail_section = dynamic_cast<Rail_section *>( item )) {
                        for(auto train_forward : rail_section->train()->connectedTrains_forward){
                            removeItem( train_forward );
                        }
                    }

                    if (auto conn = dynamic_cast<Connection *>( item )) {
                        break;
                    }


                    emit deletedItemSignal(item);
                    removeItem( item );

                }

                deselectItems();
                QGraphicsScene::keyPressEvent( event );
                break;
            }

        case Qt::Key_A:
            {
                if ( event->modifiers() & Qt::CTRL )
                {
                    deselectItems(); // для сброса Train
                    foreach ( QGraphicsItem* item, items() )
                    {
                        item->setSelected( true );
                    }

                    if ( selectedItems().length() == 1 )
                    {
                        emit selectItemSignal( selectedItems().at( 0 ) );
                    }
                }
                QGraphicsScene::keyPressEvent( event );
                break;
            }
        //поворот объекты
    case Qt::Key_R:
    {
        if ( event->modifiers() & Qt::CTRL )
        {
            foreach ( QGraphicsItem* item, selectedItems() )
            {
                if (auto rail_object = dynamic_cast<Base_rail_object *>( item ))
                    rail_object->cycleRotate();
            }
        }
        QGraphicsScene::keyPressEvent( event );
        break;
    }

        //копирования объекта
    case Qt::Key_C:
    {
        if ( event->modifiers() & Qt::CTRL )
        {
            m_listCopiedItems =  this->selectedItems();
            QRectF sourceRect;
            foreach( QGraphicsItem *current, selectedItems() ) {
                sourceRect = sourceRect.united( current->sceneBoundingRect() );
                m_topLeft_selected_point = sourceRect.topLeft();
            }
            //qDebug() << m_topLeft_selected_point;
            JsonSaver jsonSaver(m_GlobalContainerData_map);
            QJsonObject sceneCopyJson;
            jsonSaver.writeRailObjTojson(sceneCopyJson, m_listCopiedItems);
            QMimeData *mimeData = new QMimeData;
            mimeData->setData("text/json", QJsonDocument(sceneCopyJson).toJson());
            QClipboard *clipboard = QGuiApplication::clipboard();
            clipboard->setMimeData(mimeData);
        }
        QGraphicsScene::keyPressEvent( event );
        break;
    }
        //вставка объекта
    case Qt::Key_V:
    {
        if ( event->modifiers() & Qt::CTRL )
        {
            if (m_listCopiedItems.count()){ // означает, что сохранение было в текущем приложении
                auto delta_point = previousPosition() - m_topLeft_selected_point;
                foreach ( QGraphicsItem* item, m_listCopiedItems )
                {
                    if(auto odj = dynamic_cast<Base_rail_object *>(item)){
                        auto jsonObj = odj->serialize();
                        jsonObj["id"] = getUniqueID();
                        jsonObj["posX"] = delta_point.x() + odj->pos().x();
                        jsonObj["posY"] = delta_point.y() + odj->pos().y();
                        auto svgItem = odj->createFromJson(m_GlobalContainerData_map, jsonObj);
                        this->add_new_item_to_scene(svgItem);
                    }


//                    if(Rail_section* objItem = dynamic_cast<Rail_section *>( item )){ /* находим тип объекта секция */
//                        add_new_item_to_scene(nullptr , Actions::Rail_sectionAction, delta_point + objItem->pos());
//                    }

                }
                m_listCopiedItems.clear();
            }
            else{ // означает, что сохранение было в другом приложении
                QClipboard *clipboard = QGuiApplication::clipboard();
                if(clipboard->mimeData()->hasFormat("text/json")){
                    //const QByteArray * myData = qobject_cast<const QByteArray *>(clipboard->mimeData());
                    auto sceneMime = clipboard->mimeData();
                    if (sceneMime) {
                        auto Jdoc = QJsonDocument::fromJson(sceneMime->data("text/json"));
                        QJsonObject sceneJsonObj = Jdoc.object();
                        JsonSaver jsonSaver(m_GlobalContainerData_map);
                        jsonSaver.readRailObjFromjson(sceneJsonObj);
                    }
                }
            }
        }
        QGraphicsScene::keyPressEvent( event );
        break;
    }
    case Qt::Key_M:
    {
        if ( event->modifiers() & Qt::CTRL )
        {
            QTransform trans;
            foreach ( QGraphicsItem* item, selectedItems() )
            {
               if (auto rail_object = dynamic_cast<Base_rail_object *>( item )) {
                    rail_object->cycleMirror();
                }
            }
        }
        QGraphicsScene::keyPressEvent( event );
        break;
    }
    case Qt::Key_Z:
    {
        if ( event->modifiers() & Qt::CTRL )
        {
            //qDebug() << "QWERTY";
        }
        QGraphicsScene::keyPressEvent( event );
        break;
    }

    case Qt::Key_Right:
    {
        if (this->selectedItems().empty()){
            QGraphicsScene::keyPressEvent( event );
            break;
        }

        foreach (auto item , this->selectedItems() )
        {
            item->moveBy( 1 , 0 );
            if(auto rail_object = dynamic_cast<QGraphicsSvgItem *>( item )){
                find_intersect_connection(rail_object);
            }
        }
        break;
    }
    case Qt::Key_Left:
    {
        if (this->selectedItems().empty()){
            QGraphicsScene::keyPressEvent( event );
            break;
        }

        foreach (auto item , this->selectedItems() )
        {
            //Base_rail_object * rail_object = qgraphicsitem_cast<Base_rail_object *>( item );
            item->moveBy( -1 , 0 );
            //find_intersect_connection(rail_object);
        }
        break;
    }
    case Qt::Key_Up:
    {
        if (this->selectedItems().empty()){
            QGraphicsScene::keyPressEvent( event );
            break;
        }
        foreach (auto item , this->selectedItems() )
        {
            //Base_rail_object * rail_object = qgraphicsitem_cast<Base_rail_object *>( item );
            item->moveBy( 0 , -1 );
            //find_intersect_connection(rail_object);
        }
        break;
    }
    case Qt::Key_Down:
    {
        if (this->selectedItems().empty()){
            QGraphicsScene::keyPressEvent( event );
            break;
        }

        foreach (auto item , this->selectedItems() )
        {
            //Base_rail_object * rail_object = qgraphicsitem_cast<Base_rail_object *>( item );
            item->moveBy( 0 , 1 );
            //find_intersect_connection(rail_object);
        }
        break;
    }

    default:
        QGraphicsScene::keyPressEvent( event );
        break;
    }

    //QGraphicsScene::keyPressEvent( event );
}

void WorkPlace::deselectItems()
{
    foreach ( QGraphicsItem* item, selectedItems() )
    {
        item->setSelected( false );
    }
    selectedItems().clear();
}

// Движение всех выделенных элементов вместе с тем, что под мышкой
void WorkPlace::slotMove( Base_rail_object* signalOwner, qreal dx, qreal dy )
{
    QList<Train_forward*> HiddenTrain_forwards;

    foreach (auto & item , this->items() )
    {
        if(Train_forward* objItem = dynamic_cast<Train_forward *>( item )){
            if (!objItem->isVisible()){
                objItem->show();
                HiddenTrain_forwards.append(objItem);
            }

         }
    }
    foreach (auto item , this->selectedItems() )
    {
        if (Base_rail_object * rail_object = dynamic_cast<Base_rail_object *>( item )){
            if ( rail_object != signalOwner )
            {
               rail_object->moveBy( dx, dy );
            }
            find_intersect_connection(rail_object);
        }
    }
    foreach (auto item , HiddenTrain_forwards )
    {
        item->hide();
    }
}


Base_rail_object * WorkPlace::add_new_item_to_scene(Actions action, QPointF position, QObject* parent)
{
    m_previousPosition = QPointF(m_previousPosition.x() + 110,  m_previousPosition.y() );
    //auto property_tree = static_cast<QTreeWidget *>(m_GlobalContainerData_map["propertyRail_tree"]);
    Base_rail_object * svgItem = nullptr;
    if  (Rail_sectionAction == action){
        svgItem = new Rail_section(m_GlobalContainerData_map, getUniqueID() ); //Rail_section
    } else if (Rail_switchAction == action){
        svgItem = new Rail_switch(m_GlobalContainerData_map, getUniqueID());
    } else if (Rail_signalAction == action){
        svgItem = new Rail_signal(m_GlobalContainerData_map, getUniqueID());
    } else if (Rail_prot_areaAction == action){
        svgItem = new Rail_prot_area(m_GlobalContainerData_map, getUniqueID());
//    } else if (Rail_blok_edgeAction == action){
//        svgItem = new Rail_blok_edge(m_GlobalContainerData_map, getUniqueID());
    } else if (Rail_generatorENAction == action){
        svgItem = new Rail_generatorEN(m_GlobalContainerData_map, getUniqueID());
//    } else if (Train_forwardAction == action){
//        if(auto train = dynamic_cast<Train *>( parent )){
//            auto train_forward = new Train_forward(m_GlobalContainerData_map, m_MaxID++, train->parentID(), train->lastSpeedValue());
////            connect( train , &Train::TrainChangedVisibleSignal, train_forward, &Train_forward::ChangedVisibleSlot );
////            connect( train_forward, &Train_forward::changedSelectionSignal, train , &Train::childChangedSelectionSlot  );
//            svgItem = train_forward;
//        }
    } else if (Rail_station_edgeAction == action){
        svgItem = new Rail_station_edge(m_GlobalContainerData_map, getUniqueID());
    } else if (Connection_LineAction == action){
        svgItem = new Connection_Line( m_GlobalContainerData_map , getUniqueID());
    }else {
    }

    if(svgItem){
        //svgItem->setPos( position == QPointF(0,0) ? m_previousPosition : position);
        if (position != QPointF(0,0) )
            svgItem->setPos(position);
        else if(svgItem->pos() == QPointF(0,0)){
            svgItem->setPos(m_previousPosition);
            views().at(0)->centerOn(svgItem);
        }

        svgItem->setID( svgItem->ID() == 0 ? this->getUniqueID() : svgItem->ID() );
        this->addItem( svgItem );
        connect( svgItem , &Base_rail_object::clicked, this, &WorkPlace::setCurrentItem );
        connect( svgItem , &Base_rail_object::signalMove, this, &WorkPlace::slotMove );
        connect( svgItem , &Base_rail_object::childConnectionclicked, this, &WorkPlace::ConnectionClickSlot );
        connect( svgItem , &Base_rail_object::itemEditorActivated, this, &WorkPlace::itemEditorActivatedSlot );
        connect(this, &WorkPlace::deletedItemSignal, svgItem, &Base_rail_object::deletedItemSlot );
        connect(this, &WorkPlace::currentActionChanged, svgItem, &Base_rail_object::sceneActionChangedSlot );
        setCurrentAction(action);
        //svgItem->setFocus();

    }
    find_intersect_connection(svgItem);

    return svgItem;

}

/*****************************
* Добавляем новый объект на сцену, прявязываем к слотам, устанавливаем ID по необходимости
*/
Base_rail_object * WorkPlace::add_new_item_to_scene(Base_rail_object * svgItem, QPointF position, QObject* parent)
{
    m_previousPosition = QPointF(m_previousPosition.x() + 110,  m_previousPosition.y() );
    if(svgItem){
        if (position != QPointF(0,0) )
            svgItem->setPos(position);
        else if(svgItem->pos() == QPointF(0,0)){
            svgItem->setPos(m_previousPosition);
            views().at(0)->centerOn(svgItem);
        }

        if (svgItem->ID() == 0 )
            svgItem->setID(this->getUniqueID() );
        else if (svgItem->ID() >=  this->maxID())
            this->setMaxID( svgItem->ID()  + 1);


        this->addItem( svgItem );
        connect( svgItem , &Base_rail_object::clicked, this, &WorkPlace::setCurrentItem );
        connect( svgItem , &Base_rail_object::signalMove, this, &WorkPlace::slotMove );
        connect( svgItem , &Base_rail_object::childConnectionclicked, this, &WorkPlace::ConnectionClickSlot );
        connect(this, &WorkPlace::deletedItemSignal, svgItem, &Base_rail_object::deletedItemSlot );
        connect( svgItem , &Base_rail_object::itemEditorActivated, this, &WorkPlace::itemEditorActivatedSlot );
        connect(this, &WorkPlace::currentActionChanged, svgItem, &Base_rail_object::sceneActionChangedSlot );
        //setCurrentAction(action);
        //svgItem->setFocus();
    }
    find_intersect_connection(svgItem, false);

    return svgItem;

}


/*****************************
* Установка контекстного меня для сцены
*/
void WorkPlace::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    QGraphicsScene::contextMenuEvent( event );
    if(!event->isAccepted())
        m_ContextMenu.exec(event->screenPos());


}

/*****************************
* Инициализация шаблонов объектов ЖД автоматики
*/
void WorkPlace::initTemplate()
{
    /* эксперимент*/
    QList<QString> &test = get_base_rail_object();
    auto railElements = static_cast<QListView *>(m_GlobalContainerData_map["railElement"]);
    auto model = new QStandardItemModel();

    for(auto str : test){
        auto typeName = str.toLocal8Bit();
        int id = QMetaType::type(typeName);

        if (id != QMetaType::UnknownType) {
            void * myClassPtr = QMetaType::create(id);
            auto odj = static_cast<Base_rail_object *>(myClassPtr);
            auto item = odj->standardItem();
            if (item)  model->appendRow(item);
            ///QMetaType::destroy( id, myClassPtr);  //??????????????7
        }
    }
    railElements->setModel(model);
}

/*****************************
*    Действие при нажатии на одну из точек соединения
*/
void WorkPlace::ConnectionClickSlot(Connection* conn)
{
    deselectItems();
    auto centerConn =  conn->boundingRect().center();
    currentItem = add_new_item_to_scene(Connection_LineAction, conn->mapToScene(centerConn) );
    currentItem->setVisible(false);
    setPreviousPosition(conn->mapToScene(centerConn));
    setCurrentAction(Connection_LineAction);

}

/*****************************
*    Формирование состояние сцены при редактировании потомков базового объекта
*/
void WorkPlace::itemEditorActivatedSlot(Base_rail_object* item)
{
    currentItem = item;
    setCurrentAction(itemEditorAction);

}

//находим пересекающиеся соединения объектов на сцене
void WorkPlace::find_intersect_connection(QGraphicsSvgItem * selected_item, bool en_modified)
{
    bool find_intersection = false;
    for (auto child: selected_item->childItems()) {
        Connection* child_conn = dynamic_cast<Connection*>( child );  // перемешающие объекты

        if (child_conn){
            for (auto & sceneItem : items(child_conn->sceneBoundingRect(),Qt::IntersectsItemBoundingRect)) {
                Connection* scene_conn = dynamic_cast<Connection *>( sceneItem );
                if ((scene_conn != nullptr) && (scene_conn->m_parent_ID == 0) ) throw "Нулевой ID";
                if ((scene_conn != nullptr) && (scene_conn->m_parent_ID != child_conn->m_parent_ID) ){
                    //qDebug() << "child_conn=" << child_conn->sceneBoundingRect();
                    //qDebug() << "Nconn=" << child_conn->collidingItems().count();

                    scene_conn->connectWith(child_conn);
                    child_conn->connectWith(scene_conn);
//                    if (en_modified)
//                        child_conn->setStyle(scene_conn->Style());// перемешающие объекты БЕРУТ СТИЛЬ СТОЯЧИХ

                    child_conn->inheritStyle(scene_conn);// перемешающие объекты БЕРУТ СТИЛЬ СТОЯЧИХ

                    find_intersection = true;
                }
                if (find_intersection == false && child_conn->m_CurrentConnectState != Connection::NoConnect){
                    for (auto & Neighbour_conn: child_conn->m_Neighbour_connections) {
                        Neighbour_conn->disconnectWith(child_conn);
                        child_conn->disconnectWith(Neighbour_conn);
                    }
                }
            }
            if (find_intersection == false && child_conn->m_CurrentConnectState != Connection::NoConnect){
                for (auto & Neighbour_conn: child_conn->m_Neighbour_connections) {
                    Neighbour_conn->disconnectWith(child_conn);
                }
                child_conn->disconnectAll();
            }

        }
//        else if (auto child_svg = dynamic_cast<QGraphicsSvgItem *>( child )){  // перемешающие объекты)
//            find_intersect_connection(child_svg, en_modified);
//        }


        find_intersection = false;
    }

}


