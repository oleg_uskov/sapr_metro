#ifndef JsonSaver_H
#define JsonSaver_H

#include <QObject>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDialog>

#include "workplace.h"

#include "Base_rail_object.h"
//#include "Rail_switch.h"
//#include "Rail_section.h"
//#include "Rail_signal.h"
//#include "Blok_area.h"
//#include "Rail_prot_area.h"
//#include "Rail_generatorEN.h"
//#include "Train_forward.h"
//#include "Train.h"
//#include "Rail_station_edge.h"
//#include "Connection_Line.h"
//#include "Rail_platform.h"

class JsonSaver: public QObject
{
	Q_OBJECT

public:
    explicit JsonSaver(const QMap<QString, QObject *> & GlobalContainerData_map, QObject* parent = 0 );
    ~JsonSaver();
    using QObject::connect;

    bool writeRailObjTojson(QJsonObject &json, QList<QGraphicsItem *> items);
    bool writeOKdataTojson(QJsonObject &json);
    bool writeTableTojson(QJsonArray &json);
    bool readRailObjFromjson(const QJsonObject &json );
    bool readOKdataFromjson(const QJsonObject &json);
    bool readTableFromJson(const QJsonArray & arr);
    void createProjectJson(QJsonObject &ProjectJson);

    QJsonObject SerializeRailObj(Base_rail_object * objItem );
    bool DisplayRailObj(Base_rail_object * objItem , QJsonObject rail_obj);
    //bool DisplayTrain_forward(Rail_section * sect, QJsonArray trainsForward);
    QJsonObject * m_json_project;

signals:

public slots:

    bool saveProgectFile(QWidget *parent, bool save_as = false);
    bool restorProgectFile(QWidget *parent);

private:
    const QMap<QString, QObject *> &    m_GlobalContainerData_map;
    QString                             m_fileName;



};

#endif // JsonSaver_H
