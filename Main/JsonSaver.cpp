/**
* @file JsonSaver.cpp
* @brief  Серилизация и десерилизация проекта
* @author Усков Олег
* @copyright ТранспортныеТехнологии
* Ревизия:
*/


#include <QFile>
#include <QStringList>
#include <QDebug>
#include <QList>
#include <QList>
#include <QGraphicsItem>
#include <QFileDialog>
#include <QMessageBox>
#include <QListWidget>
#include <QGroupBox>
#include <QTableWidget>
#include "JsonSaver.h"
#include "AdressOK_model.h"
#include "RouteTable.h"
//#include "PortOK_model.h"
#include "mainwindow.h"


using namespace std;

#include <thread>

JsonSaver::JsonSaver(const QMap<QString, QObject *> & GlobalContainerData_map, QObject* parent ): QObject(parent),
m_GlobalContainerData_map(GlobalContainerData_map)
{

}

JsonSaver::~JsonSaver()
{

}


/** запись объекта json в файл */
bool JsonSaver::saveProgectFile(QWidget *parent , bool save_as)
{
    if (m_fileName.isEmpty() or save_as)
        m_fileName = QFileDialog::getSaveFileName(parent,
                                    tr("Сохранить проект"), "",
                                    tr("Rail JSON (*.rjsn);;All Files (*)"));
    if (m_fileName.isEmpty())
        return false;
    else
    {
        QFile saveFile(m_fileName);

        if (!saveFile.open(QIODevice::WriteOnly)) {
            qWarning("Не получилось сохранить файл");
//            QMessageBox::information(this, tr("Unable to open file"),
//                file.errorString());
            return false;
        }

        QJsonObject AllProjectObject;
        createProjectJson(AllProjectObject);


        saveFile.write(QJsonDocument(AllProjectObject).toJson() );

        saveFile.close();
    }

    return true;
}

/** создание json образ всего проекта  */
void JsonSaver::createProjectJson(QJsonObject &ProjectJson)
{
    WorkPlace * scene = static_cast<WorkPlace *>(m_GlobalContainerData_map["workplaceScene"]);
    ProjectJson["Version"] = MainWindow::version;

    QJsonObject StationObject;
    writeRailObjTojson(StationObject, scene->items());
    ProjectJson["RailObj"] = StationObject;

    QJsonObject OKdata;
    writeOKdataTojson(OKdata);
    ProjectJson["OKdata"] = OKdata;

    QJsonArray Tables;
    writeTableTojson(Tables);
    ProjectJson["Tables"] = Tables;
}

/** восстановление объекта json в файл */
bool JsonSaver::restorProgectFile(QWidget *parent)
{
    //WorkPlace * scene = static_cast<WorkPlace *>(m_GlobalContainerData_map["workplaceScene"]);

    QString fileName = QFileDialog::getOpenFileName(parent,
                                    tr("Открыть проект"), "",
                                    tr("Rail JSON (*.rjsn)"));
    if (fileName.isEmpty())
        return false;
    else
    {
        QFile openFile(fileName);
        QTextStream fileStream(&openFile);

        if (!openFile.open(QIODevice::ReadOnly| QIODevice::Text)) {
            QMessageBox::information(parent, tr("Не получилось открыть файл"),
            openFile.errorString());
            return false;
        }
        m_fileName = fileName;

        QString saveData = openFile.readAll();
        auto Jdoc = QJsonDocument::fromJson(saveData.toUtf8());

        QJsonDocument loadDoc(Jdoc);
        QJsonObject AllProjectObject = loadDoc.object();

        WorkPlace * workPlace = static_cast<WorkPlace *>(m_GlobalContainerData_map["workplaceScene"]);
        workPlace->clear();

        //bool (*readOKdataFromjson_prt)(const QJsonObject &json);
        if(AllProjectObject.contains("RailObj") && AllProjectObject["RailObj"].isObject()){
           readRailObjFromjson(AllProjectObject["RailObj"].toObject())  ; //std::thread t1(   );
        }

        if(AllProjectObject.contains("OKdata") && AllProjectObject["OKdata"].isObject()){
            readOKdataFromjson(AllProjectObject["OKdata"].toObject());
        }

        if(AllProjectObject.contains("Tables") && AllProjectObject["Tables"].isArray()){
            readTableFromJson(AllProjectObject["Tables"].toArray());
        }

        openFile.close();
    }

    return true;
}

/*!
    \fn bool JsonSaver::writeRailObjTojson(QJsonObject &json, WorkPlace * scene)

    Формирует обЪект \a json на основаниии данных со сцены \a scene
*/
bool JsonSaver::writeRailObjTojson(QJsonObject &json, QList<QGraphicsItem *> items )
{
//    QJsonArray sectionArray;

//    for (auto & item : items) { // считываем все объкты со сцены
//        if(Rail_section* objItem = dynamic_cast<Rail_section *>( item )){ /* находим тип объекта Секция */
//            sectionArray.append( objItem->serialize() );
//        }
//    }
//    json["sections"]            = sectionArray;

    QMap< QString, QJsonArray > container;
    for (auto & item : items) { // считываем все объкты со сцены
        if(Base_rail_object* objItem = dynamic_cast<Base_rail_object *>( item )){ /* находим тип объекта Секция */
            //qDebug() << objItem->JsonConteinerName();
            if (!objItem->JsonConteinerName().isEmpty()){
                auto name = objItem->JsonConteinerName();
                container[name].append(objItem->serialize());
            }
        }
    }

    QMapIterator<QString, QJsonArray> i (container);
    while (i.hasNext()) {
        i.next();
        json[i.key()]           =  i.value();
    }
    return true;
}



/*!
    \fn bool JsonSaver::writeOKdataTojson(QJsonObject &json, WorkPlace * scene)
    Формирует обЪект \a json на основаниии данных с карты адресов \a scene
*/
bool JsonSaver::writeOKdataTojson(QJsonObject &json)
{
    QJsonArray ObjControlArray;
    auto adressOK_model = static_cast<AdressOK_model *>(m_GlobalContainerData_map["AdressOK_model"]);
    for(auto  PortOK_model : adressOK_model->PortOK_model_list){
        for(int i = 0; i <  PortOK_model->columnCount() ; i++){
            for(int j = 0; j <  PortOK_model->rowCount() ; j++){
                auto item = PortOK_model->item(j,i);
                if (item){
                    QVariant PropertyOK_var = item->data(portOK_DataRole::PropertyOK);
                    auto PropertyOK_model = PropertyOK_var.value<QJsonModel *>();
                    ObjControlArray.append(PropertyOK_model->json().object());
                }
            }
        }
    }
    json["ObjControlArray"] = ObjControlArray;
    return true;
}

/*!
 *   Формирует обЪект  json на основаниии данных с таблицы маршрутов
*/
bool JsonSaver::writeTableTojson(QJsonArray & arr)
{
    auto routeTable = static_cast<RouteTable *>(m_GlobalContainerData_map["RouteTable"]);
    arr.append( routeTable->serializeBaseObj() );
    return true;
}

/*!
    Формирует обЪекты для отрисовки на сцене
*/
bool JsonSaver::readRailObjFromjson(const QJsonObject &json)
{
    QJsonArray railObjArray;

    //Base_rail_object * svgItem = nullptr;
    QJsonObject rail_obj;
    for(auto &  key : json.keys()){
        if(json[key].isArray()){
            railObjArray = json[key].toArray();
            for(auto  rail_json : railObjArray){
               rail_obj = rail_json.toObject();
               auto typeName = rail_obj["type"].toString().toLocal8Bit();
               int id = QMetaType::type(typeName);

               if (id != QMetaType::UnknownType) {
                   void * myClassPtr = QMetaType::create(id);
                   //auto Qodj = static_cast<QObject *>(myClassPtr);
                   auto odj = static_cast<Base_rail_object *>(myClassPtr);
                   auto svgItem = odj->createFromJson(m_GlobalContainerData_map, rail_obj);
                   DisplayRailObj(svgItem, rail_obj);
                   ///QMetaType::destroy( id, myClassPtr);  //??????????????7
               }
            }
        }
    }


//    if(json.contains("sections") && json["sections"].isArray()){
//        railObjArray = json["sections"].toArray();
//        for(auto  rail_json : railObjArray){ /* находим тип объекта секция */
//           rail_obj = rail_json.toObject();
//           //svgItem = new Rail_section(m_GlobalContainerData_map, rail_obj["id"].toInt()); //Rail_section
//           svgItem = Rail_section::createFromJson(m_GlobalContainerData_map, rail_obj);
//           DisplayRailObj(svgItem, rail_obj);
//        }
//    }


    return true;
}

/*!
    Формирует прототип жд объекта на сцене
*/
bool JsonSaver::DisplayRailObj(Base_rail_object * objItem , QJsonObject rail_obj)
{
    Q_UNUSED(rail_obj);
    WorkPlace * workPlace = static_cast<WorkPlace *>(m_GlobalContainerData_map["workplaceScene"]);
    workPlace->add_new_item_to_scene(objItem);
    return true;
}



/*!
    Формирует карту контроллеров
*/
bool JsonSaver::readOKdataFromjson(const QJsonObject &json)
{
    QJsonArray ObjControlArray;
    if(json.contains("ObjControlArray") && json["ObjControlArray"].isArray())
        ObjControlArray = json["ObjControlArray"].toArray();

    auto adressOK_model = dynamic_cast<AdressOK_model *>(m_GlobalContainerData_map["AdressOK_model"]);
    for(auto & port : adressOK_model->PortOK_model_list){
        port->clear();
    }

    QMap< QString, QList<QTableWidgetItem *> > TypeOK_container;

    for(auto  objControl : ObjControlArray){
        QJsonObject obj = objControl.toObject();
        if(obj.contains("2)Адрес")){
            auto fullAddr_str = obj["2)Адрес"].toString();
            auto type = obj["1)Тип ОК"].toString();
            bool ok;
            auto fullAddr = fullAddr_str.toUInt(&ok, 16);
            int numberOK  = (fullAddr >> 1) & 0x07;
            int lowPort   = (fullAddr >> 4) & 0x0f;
            int highPort  = (fullAddr >> 9) & 0x0f;

            QJsonModel * m_PropertyModel = new QJsonModel(QJsonDocument(obj));
            auto var = QVariant::fromValue(m_PropertyModel);
            auto  PortOK_model = adressOK_model->PortOK_model_list.at(highPort);
            QTableWidgetItem * item = new QTableWidgetItem();
            item->setData(portOK_DataRole::PropertyOK, var);
            item->setData(portOK_DataRole::InitFlag, true);
            item->setData(portOK_DataRole::TypeOkRole, type);
            item->setData(portOK_DataRole::ReservOK, obj["3)Резерв"].toString());
            //qDebug() << "portOK_DataRole::ReservOK ="  << item->data(portOK_DataRole::ReservOK).toBool();

            if (obj["3)Резерв"].toString() == "true"){
                QLinearGradient linearGrad(QPointF(50, 15), QPointF(60, 25));
                linearGrad.setColorAt(0, Qt::green);
                linearGrad.setColorAt(1, Qt::yellow);
                item->setBackground(QBrush(linearGrad));
            }
            else
            {
                item->setBackground(QBrush(Qt::green));
            }

            QString type_and_address = type + " 0x" + fullAddr_str;
            item->setData(Qt::DisplayRole, type_and_address);
            item->setData(portOK_DataRole::hexAddrRole, fullAddr);
            item->setFont(QFont("Times", 10, QFont::Bold));

            PortOK_model->setItem(numberOK,lowPort,item);

            TypeOK_container[type].append( item->clone() );

        }
    }
/**
 *Заполнение таблицы ОК для формирования стоек
 */
    auto OK_typeList = dynamic_cast<QTableWidget *>(m_GlobalContainerData_map["OK_typeList"]);
    QMapIterator<QString, QList<QTableWidgetItem *>> i (TypeOK_container);
    OK_typeList->setColumnCount( TypeOK_container.size());

    int col = 0;
    while (i.hasNext()) {
        i.next();
        OK_typeList->setHorizontalHeaderItem(  col, new QTableWidgetItem(i.key()) );
        for (int row = 0; row < i.value().count(); row++){
            OK_typeList->setItem( row , col , i.value().at(row));
        }
        col++;
    }
/**********************************************************************/

    return true;
}


/*!
 *   Заполнение таблицы маршрутов
*/
bool JsonSaver::readTableFromJson(const QJsonArray & arr)
{
    auto routeTable = static_cast<RouteTable *>(m_GlobalContainerData_map["RouteTable"]);
    for(auto  table : arr){ // пока что таблица одна
        routeTable->initFromJson(  table.toObject() );
    }

    //arr.append( routeTable->serializeBaseObj() );
    return true;
}


