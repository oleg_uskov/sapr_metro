#ifndef MAIN_H
#define MAIN_H

//#include "Base_rail_object.h"
#include <QGraphicsItem>
/*
*
*/
//QMap<QString, QObject *> ObjGlobalNet;
using globalNet_t    = QMap<QString, QObject *> ;
using STR            = QString;

namespace Main
{



enum Actions
{
    DefaultAction,
    SelectionAction,
    Rail_sectionAction,
    Rail_switchAction,
    Rail_signalAction,
    Rail_prot_areaAction,
    Rail_blok_edgeAction,
    Rail_generatorENAction,
    Train_forwardAction,
    Rail_station_edgeAction,
    Connection_LineAction,
    itemEditorAction,
    nameStationAction,
    nameNeighStationAction,
};

enum Types
{
    Base_rail_objectType = QGraphicsItem::UserType + 1,
    Rail_sectionType,
    Rail_switchType,
    Rail_signalType,
    Rail_prot_areaType,
    Rail_blok_edgeType,
    Rail_generatorENType,
    Train_forwardType,
    TrainType,
    Rail_station_edgeType,
    Connection_LineType,
    ConnectionType,
    Rail_platformType,
};



} // MAIN

#endif // MAIN_H
