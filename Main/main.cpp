#include "mainwindow.h"

#include <QApplication>
//#include <QQmlApplicationEngine>
//#include "JArrayColumnModel.h"
//#include "JArrayTableModel.h"
//#include "QJsonTableModel.h"
//Q_DECLARE_METATYPE(QList<QTreeWidgetItem *>)
//#include <QtQml/qqml.h>
//#include <QtQml/qqmlmoduleregistration.h>


//void qml_register_types_QJsonTableModel()
//{
//    qmlRegisterTypesAndRevisions<QJsonTableModel>("QJsonTableModel", 1);
//    qmlRegisterModule("QJsonTableModel", 1, 0);
//}

int main(int argc, char *argv[])
{
    setlocale(LC_CTYPE, "rus"); // вызов функции настройки локали

    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    //qputenv("QML2_IMPORT_PATH", QString("/home/uskov_o/Qt/5.15.2/gcc_64/qml/").toLatin1());
    //qputenv("QML_IMPORT_PATH", QString("/home/uskov_o/Qt/5.15.2/gcc_64/qml/").toLatin1());
    //qmlRegisterType<QJsonTableModel>("my.tablemodel", 1, 0, "QJsonTableModel");

//    QQmlApplicationEngine engine;
//    engine.addImportPath("qrc:/");
//    engine.load(QUrl(QStringLiteral("qrc:/Base_table_view.qml")));
//    if (engine.rootObjects().isEmpty())
//        return -1;

    //qmlRegisterType<JArrayColumnModel>("tablemodel.backend", 1, 0, "JColumnModel");
    //qmlRegisterType<JArrayTableModel>("tablemodel.backend", 1, 0, "JTableModel");
    //qmlRegisterAnonymousType<qmlColumnModel>("jsonTableModel", 1);

//! [0]
    //qRegisterMetaType<qmlColumnModel>();


    //static const QQmlModuleRegistration registration("QJsonTableModel", 1, qml_register_types_QJsonTableModel);


    return a.exec();
}
