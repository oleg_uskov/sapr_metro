#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QColorDialog>
#include <QMessageBox>
#include <QBrush>
#include <QPen>
#include <QToolButton>
#include <QDebug>
#include "QFileDialog"
#include <QSvgGenerator>
#include <QTableWidget>
#include <QTimeLine>
#include <QProgressBar>
#include <QGraphicsView>
#include <QWheelEvent>
#include <QtMath>
#include <QScrollBar>
#include <QGraphicsDropShadowEffect>
#include <QModelIndex>
#include <QProcess>
#include <QProcessEnvironment>
#include <QMap>
//#include <QQuickView>
#include <QSettings>

#include "Rail_section.h"
#include "Rail_signal.h"
#include "Rail_switch.h"
#include "Rail_station_edge.h"
#include "Blok_area.h"
#include "QJsonTableModel.h"

#include "RouteTable.h"

using namespace Main;
//QTextStream textStream(stdin);
QMap<QString, QObject *> registersdSceneObject ;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(windowTitle() + " версия " +  QString::number(version));

//    template_railScene = new QGraphicsScene(this );
//    template_railScene->setSceneRect( 0, 0, 100, 1000 );
    adressOK_model = new AdressOK_model(ui->RailOKWidjList, ui->PortWidjList, ui->propertyOK_tree);

    GlobalContainerData_map["propertyRail_tree"]       =  ui->propertyRail_tree;
//    GlobalContainerData_map["template_railScene"]      =  template_railScene;

    GlobalContainerData_map["PortWidjList"]            =  ui->PortWidjList;
    GlobalContainerData_map["RailOKWidjList"]          =  ui->RailOKWidjList;
    GlobalContainerData_map["AdressOK_model"]          =  adressOK_model;
    GlobalContainerData_map["show_generator_chbox"]    =  ui->show_generator_chbox;
    GlobalContainerData_map["show_speed_chbox"]        =  ui->show_speed_chbox;
    GlobalContainerData_map["show_BU_chbox"]           =  ui->show_BU_chbox;
    GlobalContainerData_map["OK_typeList"]             =  ui->OK_typeList;
    GlobalContainerData_map["railElement"]             =  ui->railElement;
    GlobalContainerData_map["Consol"]                  =  ui->Consol;


//    ui->propertyRail_tree->collapseAll();
//    ui->propertyOK_tree->collapseAll();

    jsonSaver = new JsonSaver(GlobalContainerData_map);
    m_RestExel_API = new RestExel(GlobalContainerData_map);
    ui->railPlan_splitter->setStretchFactor(0,5);
    ui->railPlan_splitter->setStretchFactor(1,10);
    ui->railPlan_splitter->setStretchFactor(2,3);

    ui->railElement->setDragDropMode(QAbstractItemView::DragOnly);


    setStyleSheet( "QMenu::item:selected {border: 1px solid black;}" );


    workplaceScene = new WorkPlace(GlobalContainerData_map , this );
    GlobalContainerData_map["workplaceScene"]          =  workplaceScene;
    workplaceScene->setSceneRect( 0, 0, 10000, 1000 );
    ui->graphicsView->setScene( workplaceScene );
    ui->graphicsView->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
    ui->graphicsView->centerOn(0,0);


//    ui->template_railView->setScene( template_railScene );
//    ui->template_railView->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
//    ui->template_railView->centerOn(0,0);

    //ui->propertyRail_tree->setHeaderLabels({"Значение", "Свойство"});
    ui->propertyRail_tree->setAlternatingRowColors(true);
    ui->propertyRail_tree->setStyleSheet("alternate-background-color: lightGray; background-color: white;");

    //ui->RailObjControlList->setModel(model);

    //ui->propertyOK_tree->setHeaderLabels({"Значение", "Свойство"});
    ui->mapOK_splitter->setStretchFactor(0,10);
    ui->mapOK_splitter->setStretchFactor(1,3);


    /* Запуск QML
    QQuickView *qmlView = new QQuickView();
    QWidget *container = QWidget::createWindowContainer(qmlView, this); //ui->tables
    qmlView->setSource(QUrl(QStringLiteral("qrc:/Base_table_view.qml")));
    qmlView->setInitialProperties({{"model", QVariant::fromValue(model)}});
    //qmlView->setSource(QUrl(QStringLiteral("qrc:/tempalete/qml/Route_table.qml")));
    ui->verticalLayout_route->addWidget(container);
    */

    auto table = new RouteTable(ui->tables);
    GlobalContainerData_map["RouteTable"]                  =  table;
    //writeSettings();
    readSettings();

    connects();
    styleSheets();
}

MainWindow::~MainWindow()
{
    delete ui;
//    delete currentRail_switch;
//    delete currentRail_section;
//    delete currentRail_signal;
    delete jsonSaver;
    //qDeleteAll (GlobalContainerData_map);
}

void MainWindow::connects()
{
    connect( ui->Rail_switch_button, &QPushButton::clicked, [ = ]()
    {
        workplaceScene->add_new_item_to_scene( Main::Rail_switchAction) ;//Main::ActionRail_switch
    } );
    connect( ui->Rail_section_button, &QPushButton::clicked, [ = ]()
    {
        workplaceScene->add_new_item_to_scene( Main::Rail_sectionAction); //
    } );
    connect( ui->Rail_signal_button, &QPushButton::clicked, [ = ]() {
        workplaceScene->add_new_item_to_scene( Main::Rail_signalAction); //
    } );
    connect( ui->Rail_blok_edge_button, &QPushButton::clicked, [ = ]()  {
        //workplaceScene->add_new_item_to_scene( Main::Rail_blok_edgeAction); //
        workplaceScene->add_new_item_to_scene( new Blok_area(GlobalContainerData_map , workplaceScene->getUniqueID() ));
    } );
    connect( ui->Rail_prot_area_button, &QPushButton::clicked, [ = ]()  {
        workplaceScene->add_new_item_to_scene(Main::Rail_prot_areaAction); //
    } );
    connect( ui->Rail_generatorEN_button, &QPushButton::clicked, [ = ]() {
        workplaceScene->add_new_item_to_scene(); //Main::Rail_generatorENAction
    } );
    connect( ui->Rail_station_edge_button, &QPushButton::clicked, [ = ]() {
        workplaceScene->add_new_item_to_scene(Main::Rail_station_edgeAction); //
    } );


//    connect( workplaceScene, &WorkPlace::selectItemSignal, this, &MainWindow::selectItem );
//    connect( workplaceScene, &WorkPlace::newSelectItemSignal, this, &MainWindow::selectNewItem );

    //connect( workplaceScene, &WorkPlace::selectionChanged, this, &MainWindow::checkSelections );

    connect( ui->save_action, &QAction::triggered, [ = ](){ jsonSaver->saveProgectFile(this, true); } );
    connect( ui->open_action, &QAction::triggered, [ = ](){ jsonSaver->restorProgectFile(this); } );

    connect( ui->action_Exel,               &QAction::triggered,   m_RestExel_API,  &RestExel::sendToServer );
    connect( ui->action_ping,               &QAction::triggered,   m_RestExel_API,  &RestExel::pingToServer );
    connect( ui->action_stationValidation,  &QAction::triggered,   m_RestExel_API,  &RestExel::validateToServer );
    connect( ui->DownLoad_btn,              &QPushButton::clicked, m_RestExel_API,  &RestExel::download );
    connect( ui->Status_btn,                &QPushButton::clicked, m_RestExel_API,  &RestExel::getStatusToServer );

}

void MainWindow::styleSheets()
{
    setWindowIcon( QIcon( ":/icon/Logo_SE.jpg" ) );
}

void MainWindow::keyPressEvent( QKeyEvent* event )
{
    switch ( event->key() )
    {
        case Qt::Key_S:
            {
                if ( event->modifiers() & Qt::CTRL )
                {
                    jsonSaver->saveProgectFile(this);
                }
                break;
            }

    default:
        break;
    }

    QMainWindow::keyPressEvent( event );
}

void MainWindow::writeSettings()
{
    //QSettings settings("Stalenergo", "saprlogic");
    QSettings settings( QSettings::IniFormat, QSettings::UserScope, "Stalenergo", "saprlogic");

    settings.beginGroup("HostParametrs");
    settings.setValue("IP", RestExel::IPadress);
    settings.setValue("port", RestExel::port);
    settings.endGroup();
}

void MainWindow::readSettings()
{
    //QSettings settings("stalenergo", "saprlogic");
    QSettings settings( QSettings::IniFormat, QSettings::UserScope, "Stalenergo", "saprlogic");
    qDebug() << "settings.status: " << settings.status();
    if (settings.status() == QSettings::NoError){
        settings.beginGroup("HostParametrs");
        if(!settings.contains("IP"))
            writeSettings();
        RestExel::IPadress = settings.value("IP",   QString(RestExel::defaultIPadress) ).toString();
        RestExel::port     = settings.value("port", QString(RestExel::defaultPort) ).toString();
        settings.endGroup();
    }

}

