#ifndef WORKPLACE_H
#define WORKPLACE_H

#include <QObject>
#include <QGraphicsScene>
#include <QSvgRenderer>
#include <QTreeWidget>
#include <QPointF>



#include <QGraphicsPathItem>
#include <QGraphicsSvgItem>
//#include <QSvgRenderer>
//#include <QGraphicsItemGroup>
//#include <QGraphicsSceneDragDropEvent>

//#include <QDrag>
#include <QMenu>
#include <QAction>
//#include <QStandardItemModel>




//#include "JsonSaver.h"

#include "main.h"

using namespace Main;

class QGraphicsSceneMouseEvent;
class QKeyEvent;
class JsonSaver;
class Base_rail_object;
class QGraphicsScene;
class QGraphicsSceneDragDropEvent;

class QGraphicsSvgItem;
class QPointF;
class QMenu;
class Connection;

class WorkPlace : public QGraphicsScene
{
	Q_OBJECT

	Q_PROPERTY( Actions currentAction READ currentAction WRITE setCurrentAction NOTIFY
				currentActionChanged )
	Q_PROPERTY( QPointF previousPosition READ previousPosition WRITE setPreviousPosition NOTIFY
                previousPositionChanged )

public:
	explicit WorkPlace(const QMap<QString, QObject *> & GlobalContainerData_map, QObject* parent = 0 );
	~WorkPlace();

    using QGraphicsScene::items;

	Actions currentAction() const;
	QColor color_1() const;
	QPointF previousPosition() const;

	void setCurrentAction( Actions action );
	void setPreviousPosition( const QPointF previousPosition );
	void initTemplate( void );
	void setMaxID( const int MaxID ) {m_MaxID = MaxID;   }
	int  maxID( void ) const         { return m_MaxID;   }
    int  getUniqueID( void )         { return m_MaxID++; }

//    template <typename T = Base_rail_object> //
//    T * add_new_item_to_scene(Actions type = Actions::DefaultAction, QPointF position = QPointF(0,0), QObject* parent = nullptr);

    Base_rail_object * add_new_item_to_scene(Actions type = Actions::DefaultAction, QPointF position = QPointF(0,0), QObject* parent = nullptr);
    Base_rail_object * add_new_item_to_scene(Base_rail_object *, QPointF position = QPointF(0,0), QObject* parent = nullptr);
    const QMap<QString, QObject *> &         m_GlobalContainerData_map;


signals:
	void previousPositionChanged();
	void currentActionChanged( Actions action);
	void selectItemSignal( QGraphicsItem* item );
    void newSelectItemSignal( QGraphicsItem* item );
    void deletedItemSignal( QGraphicsItem* item );

protected:
	void mousePressEvent( QGraphicsSceneMouseEvent* event )     override;
	void mouseMoveEvent( QGraphicsSceneMouseEvent* event )      override;
	void mouseReleaseEvent( QGraphicsSceneMouseEvent* event )   override;
//	void mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event) override;
	void keyPressEvent( QKeyEvent* event ) override;
//    void mouseGrabberItem( QKeyEvent* event )                      ;
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override;
    void wheelEvent(QGraphicsSceneWheelEvent *event) override;
    void dragEnterEvent(QGraphicsSceneDragDropEvent *event)    override;
//    void dragLeaveEvent(QGraphicsSceneDragDropEvent *event)    override;
    void dragMoveEvent(QGraphicsSceneDragDropEvent *event)     override;
    void dropEvent(QGraphicsSceneDragDropEvent *event)         override;

private slots:
	void deselectItems();
//	void color( QColor m_color );
//	void newRail_switch(Rail_switch* rect);


public slots:
    void slotMove( Base_rail_object* signalOwner, qreal dx, qreal dy );
    void setCurrentItem( Base_rail_object * signalOwner);


    void find_intersect_connection(QGraphicsSvgItem * item, bool en_modified = true);
    void ConnectionClickSlot(Connection* conn );
    void itemEditorActivatedSlot(Base_rail_object* item);

private:

    //QGraphicsScene*		scene;
    //Base_rail_object*   currentItem;
    QGraphicsItem*                     currentItem;
	Actions      		               m_currentAction;
	Actions 			               m_previousAction;
	QPointF				               m_previousPosition;
	QPointF				               m_topLeft_selected_point;
	bool				               m_leftMouseButtonPressed;
    QList<QGraphicsItem*>              m_listCopiedItems;

    QColor                             m_colorW;
    int                                m_MaxID;

    QMenu                              m_ContextMenu;
};

//Q_DECLARE_METATYPE(WorkPlace)

#endif // WORKPLACE_H
