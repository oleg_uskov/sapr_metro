#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QWidget>
#include <QUndoStack>


#include "JsonSaver.h"
#include "workplace.h"
#include "main.h"
#include "AdressOK_model.h"
#include "RestExel.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
    //Q_PROPERTY( int borderWidth READ borderWidth WRITE setBorderWidth NOTIFY borderWidthChanged )

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    static constexpr float version = 4.12;

signals:
    void borderWidthChanged( const int& width );
    void Rail_sectionWidthChanged( const int& width );

protected:
    void keyPressEvent( QKeyEvent* event ) override;

public slots:
    void connects();
    void styleSheets();

private slots:
//    void on_sendToServer();
//    void on_pingToServer();
    void writeSettings();
    void readSettings();

private:
    Ui::MainWindow *    ui;
    AdressOK_model *    adressOK_model;

    //QGraphicsScene*		scene;

    QGraphicsRectItem*  rect;
    QUndoStack*         undoStack;

    JsonSaver*			jsonSaver;
    WorkPlace*			workplaceScene;
    QGraphicsScene*		template_railScene;
    QString				path;
//    Rail_switch*		currentRail_switch   = nullptr;
//    Rail_section*		currentRail_section  = nullptr;
//    Rail_signal*        currentRail_signal   = nullptr;

    //MapOK *             mapOK;

    int					m_index          = 0;
    bool                m_check          = true;

    bool                checkShadow      = false;

    //QGraphicsView*      m_view;
    int                 m_valueEffect;

    QMap<QString, QObject *> GlobalContainerData_map;
    RestExel *               m_RestExel_API;


};
#endif // MAINWINDOW_H
