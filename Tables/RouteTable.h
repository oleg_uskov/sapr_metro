#ifndef ROUTETABLE_H
#define ROUTETABLE_H

#include <QObject>
#include <QJsonObject>

class QTableWidget;

class RouteTable : public QObject
{
    Q_OBJECT
public:
    explicit RouteTable(QWidget *parent = nullptr);


    QJsonObject serializeBaseObj() const;
    void initFromJson(const QJsonObject &  table_obj);
    void setTableStyle() const;


signals:

public slots:
    void cellChangedSlot(int row, int column);
private:
    QTableWidget * table ;
    QString m_tableName;

};

#endif // ROUTETABLE_H
