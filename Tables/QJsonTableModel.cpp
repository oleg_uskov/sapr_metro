#include "QJsonTableModel.h"
#include <QJsonObject>
#include <QJsonArray>

QJsonTableModel::QJsonTableModel( const QJsonTableModel::Header& header, QObject * parent )
    : QAbstractTableModel( parent )
    , m_header( header )
{
}

QJsonTableModel::QJsonTableModel( QObject * parent )
    : QAbstractTableModel( parent )
    //, m_header( header )
{

}

bool QJsonTableModel::setJson(const QJsonDocument &json)
{
    return setJson( json.array() );
}

bool QJsonTableModel::setJson( const QJsonArray& array )
{
    //beginResetModel();
    //beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_json = array;
    //endResetModel();
    //endInsertRows();
    return true;
}

QVariant QJsonTableModel::rows() const
{
    return m_json;
}

void QJsonTableModel::setRows(const QVariant &rows)
{
//    if (rows.userType() != qMetaTypeId<QJSValue>()) {
//        qmlWarning(this) << "setRows(): \"rows\" must be an array; actual type is " << rows.typeName();
//        return;
//    }
//    const QJSValue rowsAsJSValue = rows.value<QJSValue>();
//    const QVariantList rowsAsVariantList = rowsAsJSValue.toVariant().toList();
//    if (rowsAsVariantList == mRows) {
//        // No change.
//        return;
//    }
//    if (!componentCompleted) {
//        // Store the rows until we can call doSetRows() after component completion.
//        mRows = rowsAsVariantList;
//        return;
//    }
//    doSetRows(rowsAsVariantList);

    //m_json = rows.value<QJsonArray>();
    //emit rowsChanged();

}

QVariant QJsonTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if( role != Qt::DisplayRole )
    {
        return QVariant();
    }

    switch( orientation )
    {
    case Qt::Horizontal:
        return m_header[section]["title"];
    case Qt::Vertical:
        //return section + 1;
        return QVariant();
    default:
        return QVariant();
    }

}

int QJsonTableModel::rowCount(const QModelIndex &parent ) const
{
    return m_json.size();
    //return 2;
}

int QJsonTableModel::columnCount(const QModelIndex &parent ) const
{
    return m_header.size();
    //return 2;
}


QJsonObject QJsonTableModel::getJsonObject( const QModelIndex &index ) const
{
    const QJsonValue& value = m_json[index.row() ];
    return value.toObject();
}

QVariant QJsonTableModel::data( const QModelIndex &index, int role ) const
{
    switch( role )
    {

    case Qt::EditRole:
    case Qt::DisplayRole:
    {
        QJsonObject obj = getJsonObject( index );
        const QString& key = m_header[index.column()]["index"];
        if( obj.contains( key ))
        {
            QJsonValue v = obj[ key ];

            if( v.isString() )
            {
                return v.toString();
            }
            else if( v.isDouble() )
            {
                return QString::number( v.toDouble() );
            }
            else
            {
                return QVariant();
            }
        }
        else
        {
            return QVariant();
        }
    }
    case Qt::ToolTipRole:
        return QVariant();
    default:
        return QVariant();
    }
}

bool QJsonTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    int col = index.column();
    int row = index.row();

    if (Qt::EditRole == role) {
        auto jrow = m_json[ row ];
        auto obj = jrow.toObject();
        const QString& key = m_header[index.column()]["index"];
        if( obj.contains( key ))
        {
            QJsonValue v = obj[ key ];

            if( v.isString() )
            {
                obj[key] = value.toString();
                m_json[ row ] = obj;
            }
            else if( v.isDouble())
            {
                obj[key] = QString::number( value.toDouble() );
                m_json[ row ] = obj;
            }
            else
            {
                QString("error");
            }
        }
        emit dataChanged(index, index, {Qt::EditRole});
        return true;
    }

    return false;
}

Qt::ItemFlags QJsonTableModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
}
