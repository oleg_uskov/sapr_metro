#include "RouteTable.h"
#include "QJsonTableModel.h"
#include <QHBoxLayout>
#include <QTabWidget>
#include <QTableView>
#include <QTableWidget>
#include <QJsonArray>

RouteTable::RouteTable(QWidget *parent) :
    QObject(parent),
    m_tableName("Маршруты")
{
    auto tab = static_cast<QTabWidget *>( parent ) ;
    ///////////////////////////////QTableView //////////////////////////////////
//    QJsonTableModel::Header header;

//    header.push_back( QJsonTableModel::Heading( { {"title","№ маршрута" },    {"index","numb" } }) );
//    header.push_back( QJsonTableModel::Heading( { {"title","От светофора"   },    {"index","begin"} }) );
//    header.push_back( QJsonTableModel::Heading( { {"title","До"             },    {"index","end"}   }) );


//    QString list = R"(   [  {"numb":1,  "begin":"НК-76",  "end":" "}, {"numb":2,  "begin":"НК-82",  "end":" "}   ]   )";

//    QJsonDocument doc1 = QJsonDocument::fromJson(list.toUtf8());

//    auto view = new QTableView();
//    if (doc1.isArray()) {
//        auto model = new QJsonTableModel(header);
//        view->setModel(model);
//        model->setJson(doc1.array());

//    }
//    tab->addTab(view, "Маршруты");

    ///////////////////////////////QTableWidget //////////////////////////////////
    table = new QTableWidget(20,11);
    connect( table, &QTableWidget::cellChanged, this,  &RouteTable::cellChangedSlot );
    table->setHorizontalHeaderLabels({"№ маршрута" , "От светофор", "До ", "Секции маршрута" , "Доп. Секции маршрута" , "Негабаритные секции маршрута" ,  "Предмаршрутные секции" ,
                                      "Положение стрелок в маршруте", "Враждебные маршруты", "СУ", "З"});
    this->setTableStyle();
    table->resizeColumnsToContents();
    tab->addTab(table, m_tableName);
}

QJsonObject RouteTable::serializeBaseObj() const
{
    QJsonObject table_obj;
    table_obj["name"] = m_tableName;

    QJsonArray headerArray;
    for(int j = 0; j <  table->columnCount() ; j++){
        auto item = table->horizontalHeaderItem(j);
        if (item){
            QVariant data = item->data(Qt::DisplayRole);
            headerArray.append( data.toString() );
        }
    }
    table_obj["header"]   = headerArray;

    QJsonArray dataArray;
    for(int i = 0; i <  table->rowCount() ; i++){
        //QJsonArray rowdataArray;
        QString str;
        for(int j = 0; j <  table->columnCount() ; j++){
            auto item = table->item(i,j);
            if (item){
                QVariant data = item->data(Qt::DisplayRole);
                str.append( data.toString() );
                if (j <  table->columnCount() - 1) str.append( " | " );
             }
            else{
               str.append( "    " );
               if (j <  table->columnCount() - 1) str.append( " | " );
            }
        }
        dataArray.append( str );
    }

    table_obj["data"]   = dataArray;

    return table_obj;
}

void RouteTable::initFromJson(const QJsonObject &  table_obj)
{
    table->clear();
    if(table_obj.contains("header") && table_obj["header"].isArray()){
        QJsonArray headerArray = table_obj["header"].toArray();
        int i = 0;
        for(auto colName  : headerArray){
            table->setHorizontalHeaderItem(i, new QTableWidgetItem( colName.toString() )  );
            i++;
            if (i >= table->columnCount()) break;
        }
    }

    if(table_obj.contains("data") && table_obj["data"].isArray()){
        QJsonArray dataArray = table_obj["data"].toArray();
        int rowCnt = 0;
        int colCnt = 0;
        for(auto rowString :dataArray){
            QString str = rowString.toString();
            auto splitted_str = str.split(" | ");
            for(auto display_str : splitted_str){
                auto item = table->item(rowCnt,colCnt);
                if (item){
                    //table->setItem(rowCnt,colCnt, new QTableWidgetItem( item) );
                    item->setData(Qt::DisplayRole,display_str);
                } else{
                    table->setItem(rowCnt,colCnt, new QTableWidgetItem( display_str) );
                }
                colCnt++;
            }
         }
        rowCnt++;
    }

    this->setTableStyle();
}


void  RouteTable::cellChangedSlot(int row, int column){
    Q_UNUSED(row);
    table->resizeColumnToContents(column);
}

void RouteTable::setTableStyle() const{
    for(int i = 0; i <  table->rowCount() ; i++){
        for(int j = 3; j <  table->columnCount() ; j++){
            auto item = table->item(i,j);
            if (item){
                item->setBackground(QBrush(Qt::gray));
            }
            else{
               auto  newItem = new QTableWidgetItem();
               newItem->setBackground(QBrush(Qt::lightGray));
               table->setItem(i,j, newItem );
            }
        }
    }
}
