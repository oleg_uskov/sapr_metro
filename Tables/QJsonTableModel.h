#ifndef QJsonTableModel_H
#define QJsonTableModel_H

#include <QObject>
#include <QVector>
#include <QMap>
#include <QAbstractTableModel>
#include <QJsonDocument>
#include <QJsonArray>
//#include <QJSValue>
//#include <QtQml/qqml.h>

class QJsonTableModel : public QAbstractTableModel
{
    Q_OBJECT
    //Q_PROPERTY(QVariant rows READ rows WRITE setRows NOTIFY rowsChanged FINAL)
    //Q_PROPERTY(QQmlListProperty<JArrayColumnModel> columns READ columns CONSTANT FINAL)

    //QML_ELEMENT
    //QML_ADDED_IN_MINOR_VERSION(1)

    //Q_ENUMS(Roles)
public:
//    enum Roles {
//        CellRole
//    };

//    QHash<int, QByteArray> roleNames() const override {
//        return {
//            { CellRole, "value" }
//        };
//    }

    typedef QMap<QString,QString> Heading;
    typedef QVector<Heading> Header;
    QJsonTableModel( const Header& header, QObject * parent = 0);
    QJsonTableModel( QObject * parent = 0);

    Q_INVOKABLE bool setJson( const QJsonDocument& json );
    Q_INVOKABLE bool setJson( const QJsonArray& array );

    QVariant rows() const;
    void setRows(const QVariant &rows);

    virtual QJsonObject getJsonObject( const QModelIndex &index ) const;

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Qt::ItemFlags flags(const QModelIndex &index) const override;
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

signals:
    void rowsChanged();

private:
    Header m_header ; //= { {"title","Title"},    {"index","title"} };
    QJsonArray m_json;
};

#endif // QJsonTableModel_H
