#ifndef ADRESSOK_MODEL_H
#define ADRESSOK_MODEL_H

#include <QListWidgetItem>
#include <QPushButton>
#include <QListWidget>
#include <QTreeWidget>
#include <QTableWidget>
#include <QTreeView>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QJsonDocument>
#include <QJsonObject>
#include <QObject>
#include "PortOK_model.h"


class AdressOK_model : public QObject
{
    Q_OBJECT
public:
    AdressOK_model(QListWidget * list, QListWidget * listPort, QTreeView * propertyOK_tree);

    QList<PortOK_model *> PortOK_model_list;
private:
    //QJsonObject InitPropertyOK(QString type);

private:
    QListWidget * m_list;

};

#endif // ADRESSOK_MODEL_H
