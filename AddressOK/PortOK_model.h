#ifndef PORTOK_MODEL_H
#define PORTOK_MODEL_H

#include <QTableWidget>
#include <QKeyEvent>
#include <QTreeView>
#include <QTreeWidget>
#include <QTableWidget>
#include <QPointer>
#include "QJsonModel.h"

enum portOK_DataRole {
    TypeOkRole       = 0x0101,
    hexAddrRole      = 0x0102,
    ReservOK         = 0x0103,
    PropertyOK       = 0x0104,
    InitFlag         = 0x0105,
    hexAddrIndex     = 0x0106,
};

class PortOK_model : public QTableWidget
{
public:
    explicit PortOK_model(int portKSv, QTreeView * propertyOK_tree, QWidget * parent = nullptr);
    using QTableWidget::QTableWidget;
    //using QTableWidget::itemDoubleClicked;

    QDataStream mStream;
//signals:
//	void itemDoubleClicked(QTableWidgetItem * item);

//protected:
//    void mouseMoveEvent( QMouseEvent* event ) override;
    void selectionChanged(const QItemSelection & selected, const QItemSelection & deselected) override;
//    void mouseDoubleClickEvent(QMouseEvent * event) override;
//    void dropEvent(QDropEvent *event) override;
    void keyPressEvent( QKeyEvent* event ) override;

//protected:
    void ShowPropertyModel(QMap<int, QVariant> &);
    void InitPropertyModel(QMap<int, QVariant> &);
    bool addPropertyFromFile(const QString &name, QJsonModel &);

protected slots:
//    void currentChanged(const QModelIndex & current, const QModelIndex & previous) override;
    void dataChanged(const QModelIndex & topLeft, const QModelIndex & bottomRight,  const QVector<int> &roles = QVector<int>()) override;
    void itemDoubleClicked_slot(QTableWidgetItem * item);


private:
    bool m_en_change_cell = 0;
    int m_portKSv = 0;
    QTreeView * m_propertyOK_tree;
};

#endif // PORTOK_MODEL_H
