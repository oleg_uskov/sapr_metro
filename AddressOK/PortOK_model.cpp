#include "PortOK_model.h"
#include "QJsonModel.h"
#include <QModelIndexList>
#include <QTableWidgetItem>
#include <QTreeWidgetItem>
#include <QList>
#include <QCheckBox>
#include <QDebug>

//QDataStream& operator<<(QDataStream& out,  QTreeWidgetItem*  const & myObj) {
//    //QList<QTreeWidgetItem *> children;
//    myObj->write(out);
//    out << myObj->childCount();
//    for (int i = 0; i < myObj->childCount(); i++) {
//        //out << myObj->child(i);
//        myObj->child(i)->write(out);
//        out << myObj->child(i)->flags();
//    }
//    return out;
//}

//QDataStream& operator >> (QDataStream & in, QTreeWidgetItem* & myObj) {
//    myObj = new QTreeWidgetItem();
//    int childCount;
//    Qt::ItemFlags childFlags;
//    QTreeWidgetItem * child ;//= new QTreeWidgetItem();
//    myObj->read(in);
//    in >> childCount;
//    for (int i = 0; i < childCount; i++) {
//        child = new QTreeWidgetItem();
//        child->read(in);
//        in >> childFlags;
//        //in >> child;
//        child->setFlags(childFlags);
//        myObj->addChild(child);
//    }
//    return in;
//}

//QDataStream& operator<<(QDataStream& out,  QJsonModel  const & myObj) {
//    //out << myObj->json();
//    return out;
//}

//QDataStream& operator >> (QDataStream & in, QJsonModel & myObj) {
//    return in;
//}


QDataStream& operator<<(QDataStream& out,  QJsonModel*  const & myObj) {
    QJsonDocument jdoc = myObj->json();
    out << jdoc.toVariant();
    return out;
}

QDataStream& operator >> (QDataStream & in, QJsonModel* & myObj) {
    QVariant var;
    in >> var;
    myObj = new QJsonModel(QJsonDocument::fromVariant(var));
    return in;
}

//QDataStream& operator<<(QDataStream& out,   QList<QTreeWidgetItem *> const  & myObj) {
//    //for
//    out << myObj.count();
//    for (int i = 0; i < myObj.count(); i++) {
//        out << myObj.at(i);
//    }
//    return out;
//}

//QDataStream& operator >> (QDataStream & in, QList<QTreeWidgetItem *> & myObj) {
//    int cnt;
//    QTreeWidgetItem * child;
//    in >> cnt;
//    for (int i = 0; i < cnt; i++) {
//        in >> child;
//        myObj.append(child);
//    }
//    //in >> myObj;
//    return in;
//}





//Q_DECLARE_METATYPE(QTreeWidgetItem)
//Q_DECLARE_METATYPE(QTreeWidgetItem *)
//Q_DECLARE_METATYPE(QList<QTreeWidgetItem *>)

//Q_DECLARE_METATYPE(QJsonModel)
Q_DECLARE_METATYPE(QJsonModel *)


//Q_DECLARE_SEQUENTIAL_CONTAINER_METATYPE(QList<QTreeWidgetItem *>)
//Q_DECLARE_OPAQUE_POINTER(QTreeWidgetItem *)

PortOK_model::PortOK_model(int portKSv, QTreeView * propertyOK_tree, QWidget *parent):
    QTableWidget(8, 2, parent),
    m_portKSv(portKSv),
    m_propertyOK_tree(propertyOK_tree)
{
//    qRegisterMetaTypeStreamOperators<QTreeWidgetItem >("QTreeWidgetItem");
//    qRegisterMetaTypeStreamOperators<QTreeWidgetItem *>("QTreeWidgetItem *");
//    qRegisterMetaTypeStreamOperators<QList<QTreeWidgetItem *>>("QList<QTreeWidgetItem *>");

//    qRegisterMetaTypeStreamOperators<QJsonModel>("QJsonModel");
    qRegisterMetaTypeStreamOperators<QJsonModel *>("QJsonModel *");

    this->setHorizontalHeaderLabels({"ПОРТ КСн: 0" , "ПОРТ КСн: 1"});
    this->setDragDropMode(QAbstractItemView::DragDrop);
    this->setDragEnabled(true);
    this->setEditTriggers(QAbstractItemView::NoEditTriggers);
    this->setDragDropOverwriteMode(true);
    this->setDefaultDropAction(Qt::MoveAction);
    this->setSelectionMode(QAbstractItemView::ContiguousSelection);
    for (int i = 0; i < this->columnCount() ; i++) {
       this->setColumnWidth(i, 115);
    }

    connect(this, &QTableWidget::itemDoubleClicked, this, &PortOK_model::itemDoubleClicked_slot);
}

void PortOK_model::itemDoubleClicked_slot(QTableWidgetItem * item){
/*изменение состояния "наличие резерва"*/
    QVariant PropertyOK_var = item->data(portOK_DataRole::PropertyOK);;
    auto PropertyOK = PropertyOK_var.value<QJsonModel *>();

    if (item->data(portOK_DataRole::ReservOK).toBool()){
        item->setData(portOK_DataRole::ReservOK, false);
        //propertyOK["Резерв"] = "нет";
        PropertyOK->setData(PropertyOK->index(2, 1), "false");
        item->setBackground(QBrush(Qt::green));
    }
    else
    {
        QLinearGradient linearGrad(QPointF(50, 15), QPointF(60, 25));
        linearGrad.setColorAt(0, Qt::green);
        linearGrad.setColorAt(1, Qt::yellow);
        item->setBackground(QBrush(linearGrad));
        item->setData(portOK_DataRole::ReservOK, true);
        PropertyOK->setData(PropertyOK->index(2, 1), "true");
    }

}

///*событие двойного нажатия на мышь*/
//void PortOK_model::mouseDoubleClickEvent(QMouseEvent * event){
//    QTableWidget::mouseDoubleClickEvent(event);
//}

////void PortOK_model::mouseClickEvent(QMouseEvent * event){
////    QTableWidget::mouseClickEvent(event);
////}

/*событие выделения ячеек*/
void PortOK_model::selectionChanged(const QItemSelection & selected, const QItemSelection & deselected){

    if (selected.count()){
        QModelIndex current = selected.indexes().first();
        auto item_cell = this->model()->itemData(current);
        if (item_cell[Qt::DisplayRole].isValid()) // не изменяем пустые ячейки
            ShowPropertyModel(item_cell);
     }
    QTableWidget::selectionChanged(selected,deselected);
}

//void PortOK_model::dropEvent(QDropEvent *event){
//    m_en_change_cell = true;
//    QTableWidget::dropEvent(event);
////    QModelIndex cell = currentIndex();
////    auto type_and_address = cell.data().toString();
//}


//void PortOK_model::currentChanged(const QModelIndex & current, const QModelIndex & previous){
//}

void PortOK_model::dataChanged(const QModelIndex & topLeft, const QModelIndex & bottomRight,  const QVector<int> &roles){

    clearSelection();
    auto item_cell = this->model()->itemData(topLeft);

    if (!item_cell[Qt::DisplayRole].isValid()) // не изменяем пустые ячейки
        return;
/*изменяем отображение ячейки*/
    int numberOK = topLeft.row();
    int lowPort = topLeft.column();

    unsigned short fullAddress =  (0 << 13) | (m_portKSv << 9) | (0 << 8) | (lowPort << 4) | (numberOK << 1) | 1;

    QString type_and_address = item_cell[portOK_DataRole::TypeOkRole].toString() + " 0x" + QString::number(fullAddress,16).rightJustified(4, '0');
    item_cell[Qt::DisplayRole] = type_and_address;
    item_cell[portOK_DataRole::hexAddrRole] = fullAddress;

    /*изменяем поле свойств выбранного ОК*/
    if (!item_cell[portOK_DataRole::InitFlag].toBool())
            InitPropertyModel(item_cell);

    ShowPropertyModel(item_cell);

    this->model()->setItemData(topLeft, item_cell);
    QTableWidget::dataChanged(topLeft, bottomRight, roles);

}


void PortOK_model::keyPressEvent( QKeyEvent* event )
{
    switch ( event->key() )
    {
        case Qt::Key_Delete:
            {
                foreach ( QTableWidgetItem * item, selectedItems()  )
                {
                    takeItem(item->row(),  item->column());
                    //delete item;

                }
                clearSelection();

                break;
            }

    default:
        break;
    }

    QTableWidget::keyPressEvent( event );
}


void PortOK_model::ShowPropertyModel(QMap<int, QVariant> & OKdata)
{
    QVariant PropertyOK_var = OKdata[portOK_DataRole::PropertyOK];
    auto PropertyOK = PropertyOK_var.value<QJsonModel *>();

    /*добавление новой записи*/
    auto addresOK = OKdata[portOK_DataRole::hexAddrRole].toInt();
//    auto indexItem = OKdata[portOK_DataRole::hexAddrIndex].toModelIndex();
//    auto addrItem = static_cast<QJsonTreeItem*>(indexItem.internalPointer());
//    addrItem->setValue(QString::number(addresOK,16).rightJustified(4, '0'));
    PropertyOK->setData(PropertyOK->index(1, 1), QString::number(addresOK,16).rightJustified(4, '0'));

    m_propertyOK_tree->setModel(PropertyOK);
    m_propertyOK_tree->expandAll();
}


/**********************************
 * Инициализируем свойство объектных контроллеров
***********************************/
void PortOK_model::InitPropertyModel(QMap<int, QVariant> & OKdata)
{
    auto typeOK = OKdata[portOK_DataRole::TypeOkRole].toString();
    auto addresOK = OKdata[portOK_DataRole::hexAddrRole].toInt();

    if (OKdata[portOK_DataRole::InitFlag].toBool())
        return;

    QJsonModel * m_PropertyModel = new QJsonModel;
    QJsonTreeItem *root = m_PropertyModel->invisibleRootItem();;
    QJsonTreeItem *node = nullptr;

    new QJsonTreeItem("1)Тип ОК", typeOK, root);

    node = new QJsonTreeItem("2)Адрес", QString::number(addresOK,16).rightJustified(4, '0'), root);
    //OKdata[portOK_DataRole::hexAddrIndex] = m_PropertyModel->index(node->row(), 0);
    new QJsonTreeItem("3)Резерв", "true", root);
    //new QJsonTreeItem("4)Имя", "true", root);

    addPropertyFromFile(":/tempalete/json/" + typeOK + "_prop.json" , *m_PropertyModel);
    //addPropertyFromFile("../SAPR_Metro/tempalete/json/" + typeOK + "_prop.json" , *m_PropertyModel);
    OKdata[portOK_DataRole::InitFlag] = true;
    auto & var = OKdata[portOK_DataRole::PropertyOK];//
    //auto m_PropertyModel_cast = static_cast<void *>(m_PropertyModel);
    var = QVariant::fromValue(m_PropertyModel);
}


bool PortOK_model::addPropertyFromFile( const QString &name, QJsonModel & model) {
    //* Создаем модель из изменяемых свойств*//
    QString saveData;
    //auto openFile = QFile( name );
    QFile openFile(name);

    if(!openFile.open(QIODevice::ReadOnly)) {
        qDebug()<<"file not opened \n";
        return false;
    }
    else
    {
        //qDebug()<<"file opened \n";
        saveData = openFile.readAll();
    }

    openFile.close();

    auto Jdoc = QJsonDocument::fromJson(saveData.toUtf8());
    QJsonObject JObject = Jdoc.object();
    QJsonObject obj = JObject["Properties"].toObject();

    model.addJson(QJsonDocument(obj));
    return true;
    //*********************************************************//
}

