#include "AdressOK_model.h"
//#include <QTreeView>
#include <QDebug>
#include <QMetaType>
#include <QTreeWidgetItem>
#include "PortOK_model.h"


//Q_DECLARE_METATYPE(QList<QTreeWidgetItem *>)
//Q_DECLARE_SEQUENTIAL_CONTAINER_METATYPE(QList<QTreeWidgetItem *>)
//Q_DECLARE_OPAQUE_POINTER(QTreeWidgetItem *)

//using QTreeWidgetItem_prt = QTreeWidgetItem *;
//Q_DECLARE_METATYPE(QTreeWidgetItem)
//Q_DECLARE_METATYPE(QTreeWidgetItem_prt)


AdressOK_model::AdressOK_model(QListWidget * listOK, QListWidget * listPort, QTreeView * propertyOK_tree):
    QObject(nullptr)
{

    propertyOK_tree->setAlternatingRowColors(true);
    propertyOK_tree->setStyleSheet("alternate-background-color: lightGray; background-color: white;");

    m_list = listOK;
    m_list->setSpacing(2);

    //формирование списка доступных объектных контроллеров
    const QStringList RailObjectControlList = {"ОКД-Е" , "ОКД-Е-В" , "ГКЛС-Е", "ОКПС-Е-К", "ГАРС-Е", "ОКС-Е", "ОКЗМ-Е", "ОКСМ-Е", "ОКММ-Е", "ГП-Е"};
    //m_list->addItems( RailObjectControlList);
    m_list->setDragDropMode(QAbstractItemView::DragOnly);
    QLinearGradient linearGrad(QPointF(45, 15), QPointF(55, 25));
    linearGrad.setColorAt(0, Qt::green);
    linearGrad.setColorAt(1, Qt::yellow);

/*формирование типов ОК*/
    QListWidgetItem * row_item;
    for(int i = 0; i < RailObjectControlList.count() ; i++ ) {
        //row_item =  m_list->item(i);
        row_item = new QListWidgetItem();
        //row_item->setBackground(QBrush(Qt::green));
        row_item->setBackground(QBrush(linearGrad));
        row_item->setSizeHint(QSize(90,30));
        row_item->setText(RailObjectControlList[i]);
        //row_item->setData(Qt::DisplayRole, 10);
        row_item->setData(portOK_DataRole::TypeOkRole, RailObjectControlList[i]);
        row_item->setData(portOK_DataRole::hexAddrRole, 0);
        row_item->setData(portOK_DataRole::ReservOK, true);
        row_item->setData(portOK_DataRole::InitFlag, false); //QVariant::fromValue(static_cast<QObject *>(nullptr)))

        row_item->setFont(QFont("Times", 10, QFont::Bold));
        m_list->addItem( row_item);

    }

/*формирование и настройка портов КСвв*/
    QListWidgetItem * highPortItem;
    QGroupBox * highPortBox;
    QHBoxLayout * VerticalBox;
    for(int i = 0; i < 16 ; i++ ) {
        highPortItem = new QListWidgetItem(listPort);
        highPortBox = new QGroupBox("ПОРТ КСв: " + QString::number(i), listPort);
        VerticalBox = new QHBoxLayout;
        PortOK_model * lowPortTableWidget = new PortOK_model(i, propertyOK_tree);
        /**********************************************/
        VerticalBox->addWidget(lowPortTableWidget);
        PortOK_model_list.append(lowPortTableWidget);
        highPortBox->setLayout(VerticalBox);
        highPortItem->setSizeHint(QSize(270,315));
        listPort->setItemWidget(highPortItem, highPortBox);
    }

}

